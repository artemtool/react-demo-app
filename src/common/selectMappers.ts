import { SelectOption } from './../models/selectOption';
import { SelectGroup } from '../models/selectGroup';

export const mapCityIdToSelectOption = (() => {
    let cache = new Map();
    
    return (cityId: string, regions: Array<SelectGroup>) => {
        if (cache.has(cityId)) {
            return cache.get(cityId);
        }

        let selectOption;
        for (let region of regions) {
            selectOption = region.options.find(opt => opt.value === cityId);
            if (selectOption) {
                cache.set(cityId, selectOption);
                break;
            }
        }

        return selectOption;
    }
})();

export const mapValueToSelectOption = (value: any, options: Array<SelectOption>) => {
    return options.find(o => o.value == value) || null;
};