import * as SignalR from '@aspnet/signalr'
import { ACCESS_TOKEN } from './../constants/keys';

class SignalRConnectionStore {
  private readonly signalRConnection: any;

  constructor() {
    this.signalRConnection = new SignalR.HubConnectionBuilder()
      .withUrl(`${process.env.REACT_APP_CORE_API_URL}/liveUpdatesHub`, {
        accessTokenFactory: () => <string>localStorage.getItem(ACCESS_TOKEN)
      })
      .configureLogging(SignalR.LogLevel.Information)
      .build();
  }

  public get connection() {
    return this.signalRConnection;
  }
}

const store = new SignalRConnectionStore();
export default store;