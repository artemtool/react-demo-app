

import { ACCESS_TOKEN } from "../constants/keys";

class Http {
  stickAuthHeaderIfExists(headers: { [key: string]: string } = {}): Promise<{ [key: string]: string }> {
    return new Promise((resolve, reject) => {
      const accessToken = localStorage.getItem(ACCESS_TOKEN);
      if (accessToken) {
        headers['Authorization'] = `Bearer ${accessToken}`;
      }
      resolve(headers);
    });
  }

  stickAuthHeader(headers: { [key: string]: string } = {}): Promise<{ [key: string]: string }> {
    return new Promise((resolve, reject) => {
      this.stickAuthHeaderIfExists(headers)
        .then(headers => {
          if (headers['Authorization']) {
            resolve(headers)
          } else {
            reject();
          }
        });
    });
  }
}

export const http = new Http();