import { REFRESH_TOKEN, ACCESS_TOKEN } from "../constants/keys";

(function () {
  const nativeFetch = window.fetch;

  window.fetch = function () {
    const args = arguments;
    return nativeFetch.apply(window, args)
      .then(result => {
        const accessToken = localStorage.getItem(ACCESS_TOKEN);
        const refreshToken = localStorage.getItem(REFRESH_TOKEN);

        if (result.status === 401 && accessToken && refreshToken) {
          return refreshAccessToken(<string>accessToken, <string>refreshToken)
            .then(newAccessToken => {
              if (typeof args[1] === 'object') {
                let headers = args[1].headers;
                if (!headers) {
                  headers = {};
                }
                headers['Authorization'] = `Bearer ${newAccessToken}`;
              }

              return nativeFetch.apply(window, args);
            })
            .catch(() => {
              localStorage.removeItem(ACCESS_TOKEN);
              localStorage.removeItem(REFRESH_TOKEN);
            })
        } else {
          return result;
        }
      });
  }

  function refreshAccessToken(accessToken: string, refreshToken: string): Promise<string> {
    return nativeFetch.call(window, `${process.env.REACT_APP_AUTH_API_URL}/user/refreshToken`, {
      method: 'POST',
      body: JSON.stringify({
        AccessToken: accessToken,
        RefreshToken: refreshToken
      }),
      headers: {
        'Content-type': 'application/json'
      }
    })
      .then(response => {
        if (response.status === 403) {
          throw new Error('Token validation failed.');
        } else {
          return response.json();
        }
      })
      .then(result => {
        localStorage.setItem(ACCESS_TOKEN, result.accessToken);
        localStorage.setItem(REFRESH_TOKEN, result.refreshToken);

        return result.accessToken;
      });
  }
})();