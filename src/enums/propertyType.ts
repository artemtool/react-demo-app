export enum PropertyType {
  Apartment = 0,
  Villa = 1,
  HotelRoom = 2
}