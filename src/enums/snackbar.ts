export enum Snackbar {
  Success = 0,
  Error = 1,
  Info = 2,
  Warning = 3
}