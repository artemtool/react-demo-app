export enum PropertyStatus {
  Active = 0,
  Inactive = 1, 
  WaitingForModeration = 2
}