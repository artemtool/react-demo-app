export enum MessageStatus {
  Sending = 0,
  Sent = 1,
  Read = 2,
  Error = 3
}