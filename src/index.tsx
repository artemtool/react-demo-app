import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { compose } from 'recompose';
import thunkMiddleware from 'redux-thunk';
import createSagaMiddleware from 'redux-saga';
import './index.scss';
import App from './App';
// import registerServiceWorker from './registerServiceWorker';
import appReducer from './state/reducers/index';
import rootSaga from './state/sagas';
require('./common/fetchInterceptor');

const sagaMiddleware = createSagaMiddleware();
const middlewares = [
  applyMiddleware(
    thunkMiddleware,
    sagaMiddleware
  )
];
if (window['__REDUX_DEVTOOLS_EXTENSION__']) {
  middlewares.push(window['__REDUX_DEVTOOLS_EXTENSION__']())
}

export const store = createStore(
  appReducer,
  compose(...middlewares)
);
sagaMiddleware.run(rootSaga, store.dispatch);

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <App />
    </Router>
  </Provider>,
  document.getElementById('root')
);
// registerServiceWorker();
