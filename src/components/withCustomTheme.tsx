import * as React from 'react';
import {
  withStyles,
  createMuiTheme,
  MuiThemeProvider
} from '@material-ui/core/styles';
import { blue } from '@material-ui/core/colors';

const theme = createMuiTheme({
  palette: {
    primary: blue
  }
});

const styles = theme => ({
  ...theme,
  root: {
    flexGrow: 1,
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
    height: '100%',
    minHeight: '100vh'
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: 320
  },
  drawerPaper: {
    position: 'fixed',
    width: 320,
    display: 'block'
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    minWidth: 0, // So the Typography noWrap works
    maxWidth: 960,
    margin: '0 auto'
  },
  toolbar: theme.mixins.toolbar,
});

class WithCustomTheme extends React.PureComponent {

  render() {
    return (
      <MuiThemeProvider theme={theme}>
        {this.props.children}
      </MuiThemeProvider>
    );
  }
}

const withCustomTheme = (Component: React.ComponentType<any>) => {
  return withStyles(styles)((props) =>
    <WithCustomTheme>
      <Component  {...props} />
    </WithCustomTheme>
  );
}

export default withCustomTheme;