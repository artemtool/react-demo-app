import * as React from 'react';
import { Route, Redirect, withRouter } from 'react-router';
import { AppState } from '../state/appState';
import { connect } from 'react-redux';
import { User as UserType } from '../enums/user';
import { User } from './../models/user';
import { compose } from 'recompose';

interface Props {
  onlyFor: UserType;
  userType: UserType;
  component: any;
  redirectPath?: string;
  location: any;
};

class ProtectedRoute extends React.PureComponent<Props> {
  render() {
    const {
      onlyFor,
      userType,
      redirectPath,
      component,
      ...rest
    } = this.props;
    const Component = component;

    const sideRedirect = this.props.location.state && this.props.location.state.from;
    const propsRedirect = {
      pathname: redirectPath,
      state: { from: this.props.location }
    };

    return (
      <Route
        {...rest}
        render={props =>
          onlyFor === userType ? (
            <Component {...props} />
          ) : (
              <Redirect to={sideRedirect || propsRedirect} />
            )
        }
      />
    );
  }
}

function getUserType(user: User | null): UserType {
  return user
    ? UserType.Authorized
    : UserType.Anonymous;
}

const mapStateToProps = (state: AppState) => ({
  userType: getUserType(state.user.authUser)
});

export default compose(
  withRouter,
  connect(
    mapStateToProps
  )
)(ProtectedRoute);