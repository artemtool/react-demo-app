import * as React from 'react';
import Typography from '@material-ui/core/Typography';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import * as styles from './EditPropertyForm.scss';
import Select from 'react-select';
import Input from '../../components/Input/Input';
import Label from './../../components/Label/Label';
import TextArea from './../../components/TextArea/TextArea';
import Button from '@material-ui/core/Button';
import AddAPhotoIcon from '@material-ui/icons/AddAPhoto';
import ImagePreview from './../../components/ImagePreview/ImagePreview';
import { mapCityIdToSelectOption, mapValueToSelectOption } from './../../common/selectMappers';
import { propertyFeatures, sleepsOptionList, floorsOptionList, propertyTypesOptionList, roomsOptionList } from './../../constants/property';
import FormValidator from './../../common/formValidator';
import * as validator from 'validator';
import { SelectGroup } from './../../models/selectGroup';
import { Property } from './../../models/property';

interface Props {
  citySuggestions: Array<SelectGroup>;
  submitButtonText: string;
  cancelButtonText: string;
  onSubmit: (formData: FormData) => void;
  onCancel: () => void;
  defaultData?: Property;
  isDefaultDataLoading?: boolean;
};

interface State {
  cityId: string;
  type: string;
  title: string;
  address: string;
  description: string;
  dailyPrice: string;
  weeklyPrice: string;
  rooms: string;
  area: string;
  sleeps: string;
  floor: string;
  hasWiFi: boolean;
  hasParkingPlace: boolean;
  hasTV: boolean;
  hasAirConditioner: boolean;
  hasComputer: boolean;
  hasIroningStaff: boolean;
  hasKettle: boolean;
  hasMicrowaveOven: boolean;
  hasHairDryer: boolean;
  hasWashingMachine: boolean;
  hasBedLinen: boolean;
  hasTowels: boolean;
  hasDishes: boolean;
  images: Array<string>;
  imagesToUpload: Array<File>;
  imagesToDelete: Array<string>;
  mainImage: string | null;
  validation: any;
}

export default class EditPropertyForm extends React.PureComponent<Props, State> {
  private readonly SUBMIT_EXCLUDED = ['mainImage', 'images', 'imagesToUpload', 'imagesToDelete', 'validation'];
  private validator: FormValidator;

  constructor(props) {
    super(props);

    this.validator = new FormValidator([
      {
        field: 'cityId',
        method: validator.isEmpty,
        validWhen: false,
        message: 'Please select a city.'
      },
      {
        field: 'type',
        method: validator.isInt,
        validWhen: true,
        message: 'Please select a property type.'
      },
      {
        field: 'title',
        method: validator.isEmpty,
        validWhen: false,
        message: 'Please enter a property title.'
      },
      {
        field: 'address',
        method: validator.isEmpty,
        validWhen: false,
        message: 'Please enter a property address.'
      },
      {
        field: 'dailyPrice',
        method: validator.isInt,
        validWhen: true,
        message: 'Please enter a daily price.'
      },
      {
        field: 'weeklyPrice',
        method: validator.isInt,
        validWhen: true,
        message: 'Please enter a weekly price.'
      },
      {
        field: 'rooms',
        method: validator.isInt,
        validWhen: true,
        message: 'Please select rooms amount.'
      },
      {
        field: 'area',
        method: validator.isInt,
        validWhen: true,
        message: 'Please enter a property area.'
      },
      {
        field: 'sleeps',
        method: validator.isInt,
        validWhen: true,
        message: 'Please select sleeps amount.'
      },
      {
        field: 'floor',
        method: validator.isInt,
        validWhen: true,
        message: 'Please select floor.'
      }
    ]);

    this.state = {
      cityId: '',
      type: '',
      title: '',
      address: '',
      description: '',
      dailyPrice: '',
      weeklyPrice: '',
      rooms: '',
      area: '',
      sleeps: '',
      floor: '',
      hasWiFi: false,
      hasParkingPlace: false,
      hasTV: false,
      hasAirConditioner: false,
      hasComputer: false,
      hasIroningStaff: false,
      hasKettle: false,
      hasMicrowaveOven: false,
      hasHairDryer: false,
      hasWashingMachine: false,
      hasBedLinen: false,
      hasTowels: false,
      hasDishes: false,
      images: [],
      imagesToUpload: [],
      imagesToDelete: [],
      mainImage: null,
      validation: this.validator.valid()
    };
  }

  public componentDidUpdate(prevProps: Props) {
    if (this.props.defaultData && this.props.defaultData != prevProps.defaultData) {
      const property: Property = this.props.defaultData;
      const mainImageUrl = property.getMainImageUrl();
      const imageUrls = property.getImageUrls()
        .filter(url => url !== process.env.REACT_APP_PROPERTY_IMAGE_STUB);
      this.setState({
        cityId: property.city.id,
        type: property.type.toString(),
        title: property.title,
        address: property.address,
        description: property.description,
        dailyPrice: property.dailyPrice.toString(),
        weeklyPrice: property.weeklyPrice.toString(),
        rooms: property.rooms.toString(),
        area: property.area.toString(),
        sleeps: property.sleeps.toString(),
        floor: property.floor.toString(),
        hasWiFi: property.hasWiFi,
        hasParkingPlace: property.hasParkingPlace,
        hasTV: property.hasTV,
        hasAirConditioner: property.hasAirConditioner,
        hasComputer: property.hasComputer,
        hasIroningStaff: property.hasIroningStaff,
        hasKettle: property.hasKettle,
        hasMicrowaveOven: property.hasMicrowaveOven,
        hasHairDryer: property.hasHairDryer,
        hasWashingMachine: property.hasWashingMachine,
        hasBedLinen: property.hasBedLinen,
        hasTowels: property.hasTowels,
        hasDishes: property.hasDishes,
        images: imageUrls,
        mainImage: mainImageUrl === process.env.REACT_APP_PROPERTY_IMAGE_STUB
          ? null
          : mainImageUrl
      })
    }
  }

  public render() {
    const validation = this.state.validation;

    return this.props.isDefaultDataLoading
      ? 'Data is loading... Wait for a moment.'
      : (
        <form
          className={styles.form}
          onSubmit={e => this.onSubmit(e)}
          noValidate
        >
          <fieldset className={styles.fieldSet}>
            <Typography variant="body2" gutterBottom>
              <span className={styles.fieldSetCounter}>1</span>Fill up the main data
            </Typography>
            <div className={styles.fieldSetContent}>
              <div className={styles.propertyData}>
                <div className={styles.column}>
                  <div className={`${styles.formControl} ${validation.title.isInvalid && styles.hasError}`}>
                    <Label label="Title" />
                    <Input
                      id="title"
                      name="title"
                      type="text"
                      placeholder="Enter concise and expressive description"
                      className={styles.field}
                      value={this.state.title}
                      onChange={e => this.onValueChange('title', e.target.value)}
                    />
                    <Typography className={styles.errorLabel}>{validation.title.message}</Typography>
                  </div>
                  <div className={`${styles.formControl} ${validation.address.isInvalid && styles.hasError}`}>
                    <Label label="Address" />
                    <Input
                      id="address"
                      name="address"
                      type="text"
                      placeholder="Enter property address"
                      className={styles.field}
                      value={this.state.address}
                      onChange={e => this.onValueChange('address', e.target.value)}
                    />
                    <Typography className={styles.errorLabel}>{validation.address.message}</Typography>
                  </div>
                  <div className={`${styles.formControl} ${validation.sleeps.isInvalid && styles.hasError}`}>
                    <Label label="Sleeps" />
                    <Select
                      options={sleepsOptionList}
                      isClearable={true}
                      placeholder={'Sleeps'}
                      className={styles.select}
                      value={mapValueToSelectOption(this.state.sleeps, sleepsOptionList)}
                      onChange={e => this.onValueChange('sleeps', e ? e.value : '')}
                    />
                    <Typography className={styles.errorLabel}>{validation.sleeps.message}</Typography>
                  </div>
                  <div className={`${styles.formControl} ${validation.floor.isInvalid && styles.hasError}`}>
                    <Label label="Floor" />
                    <Select
                      options={floorsOptionList}
                      isClearable={true}
                      placeholder={'Floor'}
                      className={styles.select}
                      value={mapValueToSelectOption(this.state.floor, floorsOptionList)}
                      onChange={e => this.onValueChange('floor', e ? e.value : '')}
                    />
                    <Typography className={styles.errorLabel}>{validation.floor.message}</Typography>
                  </div>
                  <div className={`${styles.formControl} ${validation.dailyPrice.isInvalid && styles.hasError}`}>
                    <Label label="Price per day ($)" />
                    <Input
                      id="dailyPrice"
                      name="dailyPrice"
                      type="number"
                      placeholder="Enter daily cost"
                      className={styles.field}
                      value={this.state.dailyPrice}
                      onChange={e => this.onValueChange('dailyPrice', e.target.value)}
                    />
                    <Typography className={styles.errorLabel}>{validation.dailyPrice.message}</Typography>
                  </div>
                </div>
                <div className={styles.column}>
                  <div className={`${styles.formControl} ${validation.type.isInvalid && styles.hasError}`}>
                    <Label label="Property type" />
                    <Select
                      options={propertyTypesOptionList}
                      isClearable={true}
                      placeholder={'Select property type'}
                      className={styles.select}
                      value={mapValueToSelectOption(this.state.type, propertyTypesOptionList)}
                      onChange={e => this.onValueChange('type', e ? e.value : '')}
                    />
                    <Typography className={styles.errorLabel}>{validation.type.message}</Typography>
                  </div>
                  <div className={`${styles.formControl} ${validation.cityId.isInvalid && styles.hasError}`}>
                    <Label label="City" />
                    <Select
                      options={this.props.citySuggestions}
                      isClearable={true}
                      isLoading={this.props.citySuggestions.length === 0}
                      placeholder={'Choose city'}
                      className={styles.select}
                      value={mapCityIdToSelectOption(this.state.cityId, this.props.citySuggestions)}
                      onChange={e => this.onValueChange('cityId', e ? e.value : '')}
                    />
                    <Typography className={styles.errorLabel}>{validation.cityId.message}</Typography>
                  </div>
                  <div className={`${styles.formControl} ${validation.rooms.isInvalid && styles.hasError}`}>
                    <Label label="Rooms" />
                    <Select
                      options={roomsOptionList}
                      isClearable={true}
                      placeholder={'Rooms'}
                      className={styles.select}
                      value={mapValueToSelectOption(this.state.rooms, roomsOptionList)}
                      onChange={e => this.onValueChange('rooms', e ? e.value : '')}
                    />
                    <Typography className={styles.errorLabel}>{validation.rooms.message}</Typography>
                  </div>
                  <div className={`${styles.formControl} ${validation.area.isInvalid && styles.hasError}`}>
                    <Label label="Area (sq. m.)" />
                    <Input
                      id="area"
                      name="area"
                      type="number"
                      placeholder="Enter property area"
                      className={styles.field}
                      value={this.state.area}
                      onChange={e => this.onValueChange('area', e.target.value)}
                    />
                    <Typography className={styles.errorLabel}>{validation.area.message}</Typography>
                  </div>
                  <div className={`${styles.formControl} ${validation.weeklyPrice.isInvalid && styles.hasError}`}>
                    <Label label="Price per week ($)" />
                    <Input
                      id="weeklyPrice"
                      name="weeklyPrice"
                      type="number"
                      placeholder="Enter weekly cost"
                      className={styles.field}
                      value={this.state.weeklyPrice}
                      onChange={e => this.onValueChange('weeklyPrice', e.target.value)}
                    />
                    <Typography className={styles.errorLabel}>{validation.weeklyPrice.message}</Typography>
                  </div>
                </div>
              </div>
            </div>
          </fieldset>
          <fieldset className={styles.fieldSet}>
            <Typography variant="body2" gutterBottom>
              <span className={styles.fieldSetCounter}>2</span>Select what your property has
            </Typography>
            <div className={`${styles.fieldSetContent} ${styles.fieldSetWrap}`}>
              {
                propertyFeatures.map(feature =>
                  <FormControlLabel
                    key={feature.name}
                    control={
                      <Checkbox
                        color="primary"
                        checked={this.state[feature.name]}
                        onChange={e => this.onValueChange(feature.name, e.target.checked)}
                      />
                    }
                    label={feature.label}
                  />
                )
              }
            </div>
          </fieldset>
          <fieldset className={styles.fieldSet}>
            <Typography variant="body2" gutterBottom>
              <span className={styles.fieldSetCounter}>3</span>Upload photos to make your property more attractive for potential tenants
            </Typography>
            <div className={`${styles.fieldSetContent} ${styles.imagesContainer}`}>
              <Button className={styles.uploadButton}>
                <React.Fragment>
                  <input
                    type="file"
                    accept=".jpg, .jpeg, .png"
                    multiple
                    onChange={e => this.onNewImageAdd(e.target.files)}
                  />
                  <AddAPhotoIcon />
                </React.Fragment>
              </Button>
              {
                this.state.images.map(imageUrl =>
                  <ImagePreview
                    key={imageUrl}
                    image={imageUrl}
                    editable={1}
                    checked={this.state.mainImage === imageUrl ? 1 : 0}
                    onCheck={() => this.onMainImageChange(imageUrl)}
                    onDelete={() => this.onImageDelete(imageUrl)}
                  />
                )
              }
              {
                this.state.imagesToUpload.map(image =>
                  <ImagePreview
                    key={image.name}
                    image={image}
                    editable={1}
                    checked={this.state.mainImage === image.name ? 1 : 0}
                    onCheck={() => this.onMainImageChange(image.name)}
                    onDelete={() => this.onNewImageDelete(image.name)}
                  />
                )
              }
            </div>
          </fieldset>
          <fieldset className={styles.fieldSet}>
            <Typography variant="body2" gutterBottom>
              <span className={styles.fieldSetCounter}>4</span>Describe property key features, nearby area, etc.
            </Typography>
            <div className={styles.fieldSetContent}>
              <TextArea
                style={{ resize: 'vertical' }}
                value={this.state.description}
                onChange={e => this.onValueChange('description', e.target.value)}
              />
            </div>
          </fieldset>
          <Button
            variant="contained"
            color="primary"
            type="submit"
            className={styles.submitButton}
          >
            {this.props.submitButtonText}
          </Button>
          <Button
            color="primary"
            className={styles.submitButton}
            onClick={this.props.onCancel}
          >
            {this.props.cancelButtonText}
          </Button>
        </form>
      );
  }

  private onMainImageChange(image: string) {
    this.setState({
      mainImage: image
    });
  }

  private onImageDelete(imageUrl: string) {
    const imageName = this.getImageName(imageUrl);

    this.setState(state => ({
      images: state.images.filter(n => n !== imageUrl),
      imagesToDelete: [...state.imagesToDelete, imageName]
    }), () => {
      if (this.state.mainImage === imageUrl) {
        const newMainImage = this.state.images[0] || this.state.imagesToUpload[0] && this.state.imagesToUpload[0].name;
        this.setState({
          mainImage: newMainImage
        });
      }
    });
  }

  private onNewImageDelete(name: string) {
    this.setState(state => ({
      imagesToUpload: state.imagesToUpload.filter(i => i.name !== name)
    }), () => {
      if (this.state.mainImage === name) {
        const newMainImage = this.state.images[0] || this.state.imagesToUpload[0] && this.state.imagesToUpload[0].name;
        this.setState({
          mainImage: newMainImage
        });
      }
    });
  }

  private onNewImageAdd(fileList: FileList | null) {
    const files = Array.from(fileList || []);
    const filteredImages = files.filter(newImage => {
      return !this.state.imagesToUpload.some(image => {
        return image.name === newImage.name
          && image.size === newImage.size;
      })
    });

    if (filteredImages.length > 0) {
      let newMainImage = this.state.mainImage;
      if (this.state.images.length === 0 && this.state.imagesToUpload.length === 0) {
        newMainImage = filteredImages[0].name;
      }

      this.setState(state => ({
        imagesToUpload: [...state.imagesToUpload, ...filteredImages],
        mainImage: newMainImage
      }));
    }
  }

  private getImageName(urlOrName: string) {
    const regExp: RegExp = new RegExp(".+/(.+\..+)");
    const groups = regExp.exec(urlOrName);

    return groups && groups[1] || urlOrName; // if first capture group is null - then it is name
  }

  private onValueChange(key: string, value: string | boolean) {
    this.setState({
      ...this.state,
      [key]: value
    });
  }

  private onSubmit(e: any) {
    e.preventDefault();

    const validation = this.validator.validate(this.state);

    this.setState(state => ({
      ...state,
      validation
    }));

    if (validation.isValid) {
      const formData = new FormData();

      if (this.state.mainImage) {
        const imageName = this.getImageName(this.state.mainImage);
        formData.append('mainImageName', imageName);
      }

      for (let image of this.state.imagesToUpload) {
        formData.append('imagesToUpload', image);
      }

      for (let image of this.state.imagesToDelete) {
        formData.append('imagesToDelete', image);
      }

      for (let key in this.state) {
        let shouldPropertyBeAdded = this.SUBMIT_EXCLUDED.every(excluded => excluded !== key);
        if (shouldPropertyBeAdded) {
          formData.append(key, this.state[key]);
        }
      }

      this.props.onSubmit(formData);
    }
  }
}