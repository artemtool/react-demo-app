import * as React from 'react';
import * as styles from './TextArea.scss';

interface Props {
  [key: string]: any;
};

export default class TextArea extends React.PureComponent<Props> {
  render() {
    return (
      <textarea
        {...this.props}
        className={styles.textArea}
      />
    );
  }
}