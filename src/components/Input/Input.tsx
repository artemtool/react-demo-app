import * as React from 'react';
import * as styles from './Input.scss';

export default class Input extends React.PureComponent<any> {
  render() {
    const className = `${styles.input} ${this.props.className}`;

    return (
      <input
        {...this.props}
        className={className}
      />
    );
  }
}