export const received: string;
export const sent: string;
export const message: string;
export const dateStamp: string;
export const text: string;
export const senderName: string;
