export const messagesContainer: string;
export const rightPadding: string;
export const form: string;
export const textarea: string;
export const button: string;
export const loadingSpinner: string;
