import * as React from 'react';
import Message from './Message';
import { MessangerState } from './../../interfaces/messangerState';
import * as styles from './Messanger.scss';
import Button from '@material-ui/core/Button';
// import CircularProgress from '@material-ui/core/CircularProgress';

interface Props {
  state: MessangerState;
  senderId: string;
  senderLabel: string;
  receiverLabel: string;
  onSend: (text: string) => void;
  onLoad: () => void;
};

interface State {
  message: string;
}

export default class Messanger extends React.PureComponent<Props, State> {
  // private readonly spinner: React.ReactNode = <CircularProgress className={styles.loadingSpinner} />;
  private messagesContainerRef: any;

  constructor(props) {
    super(props);

    this.state = {
      message: ''
    };
    this.messagesContainerRef = React.createRef();
    this.onSend = this.onSend.bind(this);
    this.onMessageChange = this.onMessageChange.bind(this);
    this.onScroll = this.onScroll.bind(this);
  }

  public componentDidMount() {
    this.messagesContainerRef.current.addEventListener('scroll', this.onScroll);
  }

  public componentDidUpdate(prevProps: Props) {
    const lengthNow = this.props.state.messages.length;
    const lengthBefore = prevProps.state.messages.length;
    const lastMessageIdNow = lengthNow > 0 && this.props.state.messages[lengthNow - 1].id;
    const lastMessageIdBefore = lengthBefore > 0 && prevProps.state.messages[lengthBefore - 1].id;

    // Scroll to bottom only on initial load or if current user send message.
    if (lengthBefore === 0 || lastMessageIdNow !== lastMessageIdBefore){
      this.messagesContainerRef.current.scrollTo(0, this.messagesContainerRef.current.scrollHeight);
    }
  }

  public componentWillUnmount() {
    this.messagesContainerRef.current.removeEventListener('scroll', this.onScroll);
  }

  public render() {
    const messages = this.props.state.messages;
    const addRightPadding = this.messagesContainerRef.current &&
      this.messagesContainerRef.current.scrollHeight > this.messagesContainerRef.current.clientHeight;
    return (
      <div>
        <div
          className={`${styles.messagesContainer} ${addRightPadding && styles.rightPadding}`}
          ref={this.messagesContainerRef}
        >
          {
            messages.map(message =>
              <Message
                key={message.id}
                senderLabel={this.props.senderLabel}
                receiverLabel={this.props.receiverLabel}
                message={message}
              />
            )
          }
        </div>
        <form
          onSubmit={this.onSend}
          className={styles.form}
          noValidate
        >
          <textarea
            value={this.state.message}
            className={styles.textarea}
            onChange={this.onMessageChange}
          />
          <Button
            type="submit"
            variant="contained"
            color="primary"
            className={styles.button}
          >
            Send
          </Button>
        </form>
      </div>
    )
  }

  private onScroll(e) {
    if (this.messagesContainerRef.current.scrollTop <= 200) {
      this.props.onLoad();
    }
  }

  private onMessageChange(e: any) {
    this.setState({
      message: e.target.value
    });
  }

  private onSend(e: any) {
    e.preventDefault();

    this.props.onSend(this.state.message);
    this.setState({
      message: ''
    });
  }
}