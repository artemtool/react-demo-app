import * as React from 'react';
import { Message as MessageModel } from '../../models/Message';
import * as styles from './Message.scss';

interface Props {
  message: MessageModel;
  senderLabel: string;
  receiverLabel: string;
}

export default class Message extends React.PureComponent<Props> {
  render() {
    const message = this.props.message;
    return (
      <div className={message.isCurrentUserSender ? styles.sent : styles.received}>
        <div className={styles.message}>
          <div className={styles.senderName}>
            {message.isCurrentUserSender ? this.props.senderLabel : this.props.receiverLabel}
            <span className={styles.dateStamp}>{` at ${message.getDateLabel()}`}</span>
          </div>
          <div className={styles.text}>{message.text}</div>
        </div>
      </div>
    );
  }
}