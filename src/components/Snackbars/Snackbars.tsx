import * as React from 'react';
import { AppState } from '../../state/appState';
import { hideSnackbar } from '../../state/actions/overlays';
import { connect } from 'react-redux';
import { ISnackbar } from './../../interfaces/snackbar';
import { Snackbar as SnackbarType } from '../../enums/snackbar';
import Snackbar from './Snackbar';

interface Props {
  snackbar: ISnackbar;
  hideSnackbar: (type: SnackbarType) => void;
};

class Snackbars extends React.PureComponent<Props> {
  render() {
    const variant = this.getVariantByType(this.props.snackbar ? this.props.snackbar.type : null);

    return (
      <React.Fragment>
        {
          variant && (
            <Snackbar
              variant={variant}
              message={this.props.snackbar.message}
              onHide={() => this.hideSnackbar(this.props.snackbar.type)}
            />
          )
        }
      </React.Fragment>
    );

  }

  getVariantByType(type: SnackbarType | null): any {
    switch (type) {
      case SnackbarType.Success:
        return 'success';
      case SnackbarType.Error:
        return 'error';
      case SnackbarType.Info:
        return 'info';
      case SnackbarType.Warning:
        return 'warning';
      default:
        return null;
    }
  }

  hideSnackbar(type: SnackbarType) {
    this.props.hideSnackbar(type);
  }
}

const mapStateToProps = (state: AppState) => ({
  snackbar: state.overlays.currentSnackbar
});

const mapDispatchToProps = dispatch => ({
  hideSnackbar: type => dispatch(hideSnackbar(type))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Snackbars);