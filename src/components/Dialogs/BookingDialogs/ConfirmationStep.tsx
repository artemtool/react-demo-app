import * as React from 'react';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import * as styles from './BookingDialog.scss';
import { Link } from 'react-router-dom';
import * as routes from '../../../constants/routes';

export interface Props {
  isBookingSucceed: boolean;
  onHide: () => void;
};

export default class ConfirmationStep extends React.PureComponent<Props> {
  render() {
    return (
      <div className={styles.confirmationContainer}>
        <img src={this.props.isBookingSucceed ? 'images/accept-booking-tick.png' : 'images/booking-error.png'} />
        {this.props.isBookingSucceed && (
          <Typography className={styles.confirmationText}>
            Property was successfully booked.<br />
            You can view your bookings <Link to={routes.MY_BOOKINGS} onClick={this.props.onHide}><b>here</b></Link>.
          </Typography>
        )}
        {!this.props.isBookingSucceed && (
          <Typography className={styles.confirmationText}>
            Error occured. Try again, please.<br />
          </Typography>
        )}
        <Button
          variant="contained"
          color="primary"
          className={styles.confirmationButton}
          onClick={this.props.onHide}>
          {this.props.isBookingSucceed ? 'Ok, thanks' : 'Ok'}
        </Button>
      </div>
    );
  }
}