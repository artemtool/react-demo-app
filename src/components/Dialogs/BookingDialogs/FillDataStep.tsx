import * as React from 'react';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import * as styles from './BookingDialog.scss';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MaskedInput from 'react-text-mask';
import { BookPropertyRequest } from './../../../interfaces/dto/bookPropertyRequest';
import { User } from './../../../models/user';
import { PropertyCard } from './../../../models/propertyCard';

const PhoneNumberMask = props => {
  const { inputRef, ...other } = props;
  return (
    <MaskedInput
      {...other}
      ref={inputRef}
      mask={['(', 0, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/]}
      placeholderChar={'\u2000'}
      showMask
    />
  );
}

export interface Props {
  property: PropertyCard;
  booking: BookPropertyRequest;
  currentUser: User;
  onNext: (updatedBooking: BookPropertyRequest) => void;
  onBack: (updatedBooking: BookPropertyRequest) => void;
};

export interface State {
  guestName: string;
  guestPhoneNumber: string;
  guestsAmount: number;
}

export default class FillDataStep extends React.PureComponent<Props, State> {

  daysAmount: number;
  sleeps: Array<number>;

  constructor(props: Props) {
    super(props);
    this.daysAmount = this.getCheckInOutDiff(this.props.booking.checkInDate, this.props.booking.checkOutDate);
    this.sleeps = [];
    for (let i = 0; i < this.props.property.sleeps; i++) {
      this.sleeps.push(i + 1);
    }

    const guestName = this.props.booking.guestName || this.props.currentUser && this.props.currentUser.name;
    const guestPhoneNumber = this.props.currentUser && this.props.currentUser.phoneNumber1 || '';
    
    this.state = {
      guestName,
      guestPhoneNumber,
      guestsAmount: this.props.booking.guestsAmount
    };
  }

  getCheckInOutDiff(checkIn: Date, checkOut: Date) {
    const diffInMs = checkOut.valueOf() - checkIn.valueOf();
    return diffInMs / (1000 * 60 * 60 * 24);
  }

  onFormValueChange(key: string, value: string) {
    this.setState({
      ...this.state,
      [key]: value
    });
  }

  onSubmit = (e: any) => {
    e.preventDefault();

    this.props.onNext({
      ...this.props.booking,
      ...this.state
    });
  }

  render() {
    return (
      <React.Fragment>
        <div className={styles.propertyCard}>
          <div className={styles.propertyImgContainer}>
            <img src={this.props.property.getMainImageUrl()} className={styles.propertyImg} />
            <Typography>
              Total price<br />
              for {this.daysAmount} {this.daysAmount > 1 ? 'days' : 'day'}:<br />
              <b className={styles.propertyTotalPrice}>$ {this.props.property.dailyPrice * this.daysAmount}</b><br />
              ($ {this.props.property.dailyPrice} per day)
            </Typography>
          </div>
          <form
            onSubmit={this.onSubmit}
            className={styles.form}>
            <FormControl className={styles.formControl}>
              <TextField
                id="propertyId"
                label="Property Id"
                value={this.props.property.id}
                InputProps={{
                  readOnly: true,
                }}
              />
            </FormControl>
            <FormControl className={styles.formControl}>
              <TextField
                id="checkInDate"
                label="Check-in date"
                value={this.props.booking.checkInDate.toLocaleDateString('en-US')}
                InputProps={{
                  readOnly: true
                }}
              />
            </FormControl>
            <FormControl className={styles.formControl}>
              <TextField
                id="checkOutDate"
                label="Check-out date"
                value={this.props.booking.checkOutDate.toLocaleDateString('en-US')}
                InputProps={{
                  readOnly: true
                }}
              />
            </FormControl>
            <FormControl className={styles.formControl}>
              <TextField
                value={this.state.guestName}
                id="guestName"
                label="Guest name"
                onChange={(e) => this.onFormValueChange('guestName', e.target.value)}
              />
            </FormControl>
            <FormControl className={styles.formControl}>
              <InputLabel htmlFor="guestPhoneNumber">Guest phone number</InputLabel>
              <Input
                value={this.state.guestPhoneNumber}
                id="guestPhoneNumber"
                inputComponent={PhoneNumberMask}
                onChange={(e) => this.onFormValueChange('guestPhoneNumber', e.target.value)}
              />
            </FormControl>
            <FormControl className={styles.formControl}>
              <InputLabel shrink htmlFor="guestsAmount">Guests amount</InputLabel>
              <Select
                value={this.state.guestsAmount}
                input={<Input name="Guests amount" id="guestsAmount" />}
                onChange={(e) => this.onFormValueChange('guestsAmount', e.target.value)}
              >
                {this.sleeps.map((v, i) => <MenuItem key={i + 1} value={i + 1}>{i + 1}</MenuItem>)}
              </Select>
            </FormControl>
            <Button
              className={styles.button}
              variant="contained"
              color="primary"
              type="submit">
              Next
            </Button>
          </form>
        </div>
      </React.Fragment>
    );
  }
}