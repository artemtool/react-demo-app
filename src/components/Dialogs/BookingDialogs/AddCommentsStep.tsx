import * as React from 'react';
import Button from '@material-ui/core/Button';
import * as styles from './BookingDialog.scss';
import TextField from '@material-ui/core/TextField';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import FormControl from '@material-ui/core/FormControl';
import { BookPropertyRequest } from './../../../interfaces/dto/bookPropertyRequest';

export interface Props {
  booking: BookPropertyRequest;
  onBack: (booking: BookPropertyRequest) => void;
  onComplete: (booking: BookPropertyRequest) => void;
};

interface State {
  comments: string;
};

export default class AddCommentsStep extends React.PureComponent<Props, State> {

  constructor(props: Props) {
    super(props);
    this.state = {
      comments: this.props.booking.comments
    }
  }

  onFormValueChange(key: string, value: string) {
    this.setState({
      ...this.state,
      [key]: value
    });
  }

  onSubmit = (e: any) => {
    e.preventDefault();

    this.props.onComplete({
      ...this.props.booking,
      comments: this.state.comments
    });
  }

  onBack = () => {
    this.props.onBack({
      ...this.props.booking,
      comments: this.state.comments
    });
  }

  render() {
    return (
      <form
        onSubmit={this.onSubmit}
        className={styles.form}>
        <FormControl className={styles.formControl}>
          <TextField
            id="comments"
            label="Additional info (optional)"
            multiline={true}
            rows={4}
            value={this.state.comments}
            onChange={(e) => { this.onFormValueChange('comments', e.target.value) }}
          />
          <div className={styles.btnContainer}>
            <Button
              color="primary"
              type="button"
              onClick={this.onBack}>
              <ArrowBackIcon />
            </Button>
            <Button
              variant="contained"
              color="primary"
              type="submit">
              Confirm booking
          </Button>
          </div>
        </FormControl>
      </form>
    );
  }
}