import * as React from 'react';
import Dialog from '@material-ui/core/Dialog';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import { PropertyCard } from './../../../models/propertyCard';
import { BookPropertyRequest } from './../../../interfaces/dto/bookPropertyRequest';
import * as styles from './BookingDialog.scss';
import FillDataStep from './FillDataStep';
import AddCommentsStep from './AddCommentsStep';
import ConfirmationStep from './ConfirmationStep';
import LinearProgress from '@material-ui/core/LinearProgress';
import { connect } from 'react-redux';
import { bookProperty } from '../../../state/actions/userBookings';
import { AppState } from '../../../state/appState';
import { User } from '../../../models/user';
import { Props as FillDataProps } from './FillDataStep';
import { Props as AddCommentsProps } from './AddCommentsStep';
import { Props as ConfirmationProps } from './ConfirmationStep';
import { Dialog as DialogType } from '../../../enums/dialog';

type Props = {
  user: User,
  dialog: {
    type: DialogType,
    payload: {
      property: PropertyCard,
      checkInDate: Date,
      checkOutDate: Date
    }
  },
  onHide: () => void,
  bookProperty: (booking: BookPropertyRequest) => Promise<void>
};

type State = {
  activeStep: number,
  isBookingSaving: boolean
};

class BookingDialog extends React.PureComponent<Props, State> {

  booking: BookPropertyRequest;
  isBookingSucceed: boolean;

  constructor(props: Props) {
    super(props);

    this.state = {
      activeStep: 0,
      isBookingSaving: false
    };

    this.booking = {
      propertyId: this.props.dialog.payload.property.id,
      checkInDate: this.props.dialog.payload.checkInDate,
      checkOutDate: this.props.dialog.payload.checkOutDate,
      guestName: '',
      guestPhoneNumber: '(0  )    -  -  ',
      guestsAmount: 1,
      comments: ''
    };
  }

  onNext = (updatedBooking: BookPropertyRequest) => {
    this.booking = updatedBooking;

    this.setState(state => ({
      activeStep: state.activeStep + 1,
    }));
  };

  onBack = (updatedBooking: BookPropertyRequest) => {
    this.booking = updatedBooking;

    this.setState(state => ({
      activeStep: state.activeStep - 1,
    }));
  };

  onComplete = (booking: BookPropertyRequest) => {
    this.setState({
      isBookingSaving: true
    }, () => {
      this.props.bookProperty(booking)
        .then(() => this.onCompleteResult(true))
        .catch(() => this.onCompleteResult(false));
    });
  };

  onCompleteResult(isBookingSucceed: boolean) {
    this.setState({ isBookingSaving: false });
    this.isBookingSucceed = isBookingSucceed;

    this.setState(state => ({
      activeStep: state.activeStep + 1,
    }));
  }

  getSteps() {
    return ['Fill up details', 'Add comments', 'Confirm booking'];
  }

  getStepContent(step: number): any {
    switch (step) {
      case 0:
        return FillDataStep;
      case 1:
        return AddCommentsStep;
      case 2:
        return ConfirmationStep;
    }
  }

  getStepContentProps(step: number): any {
    switch (step) {
      case 0: {
        const props: FillDataProps = {
          property: this.props.dialog.payload.property,
          booking: this.booking,
          currentUser: this.props.user,
          onNext: this.onNext,
          onBack: this.onBack
        };

        return props;
      }
      case 1: {
        const props: AddCommentsProps = {
          booking: this.booking,
          onComplete: this.onComplete,
          onBack: this.onBack
        };

        return props;
      }
      case 2: {
        const props: ConfirmationProps = {
          isBookingSucceed: this.isBookingSucceed,
          onHide: this.props.onHide
        };

        return props;
      }
    }
  }

  render() {
    const steps = this.getSteps();

    return (
      <Dialog open={true} onClose={this.props.onHide}>
        {this.state.isBookingSaving && <LinearProgress color="secondary" className={styles.linearProgress} />}
        <div className={styles.stepperWrapper}>
          <Stepper activeStep={this.state.activeStep} orientation="vertical">
            {steps.map((label, index) => {
              const StepContentComponent = this.getStepContent(index);
              const stepProps = this.getStepContentProps(index);

              return (
                <Step key={label}>
                  <StepLabel>{label}</StepLabel>
                  <StepContent>
                    <StepContentComponent {...stepProps} />
                  </StepContent>
                </Step>
              );
            })}
          </Stepper>
        </div>
      </Dialog>
    );
  }
}

const mapStateToProps = (state: AppState) => ({
  user: state.user.authUser
});

const mapDispatchToProps = dispatch => ({
  bookProperty: booking => dispatch(bookProperty(booking)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BookingDialog);