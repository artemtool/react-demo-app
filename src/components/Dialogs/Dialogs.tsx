import * as React from 'react';
import { IDialog } from './../../interfaces/dialog';
import { AppState } from '../../state/appState';
import { hideDialog } from '../../state/actions/overlays';
import { connect } from 'react-redux';
import BookingDialog from './BookingDialogs/BookingDialog';
import { Dialog } from '../../enums/dialog';

interface Props {
  dialog: IDialog;
  hideDialog: (type: Dialog) => void;
};

class Dialogs extends React.PureComponent<Props> {
  render() {
    const DialogComponent = this.getDialogByType(this.props.dialog ? this.props.dialog.type : null);

    return (
      <React.Fragment>
        {
          DialogComponent && (
            <DialogComponent
              dialog={this.props.dialog}
              onHide={() => this.hideDialog(this.props.dialog.type)}
            />
          )
        }
      </React.Fragment>
    );
  }

  getDialogByType(type: Dialog | null): any {
    switch (type) {
      case Dialog.Booking:
        return BookingDialog;
      default:
        return null;
    }
  }

  hideDialog(type: Dialog) {
    this.props.hideDialog(type);
  }
}

const mapStateToProps = (state: AppState) => ({
  dialog: state.overlays.currentDialog
});

const mapDispatchToProps = dispatch => ({
  hideDialog: type => dispatch(hideDialog(type))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Dialogs);