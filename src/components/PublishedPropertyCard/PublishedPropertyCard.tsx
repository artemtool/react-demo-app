import * as React from 'react';
import { PropertyCard } from './../../models/propertyCard';
import * as styles from './PublishedPropertyCard.scss';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
import * as routes from '../../constants/routes';
import DateRangeIcon from '@material-ui/icons/DateRange';
import EditIcon from '@material-ui/icons/Edit';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import Tooltip from '@material-ui/core/Tooltip';
import { PropertyStatus } from '../../enums/propertyStatus';
import Button from '@material-ui/core/Button';

interface Props {
  property: PropertyCard;
};

export default class PublishedPropertyCard extends React.PureComponent<Props> {

  getPropertyStatusClass(status: PropertyStatus): string {
    switch (status) {
      case PropertyStatus.Active:
        return styles.propertyStatusActive;
      case PropertyStatus.Inactive:
        return styles.propertyStatusInactive;
      case PropertyStatus.WaitingForModeration:
        return styles.propertyStatusModeration;
      default:
        return styles.propertyStatusActive;
    }
  }

  render() {
    const statusClassName = this.getPropertyStatusClass(this.props.property.status);
    const status = this.props.property.getStatus();
    return (
      <div className={styles.propertyCard}>
        <Link to={`${routes.EDIT_PROPERTY}/${this.props.property.id}`}>
          <Button
            variant="fab"
            color="secondary"
            className={styles.editButton}
          >
            <EditIcon />
          </Button>
        </Link>
        <div className={styles.propertyImg}>
          <img src={this.props.property.getMainImageUrl()}></img>
        </div>
        <div className={styles.propertyDetails}>
          <Typography variant="title">
            <Link
              to={`${routes.PROPERTY}/${this.props.property.id}`}
              className={styles.propertyLink}
            >
              {this.props.property.city.name}, {this.props.property.address}
            </Link>
            <Tooltip title={status.tooltip} placement="top">
              <span className={`${statusClassName} ${styles.propertyStatus}`}>{status.label}</span>
            </Tooltip>
          </Typography>
          <Typography variant="subheading">{this.props.property.title} - {this.props.property.getPropertyTypeLabel()}</Typography>
          <div className={styles.propertyFeatures}>
            <Typography className={styles.propertyFeature} variant="body1">Sleeps: {this.props.property.sleeps}</Typography>
            <Typography className={styles.propertyFeature} variant="body1">Rooms: {this.props.property.rooms}</Typography>
            <Typography className={styles.propertyFeature} variant="body1">Area: {this.props.property.area}</Typography>
            <Typography className={styles.propertyFeature} variant="body1">Floor: {this.props.property.floor}</Typography>
          </div>
          <div className={styles.propertyIconizedFeatures}>
            <Tooltip title="Price per day / week" placement="top-start">
              <div className={styles.propertyIconizedFeature}>
                <AttachMoneyIcon />
                <Typography variant="body2">{this.props.property.dailyPrice} / {this.props.property.weeklyPrice}</Typography>
              </div>
            </Tooltip>
            <Tooltip title="Publish date" placement="top-start">
              <div className={styles.propertyIconizedFeature}>
                <DateRangeIcon />
                <Typography className={styles.publishDate} variant="body2">{this.props.property.getFormattedPublishDate()}</Typography>
              </div>
            </Tooltip>
          </div>
        </div>
        <div className={styles.propertyCtrls}>
          {this.props.children}
        </div>
      </div >
    );
  }
}