import * as React from 'react';

interface Props {
  onScroll: Function;
  isEmpty: boolean;
  isLoading: boolean;
  canLoadMore: boolean;
  loadingIndicator: React.ReactNode;
  emptyMock: React.ReactNode;
};

export default class InfiniteScrollList extends React.PureComponent<Props> {

  constructor(props) {
    super(props);

    this.onScroll = this.onScroll.bind(this);
  }

  public componentDidMount() {
    window.addEventListener('scroll', this.onScroll);
  }

  public componentWillUnmount() {
    window.removeEventListener('scroll', this.onScroll);
  }

  public render() {
    const spinnerStyles: any = {};
    if (!this.props.canLoadMore) {
      spinnerStyles.display = 'none';
    } else if (!this.props.isLoading) {
      spinnerStyles.opacity = 0;
    }

    return (
      <React.Fragment>
        {this.props.isEmpty && !this.props.isLoading && this.props.emptyMock}
        {!this.props.isEmpty && this.props.children}
        <div style={spinnerStyles}>
          {this.props.loadingIndicator}
        </div>
      </React.Fragment>
    );
  }

  private onScroll() {
    if (window.innerHeight + window.scrollY >= document.body.offsetHeight - 200) {
      this.props.onScroll();
    }
  }
}