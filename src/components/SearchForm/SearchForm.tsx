import * as React from 'react';
import { connect } from 'react-redux';
import Select from 'react-select'
import DatePicker from 'material-ui-pickers/DatePicker';
import MuiPickersUtilsProvider from 'material-ui-pickers/utils/MuiPickersUtilsProvider';
import DateFnsUtils from 'material-ui-pickers/utils/date-fns-utils';
import Button from '@material-ui/core/Button';
import * as styles from './SearchForm.scss';
import { fetchSuggestionsIfNeeded, updateParameters } from './../../state/actions/searchFilters';
import SearchParametersParser from './../../models/searchParametersParser';
import * as routes from '../../constants/routes';
import { State as SearchFiltersState } from '../../state/reducers/searchFilters';
import { mapCityIdToSelectOption, mapValueToSelectOption } from './../../common/selectMappers';
import FormValidator from './../../common/formValidator';
import * as validator from 'validator';
import Typography from '@material-ui/core/Typography';
import Label from './../Label/Label';
import { SelectOption } from './../../models/selectOption';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { SearchParameters } from '../../interfaces/SearchParameters';
import { AppState } from '../../state/appState';
import { propertyFeatures, sleepsOptionList, propertyTypesOptionList, roomsOptionList } from './../../constants/property';
import Input from './../Input/Input';

interface Props {
  history: any;
  aside: boolean;
  expanded: boolean;
  selectedCityOption: Object;
  searchFilters: SearchFiltersState;
  fetchSuggestions: () => Promise<void>;
  updateParameters: (params: SearchParameters) => void;
};

interface State {
  validation: any;
  filtersExpanded: boolean;
};

class SearchForm extends React.PureComponent<Props, State> {

  validator: FormValidator;
  orderByOptionList: Array<SelectOption> = [
    {
      label: 'Price per day',
      value: 'DailyPrice'
    },
    {
      label: 'Price per week',
      value: 'WeeklyPrice'
    },
    {
      label: 'Publish date',
      value: 'PublishDate'
    }
  ];
  orderOptionList: Array<SelectOption> = [
    {
      label: 'Descending',
      value: true
    },
    {
      label: 'Ascending',
      value: false
    }
  ];

  constructor(props: Props) {
    super(props);

    this.validator = new FormValidator([
      {
        field: 'cityId',
        method: validator.isEmpty,
        validWhen: false,
        message: 'Please select a city to search by.'
      },
      {
        field: 'checkInDate',
        method: this.isValidDateRange,
        validWhen: true,
        message: 'Please provide a valid date range.'
      }
    ]);

    this.state = {
      validation: this.validator.valid(),
      filtersExpanded: this.props.expanded
    };
  }

  componentDidMount() {
    this.props.fetchSuggestions();
  }

  render() {
    const validation = this.state.validation;
    const params = this.props.searchFilters.parameters;
    const checkFeaturesAmount = this.getCheckedFeaturesAmount();

    return (
      <form
        className={`${styles.form} ${this.props.aside && styles.aside}`}
        onSubmit={this.onSubmit}
      >
        <Select
          options={this.props.searchFilters.suggestions}
          value={this.props.selectedCityOption}
          isLoading={this.props.searchFilters.isLoading}
          isClearable={true}
          placeholder={'Choose city...'}
          className={validation.cityId.isInvalid && styles.errorSelect}
          onChange={e => this.onValueChange('cityId', e ? e.value : null)}
        />
        {validation.cityId.isInvalid && <Typography className={styles.errorLabel}>{validation.cityId.message}</Typography>}

        <div className={styles.fieldset}>
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <DatePicker
              disablePast={true}
              keyboard
              value={params.checkInDate}
              label="Check in"
              format="dd/MM/yy"
              placeholder="10/10/18"
              mask={value => (value ? [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/] : [])}
              disableOpenOnEnter
              animateYearScrolling={false}
              onChange={value => this.onDateChange('checkInDate', value)}
            />
            <DatePicker
              disablePast={true}
              keyboard
              value={params.checkOutDate}
              label="Check out"
              format="dd/MM/yy"
              placeholder="10/10/18"
              mask={value => (value ? [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/] : [])}
              disableOpenOnEnter
              animateYearScrolling={false}
              onChange={value => this.onDateChange('checkOutDate', value)}
            />
          </MuiPickersUtilsProvider>
        </div>
        {validation.checkInDate.isInvalid && <Typography className={styles.errorLabel}>{validation.checkInDate.message}</Typography>}

        <div className={`${styles.hiddenFilters} ${this.state.filtersExpanded && styles.expanded}`}>
          <div className={styles.fieldset}>
            <div className={styles.field}>
              <Label label="Min daily price ($)" />
              <Input
                id="minPrice"
                name="minPrice"
                type="number"
                placeholder="Min price"
                value={params.minPrice || ''}
                onChange={e => this.onValueChange('minPrice', e.target.value)}
              />
            </div>
            <div className={styles.field}>
              <Label label="Max daily price ($)" />
              <Input
                id="maxPrice"
                name="maxPrice"
                type="number"
                placeholder="Max price"
                value={params.maxPrice || ''}
                onChange={e => this.onValueChange('maxPrice', e.target.value)}
              />
            </div>
          </div>

          <div className={styles.fieldset}>
            <div className={styles.field}>
              <Label label="Sleeps" />
              <Select
                options={sleepsOptionList}
                isClearable={true}
                placeholder={'Sleeps'}
                value={mapValueToSelectOption(params.sleeps, sleepsOptionList)}
                onChange={e => this.onValueChange('sleeps', e ? e.value : null)}
              />
            </div>
            <div className={styles.field}>
              <Label label="Property type" />
              <Select
                options={propertyTypesOptionList}
                isClearable={true}
                placeholder={'Select type'}
                value={mapValueToSelectOption(params.propertyType, propertyTypesOptionList)}
                onChange={e => this.onValueChange('propertyType', e ? e.value : null)}
              />
            </div>
          </div>

          <div className={styles.fieldset}>
            <div className={styles.field}>
              <Label label="Min rooms" />
              <Select
                options={roomsOptionList}
                isClearable={true}
                placeholder={'Min rooms'}
                value={mapValueToSelectOption(params.minRooms, roomsOptionList)}
                onChange={e => this.onValueChange('minRooms', e ? e.value : null)}
              />
            </div>
            <div className={styles.field}>
              <Label label="Max rooms" />
              <Select
                options={roomsOptionList}
                isClearable={true}
                placeholder={'Max rooms'}
                value={mapValueToSelectOption(params.maxRooms, roomsOptionList)}
                onChange={e => this.onValueChange('maxRooms', e ? e.value : null)}
              />
            </div>
          </div>

          <div className={styles.fieldset}>
            <div className={styles.field}>
              <Label label="Order by" />
              <Select
                options={this.orderByOptionList}
                isSearchable={false}
                value={mapValueToSelectOption(params.orderBy, this.orderByOptionList)}
                onChange={e => this.onValueChange('orderBy', e.value)}
              />
            </div>
            <div className={styles.field}>
              <Label label="Order" />
              <Select
                options={this.orderOptionList}
                isSearchable={false}
                value={mapValueToSelectOption(params.isDesc, this.orderOptionList)}
                onChange={e => this.onValueChange('isDesc', e.value)}
              />
            </div>
          </div>
          <div className={styles.fieldset}>
            <div className={styles.field}>
              <Label label={`Check the things you need (${checkFeaturesAmount})`} />
              <div className={styles.featureList}>
                {
                  propertyFeatures.map(feature =>
                    <FormControlLabel
                      key={feature.name}
                      control={
                        <Checkbox
                          color="primary"
                          checked={params[feature.name] === true}
                          onChange={e => this.onValueChange(feature.name, e.target.checked ? true : null)}
                        />
                      }
                      label={feature.label}
                    />
                  )
                }
              </div>
            </div>
          </div>
        </div>
        <Typography
          color="primary"
          component="a"
          className={styles.showMore}
          onClick={this.toggleFilters}
        >
          {`... show ${this.state.filtersExpanded ? 'less' : 'more'} filters`}
        </Typography>
        <Button
          variant="contained"
          className={styles.searchButton}
          color="primary"
          type="submit">
          Search
        </Button>
      </form>
    );
  }

  toggleFilters = () => {
    this.setState(state => ({
      ...state,
      filtersExpanded: !state.filtersExpanded
    }));
  }

  isValidDateRange = (checkInDate, state) => {
    return Date.parse(checkInDate) < Date.parse(state.checkOutDate);
  }

  onDateChange = (key: string, date: Date) => {
    this.resetHours(date);

    this.props.updateParameters({
      ...this.props.searchFilters.parameters,
      [key]: date
    });
  }

  onValueChange = (key: string, value: any) => {
    this.props.updateParameters({
      ...this.props.searchFilters.parameters,
      [key]: value
    });
  }

  resetHours(date: Date) {
    date.setHours(0, 0, 0, 0);
  }

  getCheckedFeaturesAmount(): number {
    return propertyFeatures.reduce((counter, feature) => {
      return this.props.searchFilters.parameters[feature.name]
        ? counter + 1
        : counter;
    }, 0);
  }

  onSubmit = (event) => {
    event.preventDefault();

    const params = this.props.searchFilters.parameters;
    const validation = this.validator.validate({
      cityId: params.cityId || '',
      checkInDate: params.checkInDate,
      checkOutDate: params.checkOutDate
    });

    this.setState({ validation });

    if (validation.isValid) {
      let searchParamsParser = new SearchParametersParser();
      let queryString = searchParamsParser.getQueryString(params);
      this.props.history.push(`${routes.SEARCH}?${queryString}`);
    }
  }
}

const mapStateToProps = (state: AppState) => ({
  searchFilters: state.searchFilters,
  selectedCityOption: mapCityIdToSelectOption(
    state.searchFilters.parameters.cityId,
    state.searchFilters.suggestions
  )
})

const mapDispatchToProps = dispatch => ({
  fetchSuggestions: () => dispatch(fetchSuggestionsIfNeeded()),
  updateParameters: filters => dispatch(updateParameters(filters))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchForm);