import * as React from 'react';
import Typography from '@material-ui/core/Typography';
import * as styles from './Label.scss';

export default class Label extends React.PureComponent<any> {
  render() {
    const className = `${styles.label} ${this.props.className}`;

    return (
      <label {...this.props}>
        <Typography
          variant="body2"
          className={className}
        >
          {this.props.label}
        </Typography>
      </label>
    );
  }
}