import * as React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
// import StarIcon from '@material-ui/icons/Star';
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import DashboardIcon from '@material-ui/icons/Dashboard';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import SettingsIcon from '@material-ui/icons/Settings';
import ListIcon from '@material-ui/icons/List';
import HomeIcon from '@material-ui/icons/Home';
import Divider from '@material-ui/core/Divider';
import Avatar from '@material-ui/core/Avatar';
import { NavLink } from 'react-router-dom';

import * as styles from './DrawerMenu.scss';
import { connect } from 'react-redux';
import { signOut, trySignIn } from './../../state/actions/user';
import { compose } from 'recompose';
import withAuthentication from './../withAuthentication';
import { AppState } from '../../state/appState';
import * as routes from '../../constants/routes';
import Collapse from '@material-ui/core/Collapse';
import List from '@material-ui/core/List';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import AddIcon from '@material-ui/icons/Add';
import PeopleIcon from '@material-ui/icons/People';
import { NewPropertyBookings } from '../../interfaces/newIncomingBookings';

interface Props {
  isLoggedIn: boolean;
  userEmail: string;
  avatarLetters: string;
  imageUrl: string;
  newIncomingBookingsCount: number;
  signOut: () => Promise<void>;
}

interface State {
  partnerSubmenuOpened: boolean;
}

class DrawerMenu extends React.PureComponent<Props, State> {

  constructor(props) {
    super(props);
    this.state = {
      partnerSubmenuOpened: true
    }
  }

  collapsePartnerSubmenu = () => {
    this.setState(state => ({
      partnerSubmenuOpened: !state.partnerSubmenuOpened
    }));
  }

  render() {
    let authPart;
    if (this.props.isLoggedIn) {
      authPart =
        <React.Fragment>
          <ListItem>
            <ListItemIcon>
              <Avatar src={this.props.imageUrl}>{this.props.avatarLetters}</Avatar>
            </ListItemIcon>
            <ListItemText primary={`You are logged in as:\n${this.props.userEmail}`} />
          </ListItem>
          <Divider />
          <ListItem button onClick={this.collapsePartnerSubmenu}>
            <ListItemIcon>
              <DashboardIcon />
            </ListItemIcon>
            <ListItemText primary="Partner cockpit" />
            {this.state.partnerSubmenuOpened ? <ExpandLess /> : <ExpandMore />}
          </ListItem>
          <Collapse in={this.state.partnerSubmenuOpened} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
              <NavLink to={routes.PUBLISHED_PROPERTIES} activeClassName={styles.activeListItem}>
                <ListItem button className={styles.nested}>
                  <ListItemIcon>
                    <HomeIcon />
                  </ListItemIcon>
                  <ListItemText inset primary="Published properties" />
                </ListItem>
              </NavLink>
              <NavLink to={routes.PUBLISH_PROPERTY} activeClassName={styles.activeListItem}>
                <ListItem button className={styles.nested}>
                  <ListItemIcon>
                    <AddIcon />
                  </ListItemIcon>
                  <ListItemText inset primary="Publish property" />
                </ListItem>
              </NavLink>
              <NavLink to={routes.INCOMING_BOOKINGS} activeClassName={styles.activeListItem}>
                <ListItem button className={styles.nested}>
                  <ListItemIcon>
                    <PeopleIcon />
                  </ListItemIcon>
                  <ListItemText inset>
                    Incoming bookings
                    {
                      this.props.newIncomingBookingsCount > 0 && (
                        <span className={styles.counterLabel}>{this.props.newIncomingBookingsCount}</span>
                      )
                    }
                  </ListItemText>
                </ListItem>
              </NavLink>
            </List>
          </Collapse>
          <NavLink to={routes.MY_BOOKINGS} activeClassName={styles.activeListItem}>
            <ListItem button>
              <ListItemIcon>
                <ListIcon />
              </ListItemIcon>
              <ListItemText primary="My bookings" />
            </ListItem>
          </NavLink>
          <NavLink to={routes.SETTINGS} activeClassName={styles.activeListItem}>
            <ListItem button>
              <ListItemIcon>
                <SettingsIcon />
              </ListItemIcon>
              <ListItemText primary="Settings" />
            </ListItem>
          </NavLink>
          {/* <Link to={routes.FAVORITES}>
            <ListItem button>
              <ListItemIcon>
                <StarIcon />
              </ListItemIcon>
              <ListItemText primary="Favorites" />
            </ListItem>
          </Link> */}
          <ListItem button onClick={this.props.signOut}>
            <ListItemIcon>
              <ExitToAppIcon />
            </ListItemIcon>
            <ListItemText primary="Sign Out" />
          </ListItem>
        </React.Fragment>;
    } else {
      authPart =
        <React.Fragment>
          <NavLink to={routes.SIGN_IN} activeClassName={styles.activeListItem}>
            <ListItem button>
              <ListItemIcon>
                <VpnKeyIcon />
              </ListItemIcon>
              <ListItemText primary="Sign In" />
            </ListItem>
          </NavLink>
          <NavLink to={routes.SIGN_UP} activeClassName={styles.activeListItem}>
            <ListItem button>
              <ListItemIcon>
                <PersonAddIcon />
              </ListItemIcon>
              <ListItemText primary="Sign Up" />
            </ListItem>
          </NavLink>
        </React.Fragment>;
    }

    return (
      <React.Fragment>
        {authPart}
        {this.props.children}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state: AppState) => ({
  isLoggedIn: !!state.user.authUser,
  userEmail: state.user.authUser
    ? state.user.authUser.email
    : null,
  avatarLetters: state.user.authUser
    ? state.user.authUser.getAvatarLetters()
    : null,
  imageUrl: state.user.authUser
    ? state.user.authUser.getImageUrl()
    : null,
  newIncomingBookingsCount: state.incomingBookings.newPropertyBookings
    .reduce((total: number, newPropertyBookings: NewPropertyBookings) => {
      return total + newPropertyBookings.count;
    }, 0)
});

const mapDispatchToProps = dispatch => ({
  signOut: () => dispatch(signOut()),
  trySignIn: () => dispatch(trySignIn())
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withAuthentication
)(DrawerMenu);