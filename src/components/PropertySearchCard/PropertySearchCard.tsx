import * as React from 'react';
import { Link } from 'react-router-dom';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
// import StarIcon from '@material-ui/icons/Star';
import * as styles from './PropertySearchCard.scss';
import { PropertyCard } from './../../models/propertyCard';
import * as routes from './../../constants/routes';
import Tooltip from '@material-ui/core/Tooltip';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import DateRangeIcon from '@material-ui/icons/DateRange';

interface Props {
  property: PropertyCard;
  children: React.ReactNode;
  className: string;
}

export default class PropertySearchCard extends React.PureComponent<Props> {
  render() {
    const cardClassName = `${styles.propertyCard} ${this.props.className}`;
    return (
      <Card className={cardClassName}>
        {this.props.children}
        <CardMedia className={styles.propertyCardImage}
          image={this.props.property.getMainImageUrl()}
          title={this.props.property.title}
        />
        <CardContent className={styles.propertyCardContent}>
          <div className={styles.contentHeadline}>
            <Typography
              gutterBottom
              variant="headline"
              component="h2"
            >
              <Link to={`${routes.PROPERTY}/${this.props.property.id}`}>
                {this.props.property.getPropertyTypeLabel()} on {this.props.property.address}
              </Link>
            </Typography>
            <Typography variant="subheading">{this.props.property.title}</Typography>
          </div>
          <div className={styles.propertyFeatures}>
            <Typography className={styles.propertyFeature} variant="body1">Sleeps: {this.props.property.sleeps}</Typography>
            <Typography className={styles.propertyFeature} variant="body1">Rooms: {this.props.property.rooms}</Typography>
            <Typography className={styles.propertyFeature} variant="body1">Area: {this.props.property.area}</Typography>
            <Typography className={styles.propertyFeature} variant="body1">Floor: {this.props.property.floor}</Typography>
          </div>
          <div className={styles.propertyIconizedFeatures}>
            <Tooltip title="Price per day / week" placement="top-start">
              <div className={styles.propertyIconizedFeature}>
                <AttachMoneyIcon />
                <Typography variant="body2">{this.props.property.dailyPrice} / {this.props.property.weeklyPrice}</Typography>
              </div>
            </Tooltip>
            <Tooltip title="Publish date" placement="top-start">
              <div className={styles.propertyIconizedFeature}>
                <DateRangeIcon />
                <Typography className={styles.publishDate} variant="body2">{this.props.property.getFormattedPublishDate()}</Typography>
              </div>
            </Tooltip>
          </div>
        </CardContent>
      </Card>
    );
  }
}