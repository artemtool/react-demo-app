import * as React from 'react';
import * as styles from './LoadingButton.scss';
import Button from '@material-ui/core/Button';
import LinearProgress from '@material-ui/core/LinearProgress';

export default class LoadingButton extends React.PureComponent<any> {
  
  render() {
    const { isLoading, ...restProps }  = this.props;

    return (
      <Button
        {...restProps}
        disabled={isLoading}
      >
        {this.props.children}
        {this.props.isLoading && <LinearProgress className={styles.buttonProgress} />}
      </Button>
    );
  }
}