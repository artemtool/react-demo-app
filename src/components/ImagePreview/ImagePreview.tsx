import * as React from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';
import * as styles from './ImagePreview.scss';
import CloseIcon from '@material-ui/icons/Close';
import CheckIcon from '@material-ui/icons/Check';
import Button from '@material-ui/core/Button';

interface Props {
  image: File | string;
  checked?: number;
  editable?: number;
  imageClassName?: string;
  containerClassName?: string;
  showProgress?: boolean;
  onCheck?: () => void;
  onDelete?: () => void;
};

interface State {
  url: string;
  completed: number;
};

export default class ImagePreview extends React.PureComponent<Props, State> {
  private fileReader: FileReader;

  constructor(props: Props) {
    super(props);

    this.state = {
      url: '/images/image-mock.png',
      completed: 0
    };
    this.onProgress = this.onProgress.bind(this);
    this.onLoad = this.onLoad.bind(this);
    this.onCheck = this.onCheck.bind(this);
    this.onDelete = this.onDelete.bind(this);
  }

  public componentDidMount() {
    this.loadImage();
  }

  public componentDidUpdate(prevProps: Props) {
    if (this.props.image !== prevProps.image) {
      this.removeListeners();
      this.loadImage();
    }
  }

  public componentWillUnmount() {
    this.removeListeners();
  }

  render() {
    const {
      imageClassName,
      containerClassName,
      showProgress
    } = this.props;

    const composedContainerClassName = `${styles.imageContainer} ${containerClassName}`;
    const composedImageClassName = `${styles.image} ${imageClassName}`;

    return (
      <div className={`${composedContainerClassName} ${this.props.editable && styles.editable} ${this.props.checked && styles.checked}`}>
        <img
          src={this.state.url}
          className={composedImageClassName}
        />
        {
          showProgress !== false && this.state.completed < 100 && (
            <LinearProgress
              variant="determinate"
              value={this.state.completed}
              className={styles.progressBar}
            />
          )
        }
        {
          this.props.editable && !this.props.checked && (
            <Button
              variant="fab"
              color="primary"
              className={styles.checkButton}
              onClick={this.onCheck}
            >
              <CheckIcon className={styles.buttonIcon} />
            </Button>
          )
        }
        {
          this.props.editable && (
            <Button
              variant="fab"
              color="secondary"
              className={styles.deleteButton}
              onClick={this.onDelete}
            >
              <CloseIcon className={styles.buttonIcon} />
            </Button>
          )
        }
      </div>
    )
  }

  private loadImage() {
    if (this.props.image instanceof File) {
      this.fileReader = new FileReader();
      this.fileReader.addEventListener('progress', this.onProgress);
      this.fileReader.addEventListener('load', this.onLoad);
      this.fileReader.readAsDataURL(this.props.image);
    } else {
      this.setState({
        url: this.props.image,
        completed: 100
      });
    }
  }

  private removeListeners() {
    if (this.props.image instanceof File) {
      this.fileReader.removeEventListener('progress', this.onProgress);
      this.fileReader.removeEventListener('load', this.onLoad);
    }
  }

  private onProgress(e: any) {
    if (!e.lengthComputable) {
      return;
    }

    let completed = (e.loaded / e.total) * 100;
    this.setState({
      completed
    });
  }

  private onLoad(e: any) {
    this.setState({
      url: e.target.result
    });
  }

  private onCheck() {
    if (typeof this.props.onCheck === 'function') {
      this.props.onCheck();
    }
  }

  private onDelete() {
    if (typeof this.props.onDelete === 'function') {
      this.props.onDelete();
    }
  }
}