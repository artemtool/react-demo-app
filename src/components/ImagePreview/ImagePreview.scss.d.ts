export const imageContainer: string;
export const checked: string;
export const editable: string;
export const deleteButton: string;
export const checkButton: string;
export const image: string;
export const progressBar: string;
export const buttonIcon: string;
