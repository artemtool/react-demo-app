import { IMessage } from './../interfaces/message';
import { MessageStatus } from '../enums/messageStatus';

export class Message {
  id: string;
  senderId: string;
  propertyBookingId: string;
  date: Date;
  text: string;
  isCurrentUserSender: boolean;
  status: MessageStatus;

  constructor(message: IMessage, isCurrentUserSender: boolean) {
    this.id = message.id;
    this.senderId = message.senderId;
    this.propertyBookingId = message.propertyBookingId;
    this.date = new Date(message.date);
    this.text = message.text;
    this.isCurrentUserSender = isCurrentUserSender;
    this.status = message.status;
  }

  getDateLabel(): string {
    return this.date.toLocaleString('en-US');
  }
}