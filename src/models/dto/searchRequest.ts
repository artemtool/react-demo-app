import { PageableListParameters } from './../../interfaces/pageableListParameters';
import { PropertyType } from '../../enums/propertyType';
import { SearchParameters } from '../../interfaces/SearchParameters';

export class SearchRequest {
  cityId: string;
  checkInDate: Date;
  checkOutDate: Date;
  listParameters: PageableListParameters;
  minPrice?: number;
  maxPrice?: number;
  minRooms?: number;
  maxRooms?: number;
  sleeps?: number;
  hasWiFi?: boolean;
  hasParkingPlace?: boolean;
  hasTV?: boolean;
  hasAirConditioner?: boolean;
  hasBedLinen?: boolean;
  hasComputer?: boolean;
  hasDishes?: boolean;
  hasHairDryer?: boolean;
  hasIroningStaff?: boolean;
  hasKettle?: boolean;
  hasMicrowaveOven?: boolean;
  hasTowels?: boolean;
  hasWashingMachine?: boolean;
  propertyType?: PropertyType;

  constructor(
    params: SearchParameters,
    perPageAmount: number,
    page: number,
  ) {
    this.cityId = params.cityId;
    this.checkInDate = params.checkInDate;
    this.checkOutDate = params.checkOutDate;
    this.minPrice = params.minPrice;
    this.maxPrice = params.maxPrice;
    this.minRooms = params.minRooms;
    this.maxRooms = params.maxRooms;
    this.sleeps = params.sleeps;
    this.hasWiFi = params.hasWiFi;
    this.hasParkingPlace = params.hasParkingPlace;
    this.hasTV = params.hasTV;
    this.hasAirConditioner = params.hasAirConditioner;
    this.hasBedLinen = params.hasBedLinen;
    this.hasComputer = params.hasComputer;
    this.hasDishes = params.hasDishes;
    this.hasHairDryer = params.hasHairDryer;
    this.hasIroningStaff = params.hasIroningStaff;
    this.hasKettle = params.hasKettle;
    this.hasMicrowaveOven = params.hasMicrowaveOven;
    this.hasTowels = params.hasTowels;
    this.hasWashingMachine = params.hasWashingMachine;
    this.propertyType = params.propertyType;
    this.listParameters = {
      page,
      perPageAmount,
      isDesc: params.isDesc,
      orderBy: params.orderBy
    };
  }
}