import { IPropertyBooking } from '../interfaces/propertyBooking';
import { PROPERTY } from '../constants/routes';

export class PropertyBooking {
  id: string;
  bookerId: string;
  propertyId: string;
  checkInDate: Date;
  checkOutDate: Date;
  guestName: string;
  guestPhoneNumber: string;
  guestsAmount: number;
  comments: string;
  dailyPrice: number;
  propertyMainImageName: string;
  propertyAddress: string;
  propertyOwnerName: string;
  propertyOwnerPhone: string;

  constructor(booking: IPropertyBooking) {
    this.id = booking.id;
    this.bookerId = booking.bookerId;
    this.propertyId = booking.propertyId;
    this.checkInDate = new Date(booking.checkInDate);
    this.checkOutDate = new Date(booking.checkOutDate);
    this.guestName = booking.guestName;
    this.guestPhoneNumber = booking.guestPhoneNumber;
    this.guestsAmount = booking.guestsAmount;
    this.comments = booking.comments;
    this.dailyPrice = booking.dailyPrice;
    this.propertyMainImageName = booking.propertyMainImageName;
    this.propertyAddress = booking.propertyAddress;
    this.propertyOwnerName = booking.propertyOwnerName;
    this.propertyOwnerPhone = booking.propertyOwnerPhone;
  }

  getDateIntervalLabel(): string {
    return `${this.checkInDate.toLocaleDateString('en-US')} - ${this.checkOutDate.toLocaleDateString('en-US')}`;
  }

  getPropertyMainImageUrl(): string {
    let imageUrl;

    if (this.propertyMainImageName) {
      imageUrl = `${process.env.REACT_APP_CORE_API_URL}/${this.propertyMainImageName}`;
    } else {
      imageUrl = process.env.REACT_APP_PROPERTY_IMAGE_STUB;
    }

    return imageUrl;
  }

  getPropertyUrl(): string {
    return `${PROPERTY}/${this.propertyId}`;
  }
}