import { IProperty } from '../interfaces/property';
import { IPropertyBooking } from '../interfaces/propertyBooking';
import { IUser } from './../interfaces/user';
import { PropertyCard } from './propertyCard';

export class Property extends PropertyCard {
  owner: IUser | null;
  bookings: Array<IPropertyBooking> | null;
  description: string;
  hasWiFi: boolean;
  hasParkingPlace: boolean;
  hasTV: boolean;
  hasAirConditioner: boolean;
  hasBedLinen: boolean;
  hasComputer: boolean;
  hasDishes: boolean;
  hasHairDryer: boolean;
  hasIroningStaff: boolean;
  hasKettle: boolean;
  hasMicrowaveOven: boolean;
  hasTowels: boolean;
  hasWashingMachine: boolean;
  imageNames: Array<string>;

  constructor(property: IProperty) {
    super(property);
    this.owner = property.owner;
    this.bookings = property.bookings;
    this.description = property.description;
    this.hasWiFi = property.hasWiFi;
    this.hasParkingPlace = property.hasParkingPlace;
    this.hasTV = property.hasTV;
    this.hasAirConditioner = property.hasAirConditioner;
    this.hasBedLinen = property.hasBedLinen;
    this.hasComputer = property.hasComputer;
    this.hasDishes = property.hasDishes;
    this.hasHairDryer = property.hasHairDryer;
    this.hasIroningStaff = property.hasIroningStaff;
    this.hasKettle = property.hasKettle;
    this.hasMicrowaveOven = property.hasMicrowaveOven;
    this.hasTowels = property.hasTowels;
    this.hasWashingMachine = property.hasWashingMachine;
    this.imageNames = property.imageNames;
  }

  getImageUrls(): Array<string> {
    if (!this.mainImageName) {
      return [<string>process.env.REACT_APP_PROPERTY_IMAGE_STUB];
    } else {
      const images = this.imageNames
        .filter(name => name !== this.mainImageName)
        .map(imageName => `${process.env.REACT_APP_CORE_API_URL}/${imageName}`);
      images.unshift(this.getMainImageUrl());
      return images;
    }
  }
}