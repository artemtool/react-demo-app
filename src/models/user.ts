import { IUser } from './../interfaces/user';

export class User {
  id: string;
  email: string;
  role: string;
  name: string;
  surname: string;
  created: Date;
  lastLogin: Date;
  verified: boolean;
  imageName: string | null;
  phoneNumber1: string | null;
  phoneNumber2: string | null;
  phoneNumber3: string | null;

  constructor(user: IUser) {
    this.id = user.id;
    this.email = user.email;
    this.role = user.role;
    this.name = user.name;
    this.surname = user.surname;
    this.created = new Date(user.created);
    this.lastLogin = new Date(user.lastLogin);
    this.verified = user.verified;
    this.imageName = user.imageName;
    this.phoneNumber1 = user.phoneNumber1;
    this.phoneNumber2 = user.phoneNumber2;
    this.phoneNumber3 = user.phoneNumber3;
  }

  getImageUrl(): string | null {
    return this.imageName
      ? `${process.env.REACT_APP_CORE_API_URL}/${this.imageName}`
      : null;
  }

  getAvatarLetters(): string {
    return this.name
      .substring(0, 2)
      .toUpperCase();
  }
}
