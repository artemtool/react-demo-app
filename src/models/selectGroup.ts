import { SelectOption } from "./selectOption";

export class SelectGroup {
    label: string;
    options: Array<SelectOption>;
  
    constructor(label: string, options: Array<SelectOption>) {
      this.label = label;
      this.options = options;
    }
  }