import { SearchParameters } from "../interfaces/SearchParameters";

const queryString = require('query-string');

export default class SearchParametersParser {

  getSearchParameters(query: string): SearchParameters {
    const queryMap = queryString.parse(query);
    const propertyType = Number(queryMap.propertyType);
    return {
      cityId: queryMap.cityId,
      checkInDate: new Date(Number(queryMap.checkInDate)),
      checkOutDate: new Date(Number(queryMap.checkOutDate)),
      minPrice: Number(queryMap.minPrice) || undefined,
      maxPrice: Number(queryMap.maxPrice) || undefined,
      minRooms: Number(queryMap.minRooms) || undefined,
      maxRooms: Number(queryMap.maxRooms) || undefined,
      sleeps: Number(queryMap.sleeps) || undefined,
      propertyType: Number.isInteger(propertyType)
        ? propertyType
        : undefined,
      hasAirConditioner: queryMap.hasAirConditioner === 'true' || undefined,
      hasBedLinen: queryMap.hasBedLinen === 'true' || undefined || undefined,
      hasComputer: queryMap.hasComputer === 'true' || undefined,
      hasDishes: queryMap.hasDishes === 'true' || undefined,
      hasHairDryer: queryMap.hasHairDryer === 'true' || undefined,
      hasIroningStaff: queryMap.hasIroningStaff === 'true' || undefined,
      hasKettle: queryMap.hasKettle === 'true' || undefined,
      hasMicrowaveOven: queryMap.hasMicrowaveOven === 'true' || undefined,
      hasParkingPlace: queryMap.hasParkingPlace === 'true' || undefined,
      hasTowels: queryMap.hasTowels === 'true' || undefined,
      hasTV: queryMap.hasTV === 'true' || undefined,
      hasWashingMachine: queryMap.hasWashingMachine === 'true' || undefined,
      hasWiFi: queryMap.hasWiFi === 'true' || undefined,
      isDesc: queryMap.isDesc === 'true',
      orderBy: queryMap.orderBy || 'PublishDate'
    };
  }

  getQueryString(searchParameters: SearchParameters): string {
    const params: any = { ...searchParameters };
    params.checkInDate = searchParameters.checkInDate.valueOf();
    params.checkOutDate = searchParameters.checkOutDate.valueOf();
    return queryString.stringify(params);
  }
}