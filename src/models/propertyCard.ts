import { PropertyStatus } from './../interfaces/propertyStatus';
import { PropertyStatus as PropertyStatusType } from '../enums/propertyStatus';
import { ICity } from '../interfaces/city';
import { PropertyType } from '../enums/propertyType';
import { IPropertyCard } from './../interfaces/propertyCard';

export class PropertyCard {
  id: string;
	city: ICity;
  type: PropertyType;
  title: string;
  address: string;
  mainImageName: string | null;
  dailyPrice: number;
  weeklyPrice: number;
  rooms: number;
  area: number;
  sleeps: number;
  floor: number;
  publishDate: Date;
  status: PropertyStatusType;
  ownerId: string;

  constructor(card: IPropertyCard) {
    this.id = card.id;
    this.city = card.city;
    this.type = card.type;
    this.title = card.title;
    this.address = card.address;
    this.mainImageName = card.mainImageName;
    this.dailyPrice = card.dailyPrice;
    this.weeklyPrice = card.weeklyPrice;
    this.rooms = card.rooms;
    this.area = card.area;
    this.sleeps = card.sleeps;
    this.floor = card.floor;
    this.publishDate = new Date(card.publishDate);
    this.status = card.status;
    this.ownerId = card.ownerId;
  }

  getFormattedPublishDate(): string {
    return this.publishDate.toLocaleDateString('en-US');
  }

  getPropertyTypeLabel(): string {
    switch (this.type) {
      case PropertyType.Apartment:
        return 'Apartments';
      case PropertyType.Villa:
        return 'Villa';
      case PropertyType.HotelRoom:
        return 'Hotel room';
      default:
        return 'Unknown property type';
    }
  }

  getStatus(): PropertyStatus {
    switch (this.status) {
      case PropertyStatusType.Active:
        return {
          label: 'Active',
          tooltip: 'Property is avaliable for booking'
        };
      case PropertyStatusType.Inactive:
        return {
          label: 'Inactive',
          tooltip: 'Property is not avaliable for booking'
        };
      case PropertyStatusType.WaitingForModeration:
        return {
          label: 'On moderation',
          tooltip: 'Waiting for moderator to approve publishing'
        };
      default:
        return {
          label: 'Active',
          tooltip: 'Property is avaliable for booking'
        };
    }
  }

  getMainImageUrl(): string {
    let imageUrl;

    if (this.mainImageName) {
      imageUrl = `${process.env.REACT_APP_CORE_API_URL}/${this.mainImageName}`;
    } else {
      imageUrl = process.env.REACT_APP_PROPERTY_IMAGE_STUB;
    }

    return imageUrl;
  };
}