import * as React from 'react';
import Typography from '@material-ui/core/Typography';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { fetchIncomingBookings, fetchSelectedProperty } from '../../state/actions/incomingBookings';
import { AppState } from '../../state/appState';
import * as styles from './IncomingBookings.scss';
import * as routes from '../../constants/routes';
import { Link, match } from 'react-router-dom';
import CircularProgress from '@material-ui/core/CircularProgress';
import { State as IncomingBookingsState } from '../../state/reducers/incomingBookings';
import Select from 'react-select';
import Label from './../../components/Label/Label';
import { fetchPropertySuggestions } from './../../state/actions/incomingBookings';
import { SelectOption } from './../../models/selectOption';
import { PropertyCard } from '../../models/propertyCard';
import InfiniteScrollList from './../../components/InfiniteScrollList';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Tooltip from '@material-ui/core/Tooltip';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import DateRangeIcon from '@material-ui/icons/DateRange';
import { mapValueToSelectOption } from '../../common/selectMappers';
import { MessangerState } from './../../interfaces/messangerState';
import { User } from './../../models/user';
import Messanger from './../../components/Messanger/Messanger';
import { fetchMessages, sendMessage } from './../../state/actions/messanger';
import { NewPropertyBookings } from './../../interfaces/newIncomingBookings';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

type RouteParams = {
  propertyId: string;
};

interface Props {
  match: match<RouteParams>;
  history: any;
  incomingBookings: IncomingBookingsState;
  messangerStates: Array<MessangerState>;
  user: User;
  newPropertyBookings: Array<NewPropertyBookings>;
  fetchIncomingBookings: (propertyId: string, initialLoad?: boolean) => Promise<void>;
  fetchPropertySuggestions: (address: string) => Promise<void>;
  fetchSelectedProperty: (id: string) => Promise<void>;
  fetchMessages: (bookingId: string, initialLoad: boolean) => Promise<void>;
  sendMessage: (bookingId: string, text: string) => Promise<void>;
};

const mapPropertyCardsToSelectOptions = (properties: Array<PropertyCard>): Array<SelectOption> => {
  return properties.map(p => new SelectOption(`${p.city.name}, ${p.address}`, p.id));
}

class IncomingBookingsPage extends React.PureComponent<Props> {

  private readonly spinner: React.ReactNode = <CircularProgress className={styles.loadingSpinner} />;
  private preloadedMessangerIds: Array<string> = [];

  componentDidMount() {
    const id = this.props.match.params.propertyId;
    this.onPropertyIdChange(id);
  }

  componentDidUpdate(prevProps: Props) {
    const currentPropertyId = this.props.match.params.propertyId;

    if (currentPropertyId != prevProps.match.params.propertyId) {
      this.onPropertyIdChange(currentPropertyId);
    }
  }

  render() {
    const propertySuggestions = mapPropertyCardsToSelectOptions(this.props.incomingBookings.suggestions);
    const property = this.props.incomingBookings.selectedProperty;
    const selectedPropertyId = property
      ? property.id
      : null;

    return (
      <React.Fragment>
        <Typography variant="display1" gutterBottom>Incoming bookings</Typography>
        <Typography variant="subheading" gutterBottom>
          All bookings, that was done by people on your <Link to={routes.PUBLISHED_PROPERTIES} className={styles.link}><b>published properties</b></Link>
        </Typography>
        {
          this.props.newPropertyBookings.length > 0 && (
            <List className={styles.newPropertyBookings}>
            <span className={styles.newBookingsLabel}>New bookings</span>
              {
                this.props.newPropertyBookings.map(n =>
                  <ListItem
                    key={n.propertyId}
                    button
                  >
                    <Link to={`${routes.INCOMING_BOOKINGS}/${n.propertyId}`}>
                      <ListItemText primary={`${n.count} new ${n.count > 1 ? 'bookings' : 'booking'} at ${n.propertyAddress}`} />
                    </Link>
                  </ListItem>
                )
              }
            </List>
          )
        }
        <div className={styles.propertyCard}>
          <div className={styles.propertyImg}>
            {!property && <div className={styles.propertyImgPlaceholder}></div>}
            {property && <img src={property.getMainImageUrl()} />}
          </div>
          <div className={styles.propertyDetails}>
            <Label
              label="Start typing property address (at least 3 characters)"
            />
            <Select
              options={propertySuggestions}
              isClearable={true}
              placeholder={'Enter property address'}
              value={mapValueToSelectOption(selectedPropertyId, propertySuggestions)}
              onInputChange={value => this.onAddressChange(value)}
              onChange={e => this.onSelectedPropertyChange(e ? e.value : null)}
            />
            <div className={styles.propertyFeatures}>
              {!property && (
                <React.Fragment>
                  <div className={styles.propertyFeaturePlaceholder}></div>
                  <div className={styles.propertyFeaturePlaceholder}></div>
                  <div className={styles.propertyFeaturePlaceholder}></div>
                  <div className={styles.propertyFeaturePlaceholder}></div>
                </React.Fragment>
              )}
              {property && (
                <React.Fragment>
                  <Typography className={styles.propertyFeature} variant="body1">Sleeps: {property.sleeps}</Typography>
                  <Typography className={styles.propertyFeature} variant="body1">Rooms: {property.rooms}</Typography>
                  <Typography className={styles.propertyFeature} variant="body1">Area: {property.area}</Typography>
                  <Typography className={styles.propertyFeature} variant="body1">Floor: {property.floor}</Typography>
                </React.Fragment>
              )}
            </div>
            {property && (
              <div className={styles.propertyIconizedFeatures}>
                <Tooltip title="Price per day / week" placement="top-start">
                  <div className={styles.propertyIconizedFeature}>
                    <AttachMoneyIcon />
                    <Typography variant="body2">{property.dailyPrice} / {property.weeklyPrice}</Typography>
                  </div>
                </Tooltip>
                <Tooltip title="Publish date" placement="top-start">
                  <div className={styles.propertyIconizedFeature}>
                    <DateRangeIcon />
                    <Typography className={styles.publishDate} variant="body2">{property.getFormattedPublishDate()}</Typography>
                  </div>
                </Tooltip>
              </div>
            )}
          </div>
        </div>
        <InfiniteScrollList
          onScroll={this.onScroll}
          isEmpty={this.props.incomingBookings.bookings.length === 0}
          isLoading={this.props.incomingBookings.isLoading}
          canLoadMore={this.props.incomingBookings.canLoadMore}
          loadingIndicator={this.spinner}
          emptyMock="There are no bookings to show..."
        >
          {
            this.props.incomingBookings.bookings.map(booking => {
              const messangerState: MessangerState = this.props.messangerStates
                .find(s => s.connectedEntityId === booking.id) || {
                  connectedEntityId: booking.id,
                  messages: [],
                  isLoading: false,
                  canLoadMore: false,
                  page: 1
                };

              return (
                <ExpansionPanel
                  key={booking.id}
                  onChange={(e, expanded: boolean) => { this.onPanelStateChange(expanded, booking.id) }}
                >
                  <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography>{booking.getDateIntervalLabel()}</Typography>
                  </ExpansionPanelSummary>
                  <ExpansionPanelDetails className={styles.booking}>
                    <div>
                      <div>
                        <Typography>Price per day: <b>${booking.dailyPrice}</b></Typography>
                        <Typography>Booked for: <b>{booking.guestName}</b></Typography>
                        <Typography>Guest phone: <b>{booking.guestPhoneNumber}</b></Typography>
                        <Typography>Guests amount: <b>{booking.guestsAmount}</b></Typography>
                        <Typography>Guests comments: <b>{booking.comments}</b></Typography>
                      </div>
                    </div>
                    {
                      <div className={styles.messangerWrapper}>
                        <Messanger
                          state={messangerState}
                          senderId={this.props.user.id}
                          senderLabel={'You'}
                          receiverLabel={'Guest'}
                          onLoad={() => this.onMessangerScroll(booking.id)}
                          onSend={text => this.onSend(messangerState.connectedEntityId, text)}
                        />
                      </div>
                    }
                  </ExpansionPanelDetails>
                </ExpansionPanel>
              )
            })
          }
        </InfiniteScrollList>
      </React.Fragment>
    );
  }

  private onPanelStateChange(expanded: boolean, bookingId: string) {
    const alreadyPreloaded = !!this.preloadedMessangerIds.find(id => id === bookingId);

    if (!alreadyPreloaded) {
      this.preloadedMessangerIds.push(bookingId);
      this.props.fetchMessages(bookingId, true);
    }
  }

  private onMessangerScroll(bookingId: string) {
    this.props.fetchMessages(bookingId, false);
  }

  private onSend(bookingId: string, text: string) {
    this.props.sendMessage(bookingId, text);
  }

  private onAddressChange(address: string) {
    if (address.length > 2) {
      this.props.fetchPropertySuggestions(address);
    }
  }

  private onSelectedPropertyChange(id: string) {
    if (id) {
      this.props.history.push(`${routes.INCOMING_BOOKINGS}/${id}`);
    } else {
      this.props.history.push(routes.INCOMING_BOOKINGS);
    }
  }

  private onScroll = () => {
    const selectedPropertyId = this.props.incomingBookings.selectedProperty && this.props.incomingBookings.selectedProperty.id;
    if (selectedPropertyId) {
      this.props.fetchIncomingBookings(selectedPropertyId);
    }
  }

  private onPropertyIdChange(id: string) {
    this.props.fetchSelectedProperty(id);
    this.props.fetchIncomingBookings(id, true);
  }
}

const mapStateToProps = (state: AppState) => ({
  incomingBookings: state.incomingBookings,
  messangerStates: state.messanger.states,
  user: state.user.authUser,
  newPropertyBookings: state.incomingBookings.newPropertyBookings
});

const mapDispatchToProps = dispatch => ({
  fetchIncomingBookings: (propertyId, initialLoad) => dispatch(fetchIncomingBookings(propertyId, initialLoad)),
  fetchPropertySuggestions: address => dispatch(fetchPropertySuggestions(address)),
  fetchSelectedProperty: id => dispatch(fetchSelectedProperty(id)),
  fetchMessages: (bookingId: string, initialLoad: boolean) => dispatch(fetchMessages(bookingId, initialLoad)),
  sendMessage: (bookingId: string, text: string) => dispatch(sendMessage(bookingId, text))
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(IncomingBookingsPage);