import * as React from 'react';
import { match } from 'react-router-dom';
import { Property } from './../../models/property';
import Typography from '@material-ui/core/Typography';
import * as styles from './Property.scss';
import { propertyFeatures } from './../../constants/property';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { connect } from 'react-redux';
import { AppState } from '../../state/appState';
import { fetchProperty } from '../../state/actions/property';
import ImageGallery from 'react-image-gallery';

type RouteParams = {
  propertyId: string;
};

interface Props {
  match: match<RouteParams>;
  property: Property;
  isLoading: boolean;
  fetchProperty: (id: string) => Promise<void>;
};

class PropertyPage extends React.PureComponent<Props> {

  componentDidMount() {
    this.props.fetchProperty(this.props.match.params.propertyId);
  }

  render() {
    const property = this.props.property;
    const isLoading = this.props.isLoading;
    const errorOccured = !this.props.isLoading && !this.props.property;
    const images = this.props.property
      ? this.props.property.getImageUrls().map(url => ({
        original: url,
        thumbnail: url
      }))
      : [];

    return (
      <React.Fragment>
        {isLoading && 'Loading...'}
        {errorOccured && 'Error occured'}
        {property && (
          <React.Fragment>
            <Typography variant="display1" gutterBottom>{`${property.city.name}, ${property.address}`}</Typography>
            <Typography variant="subheading" gutterBottom>{`${property.title} - ${property.getPropertyTypeLabel()}`}</Typography>
            <article className={styles.propertyBlock}>
              <Typography variant="body2" component="header" gutterBottom>
                <span className={styles.propertyBlockCounter}>1</span>Main information
            </Typography>
              <div className={styles.propertyBlockContent}>
                <div className={styles.imageGallery}>
                  <ImageGallery
                    items={images}
                    lazyLoad={true}
                    showPlayButton={false}
                    showIndex={true}
                  />
                </div>
                <div className={styles.propertyData}>
                  <div className={styles.propertyDataProp}>
                    <Typography variant="body1">Sleeps:</Typography>
                    <Typography variant="display1" component="span">{property.sleeps}</Typography>
                  </div>
                  <div className={styles.propertyDataProp}>
                    <Typography variant="body1">Rooms:</Typography>
                    <Typography variant="display1" component="span">{property.rooms}</Typography>
                  </div>
                  <div className={styles.propertyDataProp}>
                    <Typography variant="body1">Floor:</Typography>
                    <Typography variant="display1" component="span">{property.floor}</Typography>
                  </div>
                  <div className={styles.propertyDataProp}>
                    <Typography variant="body1">Area (sq. m.):</Typography>
                    <Typography variant="display1" component="span">{property.area}</Typography>
                  </div>
                  <div className={styles.propertyDataProp}>
                    <Typography variant="body1">Price per day ($):</Typography>
                    <Typography variant="display1" component="span">{property.dailyPrice}</Typography>
                  </div>
                  <div className={styles.propertyDataProp}>
                    <Typography variant="body1">Price per week ($):</Typography>
                    <Typography variant="display1" component="span">{property.weeklyPrice}</Typography>
                  </div>
                </div>
              </div>
            </article>
            <article className={styles.propertyBlock}>
              <Typography variant="body2" component="header" gutterBottom>
                <span className={styles.propertyBlockCounter}>2</span>Feature list
            </Typography>
              <div className={`${styles.propertyBlockContent} ${styles.propertyFeatures}`}>
                {
                  propertyFeatures.map((feature, i) =>
                    <FormControlLabel
                      key={feature.name}
                      className={styles.defaultCursor}
                      control={
                        <Checkbox
                          color="primary"
                          checked={property[feature.name]}
                          disableRipple={true}
                          className={styles.defaultCursor}
                        />
                      }
                      label={feature.label}
                    />
                  )
                }
              </div>
            </article>
            <article className={styles.propertyBlock}>
              <Typography variant="body2" component="header" gutterBottom>
                <span className={styles.propertyBlockCounter}>3</span>Property description
            </Typography>
              <div className={styles.propertyBlockContent}>
                <Typography variant="body1" align="justify">{property.description}</Typography>
              </div>
            </article>
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

const mapPropsToState = (state: AppState) => ({
  property: state.property.model,
  isLoading: state.property.isLoading
});

const mapDispatchToState = dispatch => ({
  fetchProperty: id => dispatch(fetchProperty(id))
});


export default connect(
  mapPropsToState,
  mapDispatchToState
)(PropertyPage);