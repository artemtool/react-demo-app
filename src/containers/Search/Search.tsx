import * as React from 'react';
import SearchParametersParser from '../../models/searchParametersParser';
import CircularProgress from '@material-ui/core/CircularProgress';
import * as styles from './Search.scss';
import { connect } from 'react-redux';
import { State as SearchResultsState } from './../../state/reducers/searchResults';
import { State as SearchFiltersState } from './../../state/reducers/searchFilters';
import { fetchProperties } from '../../state/actions/searchResults';
import { AppState } from '../../state/appState';
import PropertySearchCard from './../../components/PropertySearchCard/PropertySearchCard';
import { showDialog, showSnackbar } from './../../state/actions/overlays';
import { IDialog } from './../../interfaces/dialog';
import { PropertyCard } from './../../models/propertyCard';
import InfiniteScrollList from '../../components/InfiniteScrollList';
import { Dialog } from '../../enums/dialog';
import { SearchParameters } from '../../interfaces/SearchParameters';
import { updateParameters } from '../../state/actions/searchFilters';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import * as routes from '../../constants/routes';
import EditIcon from '@material-ui/icons/Edit';
import { ISnackbar } from '../../interfaces/snackbar';
import { Snackbar } from '../../enums/snackbar';

interface Props {
  searchResults: SearchResultsState;
  searchFilters: SearchFiltersState;
  currentUserId: string | null;
  location: any;
  fetchProperties: (params: SearchParameters, initialLoad: boolean) => Promise<void>;
  updateParameters: (params: SearchParameters) => void;
  showDialog: (dialog: IDialog) => void;
  showSnackbar: (snackbar: ISnackbar) => void;
}

class SearchPage extends React.PureComponent<Props> {

  spinner: React.ReactNode = <CircularProgress className={styles.loadingSpinner} />;
  appliedSearchParameters: SearchParameters;

  componentDidMount() {
    const paramsParser = new SearchParametersParser();
    const searchParams = paramsParser.getSearchParameters(this.props.location.search);

    this.initSearchFilters(searchParams);
    this.props.fetchProperties(searchParams, true);

    this.appliedSearchParameters = searchParams;
  }

  componentDidUpdate(prevProps: Props) {
    if (this.props.location.search !== prevProps.location.search) {
      this.props.fetchProperties(this.props.searchFilters.parameters, true);
      this.appliedSearchParameters = this.props.searchFilters.parameters;
    }
  }

  initSearchFilters(params: SearchParameters) {
    if (params.cityId !== this.props.searchFilters.parameters.cityId) {
      this.props.updateParameters(params);
    }
  }

  onScroll = () => {
    this.props.fetchProperties(this.appliedSearchParameters, false);
  }

  bookProperty(property: PropertyCard) {
    if (this.props.currentUserId) {
      this.props.showDialog({
        type: Dialog.Booking,
        payload: {
          property,
          checkInDate: this.props.searchFilters.parameters.checkInDate,
          checkOutDate: this.props.searchFilters.parameters.checkOutDate,
        }
      });
    } else {
      this.props.showSnackbar({
        type: Snackbar.Info,
        message: 'You should log in to book properties'
      })
    }
  }

  render() {
    return (
      <InfiniteScrollList
        onScroll={this.onScroll}
        isEmpty={this.props.searchResults.properties.length === 0}
        isLoading={this.props.searchResults.isLoading}
        canLoadMore={this.props.searchResults.canLoadMore}
        loadingIndicator={this.spinner}
        emptyMock="Empty..."
      >
        <div className={styles.propertyList}>
          {
            this.props.searchResults.properties.map(p =>
              <PropertySearchCard
                key={p.id}
                property={p}
                className={styles.propertyCard}
              >
                {
                  this.props.currentUserId === p.ownerId
                    ? (
                      <Link to={`${routes.EDIT_PROPERTY}/${p.id}`}>
                        <Button
                          variant="fab"
                          color="secondary"
                          className={styles.bookButton}
                        >
                          <EditIcon />
                        </Button>
                      </Link>
                    )
                    : (
                      <Button
                        size="small"
                        color="secondary"
                        variant="contained"
                        onClick={() => this.bookProperty(p)}
                        className={styles.bookButton}
                      >
                        Book now
                      </Button>
                    )
                }
              </PropertySearchCard>
            )
          }
        </div>
      </InfiniteScrollList>
    );
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    searchResults: state.searchResults,
    searchFilters: state.searchFilters,
    currentUserId: state.user.authUser
      ? state.user.authUser.id
      : null
  }
};

const mapDispatchToProps = dispatch => {
  return {
    updateParameters: (params: SearchParameters) => dispatch(updateParameters(params)),
    fetchProperties: (params: SearchParameters, initialLoad: boolean) => dispatch(fetchProperties(params, initialLoad)),
    showDialog: (dialog: IDialog) => dispatch(showDialog(dialog)),
    showSnackbar: (snackbar: ISnackbar) => dispatch(showSnackbar(snackbar))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchPage);