import * as React from 'react';
import Typography from '@material-ui/core/Typography';
import { connect } from 'react-redux';
import { fetchSuggestionsIfNeeded } from './../../state/actions/searchFilters';
import { AppState } from './../../state/appState';
import { showSnackbar } from './../../state/actions/overlays';
import { ISnackbar } from './../../interfaces/snackbar';
import * as routes from '../../constants/routes';
import { Snackbar } from '../../enums/snackbar';
import EditPropertyForm from '../../components/EditPropertyForm/EditPropertyForm';
import { SelectGroup } from './../../models/selectGroup';
import { http } from './../../common/http';

interface Props {
  citySuggestions: Array<SelectGroup>;
  history: any;
  fetchCitySuggestions: () => Promise<void>;
  showSnackbar: (snackbar: ISnackbar) => void;
};

class PublishPropertyPage extends React.PureComponent<Props> {

  constructor(props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this);
    this.onCancel = this.onCancel.bind(this);
  }

  public componentDidMount() {
    this.props.fetchCitySuggestions();
  }

  public render() {
    return (
      <React.Fragment>
        <Typography variant="display1" gutterBottom>Publish property for rent</Typography>
        <Typography variant="subheading" gutterBottom>Want to lease out your property? Fill in the form below and start making money</Typography>
        <EditPropertyForm
          citySuggestions={this.props.citySuggestions}
          submitButtonText="Publish property"
          cancelButtonText="Cancel"
          onSubmit={this.onSubmit}
          onCancel={this.onCancel}
        />
      </React.Fragment>
    );
  }

  private onSubmit(formData: FormData) {
    http.stickAuthHeader()
      .then(headers => {
        return fetch(`${process.env.REACT_APP_CORE_API_URL}/property/publish`, {
          method: 'POST',
          body: formData,
          headers
        })
      })
      .then(() => {
        this.props.history.push(`${routes.PUBLISHED_PROPERTIES}`);
        this.props.showSnackbar({
          type: Snackbar.Success,
          message: `Property was published succesfully.`
        });
      })
      .catch(error => {
        this.props.showSnackbar({
          type: Snackbar.Error,
          message: error.message
        });
      });
  }

  private onCancel() {
    this.props.history.goBack();
  }
}

const mapStateToProps = (state: AppState) => ({
  citySuggestions: state.searchFilters.suggestions
});

const mapStateToDispatch = dispatch => ({
  fetchCitySuggestions: () => dispatch(fetchSuggestionsIfNeeded()),
  showSnackbar: (snackbar: ISnackbar) => dispatch(showSnackbar(snackbar))
});

export default connect(
  mapStateToProps,
  mapStateToDispatch
)(PublishPropertyPage);