import * as React from 'react';
import Typography from '@material-ui/core/Typography';
import { connect } from 'react-redux';
import { fetchSuggestionsIfNeeded } from './../../state/actions/searchFilters';
import { AppState } from './../../state/appState';
import { showSnackbar } from './../../state/actions/overlays';
import { ISnackbar } from './../../interfaces/snackbar';
import * as routes from '../../constants/routes';
import { Snackbar } from '../../enums/snackbar';
import EditPropertyForm from '../../components/EditPropertyForm/EditPropertyForm';
import { SelectGroup } from './../../models/selectGroup';
import { Property } from './../../models/property';
import { match } from 'react-router-dom';
import { http } from './../../common/http';

type RouteParams = {
  propertyId: string
};

interface Props {
  citySuggestions: Array<SelectGroup>;
  history: any;
  match: match<RouteParams>;
  fetchCitySuggestions: () => Promise<void>;
  showSnackbar: (snackbar: ISnackbar) => void;
};

interface State {
  propertyOnEdit?: Property;
  isPropertyOnEditLoading?: boolean;
}

class EditPropertyPage extends React.PureComponent<Props, State> {

  constructor(props) {
    super(props);

    this.state = {};
    this.onSubmit = this.onSubmit.bind(this);
    this.onCancel = this.onCancel.bind(this);
  }

  public componentDidMount() {
    this.props.fetchCitySuggestions();
    this.fetchPropertyOnEdit(this.props.match.params.propertyId);
  }

  public render() {
    return (
      <React.Fragment>
        <Typography variant="display1" gutterBottom>Edit property</Typography>
        <Typography variant="subheading" gutterBottom>Here you can edit already published property</Typography>
        <EditPropertyForm
          citySuggestions={this.props.citySuggestions}
          submitButtonText="Save property"
          cancelButtonText="Cancel"
          onSubmit={this.onSubmit}
          onCancel={this.onCancel}
          defaultData={this.state.propertyOnEdit}
          isDefaultDataLoading={this.state.isPropertyOnEditLoading}
        />
      </React.Fragment>
    );
  }

  private fetchPropertyOnEdit(id: string) {
    this.setState({
      isPropertyOnEditLoading: true
    });

    const headers = {
      'Content-Type': 'application/json'
    };

    return http.stickAuthHeader(headers)
      .then(headers => {
        return fetch(`${process.env.REACT_APP_CORE_API_URL}/property/${id}/edit`, {
          method: "GET",
          headers
        })
      })
      .then(response => {
        if (response.status === 403) {
          throw new Error("You are not the owner of this property!");
        } else {
          return response.json();
        }
      })
      .then(result => {
        this.setState({
          propertyOnEdit: new Property(result),
          isPropertyOnEditLoading: false
        });
      })
      .catch(() => {
        this.setState({
          isPropertyOnEditLoading: false
        });
        this.props.history.push(routes.PUBLISHED_PROPERTIES);
      });
  }

  private onSubmit(formData: FormData) {
    const propertyId = this.props.match.params.propertyId;
    formData.append('propertyId', propertyId);

    http.stickAuthHeader()
      .then(headers => {
        return fetch(`${process.env.REACT_APP_CORE_API_URL}/property/update`, {
          method: 'POST',
          body: formData,
          headers
        });
      })
      .then(response => {
        if (response.status === 403) {
          throw new Error("You are not the owner of this property!");
        } else {
          this.props.history.push(`${routes.PROPERTY}/${propertyId}`);
          this.props.showSnackbar({
            type: Snackbar.Success,
            message: `Property was succesfully updated.`
          });
        }
      })
      .catch(error => {
        this.props.showSnackbar({
          type: Snackbar.Error,
          message: error.message
        });
      });

  }

  private onCancel() {
    this.props.history.goBack();
  }
}

const mapStateToProps = (state: AppState) => ({
  citySuggestions: state.searchFilters.suggestions
});

const mapStateToDispatch = dispatch => ({
  fetchCitySuggestions: () => dispatch(fetchSuggestionsIfNeeded()),
  showSnackbar: (snackbar: ISnackbar) => dispatch(showSnackbar(snackbar))
});

export default connect(
  mapStateToProps,
  mapStateToDispatch
)(EditPropertyPage);