export const bookingDate: string;
export const booking: string;
export const bookingContent: string;
export const bookingDetails: string;
export const propertyDetails: string;
export const propertyLink: string;
export const messangerWrapper: string;
export const loadingSpinner: string;
