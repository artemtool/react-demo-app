import * as React from 'react';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { fetchUserBookings } from '../../state/actions/userBookings';
import { AppState } from '../../state/appState';
import * as styles from './UserBookings.scss';
import { Link } from 'react-router-dom';
import CircularProgress from '@material-ui/core/CircularProgress';
import { State as UserBookingsState } from '../../state/reducers/userBookings';
import InfiniteScrollList from './../../components/InfiniteScrollList';
import { MessangerState } from '../../interfaces/messangerState';
import Messanger from './../../components/Messanger/Messanger';
import { sendMessage, fetchMessages } from './../../state/actions/messanger';
import { User } from '../../models/user';

interface Props {
  userBookings: UserBookingsState;
  messangerStates: Array<MessangerState>;
  user: User;
  fetchUserBookings: (initialLoad: boolean) => Promise<void>;
  fetchMessages: (bookingId: string, initialLoad: boolean) => Promise<void>;
  sendMessage: (bookingId: string, text: string) => Promise<void>;
}

class UserBookingsPage extends React.PureComponent<Props> {

  private spinner: React.ReactNode = <CircularProgress className={styles.loadingSpinner} />;
  private preloadedMessangerIds: Array<string> = [];

  constructor(props) {
    super(props);

    this.onPageScroll = this.onPageScroll.bind(this);
  }

  public componentDidMount() {
    this.props.fetchUserBookings(true);
  }

  public render() {
    return (
      <React.Fragment>
        <Typography variant="display1" gutterBottom>My bookings</Typography>
        <Typography variant="subheading" gutterBottom>The history of bookings you have ever made with "SBS - Simple Booking System"</Typography>
        <InfiniteScrollList
          onScroll={this.onPageScroll}
          isEmpty={this.props.userBookings.bookings.length === 0}
          isLoading={this.props.userBookings.isLoading}
          canLoadMore={this.props.userBookings.canLoadMore}
          loadingIndicator={this.spinner}
          emptyMock="Empty..."
        >
          {
            this.props.userBookings.bookings.map(booking => {
              const messangerState: MessangerState = this.props.messangerStates
                .find(s => s.connectedEntityId === booking.id) || {
                  connectedEntityId: booking.id,
                  messages: [],
                  isLoading: false,
                  canLoadMore: false,
                  page: 1
                };
              return (
                <ExpansionPanel
                  key={booking.id}
                  onChange={(e, expanded: boolean) => { this.onPanelStateChange(expanded, booking.id) }}
                >
                  <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography>{booking.propertyAddress}</Typography>
                    <Typography className={styles.bookingDate}>{booking.getDateIntervalLabel()}</Typography>
                  </ExpansionPanelSummary>
                  <ExpansionPanelDetails className={styles.booking}>
                    <div className={styles.bookingContent}>
                      <div className={styles.bookingDetails}>
                        <Typography>Price per day: <b>${booking.dailyPrice}</b></Typography>
                        <Typography>Booked for: <b>{booking.guestName}</b></Typography>
                        <Typography>Guest phone: <b>{booking.guestPhoneNumber}</b></Typography>
                        <Typography>Guests amount: <b>{booking.guestsAmount}</b></Typography>
                        <Typography>Comments: <b>{booking.comments}</b></Typography>
                      </div>
                      <div className={styles.propertyDetails}>
                        <img src={booking.getPropertyMainImageUrl()}></img>
                        <Link to={booking.getPropertyUrl()}>
                          <Typography variant="body1" className={styles.propertyLink}>Go to property page</Typography>
                        </Link>
                      </div>
                    </div>
                    {
                      <div className={styles.messangerWrapper}>
                        <Messanger
                          state={messangerState}
                          senderId={this.props.user.id}
                          senderLabel={'You'}
                          receiverLabel={'Property owner'}
                          onLoad={() => this.onMessangerScroll(booking.id)}
                          onSend={text => this.onSend(messangerState.connectedEntityId, text)}
                        />
                      </div>
                    }
                  </ExpansionPanelDetails>
                </ExpansionPanel>
              )
            })
          }
        </InfiniteScrollList>
      </React.Fragment>
    );
  }

  private onPanelStateChange(expanded: boolean, bookingId: string) {
    const alreadyPreloaded = !!this.preloadedMessangerIds.find(id => id === bookingId);

    if (!alreadyPreloaded) {
      this.preloadedMessangerIds.push(bookingId);
      this.props.fetchMessages(bookingId, true);
    }
  }

  private onMessangerScroll(bookingId: string) {
    this.props.fetchMessages(bookingId, false);
  }

  private onSend(bookingId: string, text: string) {
    this.props.sendMessage(bookingId, text);
  }

  private onPageScroll() {
    this.props.fetchUserBookings(false);
  }
}

const mapStateToProps = (state: AppState) => ({
  userBookings: state.userBookings,
  messangerStates: state.messanger.states,
  user: state.user.authUser
});

const mapDispatchToProps = dispatch => ({
  fetchUserBookings: initialLoad => dispatch(fetchUserBookings(initialLoad)),
  fetchMessages: (bookingId: string, initialLoad: boolean) => dispatch(fetchMessages(bookingId, initialLoad)),
  sendMessage: (bookingId: string, text: string) => dispatch(sendMessage(bookingId, text))
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(UserBookingsPage);