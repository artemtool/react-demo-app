import * as React from 'react';

import * as styles from './Home.scss';
import SearchForm from '../../components/SearchForm/SearchForm';

interface Props {
  history: Object;
}

class HomePage extends React.PureComponent<Props> {
  render() {
    return (
      <div className={styles.searchBarWrapper}>
        <SearchForm history={this.props.history} />
      </div>
    );
  }
}

export default HomePage;