import * as React from 'react';
import { NavLink } from 'react-router-dom';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { AppState } from '../../state/appState';
import { signInWithEmail } from './../../state/actions/user';
import * as routes from './../../constants/routes';
import * as styles from './SignIn.scss';
import Typography from '@material-ui/core/Typography';
import LoadingButton from './../../components/LoadingButton/LoadingButton';
import Label from './../../components/Label/Label';
import Input from './../../components/Input/Input';
import { User } from './../../models/user';
import { showSnackbar } from './../../state/actions/overlays';
import { ISnackbar } from './../../interfaces/snackbar';
import { Snackbar } from './../../enums/snackbar';
import FormValidator from './../../common/formValidator';
import * as validator from 'validator';

interface Props {
  history: any;
  isSignInProceeding: boolean;
  signInWithEmail: (email: string, password: string) => Promise<User>;
  showSnackbar: (snackbar: ISnackbar) => void;
}

interface State {
  email: string;
  password: string;
  validation: any;
}

const initialState: State = {
  email: '',
  password: '',
  validation: null
};

class SignInPage extends React.PureComponent<Props, State> {
  validator: FormValidator;

  constructor(props) {
    super(props);

    this.validator = new FormValidator([
      {
        field: 'email',
        method: validator.isEmail,
        validWhen: true,
        message: 'Please provide an email address.'
      },
      {
        field: 'password',
        method: validator.isEmpty,
        validWhen: false,
        message: 'Please provide a password.'
      }
    ]);

    this.state = {
      ...initialState,
      validation: this.validator.valid()
    };
  }

  onSubmit = (event) => {
    event.preventDefault();

    const {
      email,
      password,
    } = this.state;

    const validation = this.validator.validate(this.state);

    this.setState(state => ({
      ...state,
      validation
    }));

    if (validation.isValid) {
      this.props.signInWithEmail(email, password)
        .then((user: User) => {
          this.props.showSnackbar({
            type: Snackbar.Info,
            message: `Welcome, ${user.name} ${user.surname}`
          });
        })
        .catch(error => {
          this.props.showSnackbar({
            type: Snackbar.Error,
            message: error.message
          });
        });
    }
  }

  onFormValueChange(name: string, value: string) {
    this.setState({
      ...this.state,
      [name]: value
    });
  }

  render() {
    const {
      email,
      password,
      validation
    } = this.state;
    return (
      <React.Fragment>
        <Typography variant="display1" align="center" gutterBottom>Sign in</Typography>
        <Typography variant="subheading" align="center">Sign in to your account to use all the features</Typography>
        <form
          className={styles.form}
          onSubmit={this.onSubmit}>
          <div className={`${styles.fieldWrapper} ${validation.email.isInvalid && styles.hasError}`}>
            <Label label="Email" />
            <Input
              id="email"
              label="Email"
              placeholder="Enter your email"
              className={styles.field}
              value={email}
              onChange={event => this.onFormValueChange('email', event.target.value)}
            />
            <Typography className={styles.errorLabel}>{validation.email.message}</Typography>
          </div>
          <div className={`${styles.fieldWrapper} ${validation.password.isInvalid && styles.hasError}`}>
            <Label label="Password" />
            <Input
              id="password"
              type="password"
              label="Password"
              placeholder="Enter your password"
              className={styles.field}
              value={password}
              onChange={event => this.onFormValueChange('password', event.target.value)}
            />
            <Typography className={styles.errorLabel}>{validation.password.message}</Typography>
          </div>
          <LoadingButton
            isLoading={this.props.isSignInProceeding}
            className={styles.button}
            type="submit"
            variant="contained"
            color="primary">
            Sign In
            </LoadingButton>
        </form>
        <Typography variant="body2" align="center" className={styles.signUpSuggestion}>
          Don't have an account yet?
            <NavLink to={routes.SIGN_UP} className={styles.signUpLink}>
            Sign up
            </NavLink>
        </Typography>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state: AppState) => ({
  isSignInProceeding: state.user.isLoading
});

const mapDispatchToProps = dispatch => ({
  signInWithEmail: (email, password) => dispatch(signInWithEmail(email, password)),
  showSnackbar: type => dispatch(showSnackbar(type))
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(SignInPage);