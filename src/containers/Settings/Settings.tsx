import * as React from 'react';
import * as styles from './Settings.scss';
import Typography from '@material-ui/core/Typography';
import CheckIcon from '@material-ui/icons/Check';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { changeUserImage, updateUserContacts, updateUserDetails, deleteUserImage } from './../../state/actions/user';
import LoadingButton from './../../components/LoadingButton/LoadingButton';
import Input from '../../components/Input/Input';
import Label from './../../components/Label/Label';
import { showSnackbar } from './../../state/actions/overlays';
import FormValidator from './../../common/formValidator';
import * as validator from 'validator';
import { User } from './../../models/user';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import IconButton from '@material-ui/core/IconButton';
import { AppState } from '../../state/appState';
import ImagePreview from '../../components/ImagePreview/ImagePreview';
import { UpdateUserContactsRequest } from './../../interfaces/dto/updateUserContactsRequest';
import { UpdateUserDetailsRequest } from '../../interfaces/dto/updateUserDetailsRequest';
import { ISnackbar } from './../../interfaces/snackbar';
import { Snackbar } from '../../enums/snackbar';

interface Props {
  user: User;
  showSnackbar: (snackbar: ISnackbar) => void;
  deleteUserImage: () => Promise<void>;
  changeUserImage: (formData: FormData) => Promise<void>;
  updateUserContacts: (request: UpdateUserContactsRequest) => Promise<void>;
  updateUserDetails: (request: UpdateUserDetailsRequest) => Promise<void>;
}

interface State {
  profileImage: File | null;
  passwordOne: string;
  passwordTwo: string;
  phoneNumberOne: string;
  phoneNumberTwo: string;
  phoneNumberThree: string;
  imageValidation: any;
  detailsValidation: any;
  contactsValidation: any;
};

class SettingsPage extends React.PureComponent<Props, State> {
  imageValidator: FormValidator;
  detailsValidator: FormValidator;
  contactsValidator: FormValidator;
  fileInputRef: any;

  constructor(props) {
    super(props);

    this.imageValidator = new FormValidator([
      {
        field: 'profileImage',
        method: validator.isEmpty,
        validWhen: false,
        message: 'Choose an image you want to upload.'
      }
    ]);

    this.contactsValidator = new FormValidator([
      {
        field: 'phoneNumberOne',
        method: this.phoneAllowed,
        validWhen: true,
        message: 'Please provide a valid US phone number or keep it empty.'
      },
      {
        field: 'phoneNumberTwo',
        method: this.phoneAllowed,
        validWhen: true,
        message: 'Please provide a valid US phone number or keep it empty.'
      },
      {
        field: 'phoneNumberThree',
        method: this.phoneAllowed,
        validWhen: true,
        message: 'Please provide a valid US phone number or keep it empty.'
      }
    ]);

    this.detailsValidator = new FormValidator([
      {
        field: 'passwordOne',
        method: validator.isEmpty,
        validWhen: false,
        message: 'Please provide a password.'
      },
      {
        field: 'passwordTwo',
        method: this.passwordMatch,
        validWhen: true,
        message: 'Password should be the same as the one above.'
      }
    ]);

    this.state = {
      profileImage: null,
      passwordOne: '',
      passwordTwo: '',
      phoneNumberOne: this.props.user && this.props.user.phoneNumber1 || '',
      phoneNumberTwo: this.props.user && this.props.user.phoneNumber2 || '',
      phoneNumberThree: this.props.user && this.props.user.phoneNumber3 || '',
      imageValidation: this.imageValidator.valid(),
      detailsValidation: this.detailsValidator.valid(),
      contactsValidation: this.contactsValidator.valid()
    };
    this.fileInputRef = React.createRef();
  }

  componentDidUpdate(prevProps: Props) {
    if (this.props.user && this.props.user != prevProps.user) {
      this.setState({
        phoneNumberOne: this.props.user.phoneNumber1 || '',
        phoneNumberTwo: this.props.user.phoneNumber2 || '',
        phoneNumberThree: this.props.user.phoneNumber3 || ''
      });
    }
  }

  phoneAllowed = (value: string) => {
    return !value || validator.isMobilePhone(value, 'en-US');
  }

  imageAllowed = () => {
    return this.state.profileImage && this.state.profileImage.size <= 5000000; // <= ~5Mb
  }

  passwordMatch = (passwordTwo: string, state: State) => {
    return state.passwordOne === passwordTwo;
  }

  onImageDelete = () => {
    if (this.state.profileImage) {
      this.setState({
        profileImage: null
      });
      this.fileInputRef.current.value = null;
    } else {
      this.props.deleteUserImage()
        .then(() => {
          this.props.showSnackbar({
            type: Snackbar.Success,
            message: `Profile image was successfully deleted.`
          });
        })
        .catch(error => {
          this.props.showSnackbar({
            type: Snackbar.Error,
            message: error.message
          });
        });
    }
  }

  onImageSave = (e) => {
    e.preventDefault();

    const validation = this.imageValidator.validate({
      profileImage: this.state.profileImage || ''
    });

    this.setState(state => ({
      ...state,
      imageValidation: validation
    }));

    if (validation.isValid) {
      const formData = new FormData();
      formData.append('image', this.state.profileImage as Blob);

      this.props.changeUserImage(formData)
        .then(() => {
          this.setState({
            profileImage: null
          });
          this.fileInputRef.current.value = null;

          this.props.showSnackbar({
            type: Snackbar.Success,
            message: `Profile image was successfully changed.`
          });
        })
        .catch(error => {
          this.props.showSnackbar({
            type: Snackbar.Error,
            message: error.message
          });
        });
    }
  }

  onDetailsSave = (e) => {
    e.preventDefault();

    const validation = this.detailsValidator.validate(this.state);

    this.setState(state => ({
      ...state,
      detailsValidation: validation
    }));

    if (validation.isValid) {
      this.props.updateUserDetails({
        password: this.state.passwordOne
      })
        .then(() => {
          this.setState({
            passwordOne: '',
            passwordTwo: '',
          });
          this.props.showSnackbar({
            type: Snackbar.Success,
            message: `Details was successfully updated.`
          });
        })
        .catch(error => {
          this.props.showSnackbar({
            type: Snackbar.Error,
            message: error.message
          });
        });
    }
  }

  onContactsSave = (e) => {
    e.preventDefault();

    const validation = this.contactsValidator.validate(this.state);

    this.setState(state => ({
      ...state,
      contactsValidation: validation
    }));

    if (validation.isValid) {
      this.props.updateUserContacts({
        phoneNumberOne: this.state.phoneNumberOne,
        phoneNumberTwo: this.state.phoneNumberTwo,
        phoneNumberThree: this.state.phoneNumberThree,
      })
        .then(() => {
          this.props.showSnackbar({
            type: Snackbar.Success,
            message: `Contact information was successfully updated.`
          });
        })
        .catch(error => {
          this.props.showSnackbar({
            type: Snackbar.Error,
            message: error.message
          });
        });
    }
  }

  onValueChange(name: string, value: any) {
    this.setState({
      ...this.state,
      [name]: value
    });
  }

  render() {
    const {
      profileImage,
      imageValidation,
      contactsValidation,
      detailsValidation,
    } = this.state;
    const user = this.props.user;
    const currentProfileImageUrl = user && user.getImageUrl();

    return (
      <React.Fragment>
        <Typography variant="display1" gutterBottom>Settings</Typography>
        <Typography variant="subheading" gutterBottom>Here you can configure your profile settings</Typography>
        <section className={styles.section}>
          <Typography variant="body2" gutterBottom className={styles.header}>
            <span className={styles.point}><CheckIcon className={styles.icon} /></span>Upload profile image
          </Typography>
          <div className={styles.content}>
            <form
              onSubmit={this.onImageSave}
              className={styles.form}
              noValidate
            >
              <div className={`${styles.fieldWrapper} ${imageValidation.profileImage.isInvalid && styles.hasError}`}>
                <div className={styles.profileImageWrapper}>
                  {!currentProfileImageUrl && <Typography variant="display1" component="span">{user && user.getAvatarLetters()}</Typography>}
                  {currentProfileImageUrl && <img src={currentProfileImageUrl} className={styles.profileImage} />}
                  {
                    profileImage && (
                      <ImagePreview
                        image={profileImage}
                        showProgress={false}
                        containerClassName={styles.previewImageContainer}
                        imageClassName={styles.previewImage}
                      />
                    )
                  }
                  <IconButton className={styles.uploadImageButton}>
                    <div className={styles.uploadImageTooltip}>Hover to change</div>
                    <input
                      type="file"
                      ref={this.fileInputRef}
                      onChange={e => this.onValueChange('profileImage', e.target.files ? e.target.files[0] : null)}
                    />
                    <CloudUploadIcon color="secondary" className={styles.uploadImageIcon} />
                  </IconButton>
                </div>
                <Typography className={`${styles.errorLabel} ${styles.image}`}>{imageValidation.profileImage.message}</Typography>
              </div>
              <LoadingButton
                className={styles.button}
                isLoading={false}
                type="submit"
                variant="contained"
                color="primary"
              >
                Save image
              </LoadingButton>
              {
                (currentProfileImageUrl || this.state.profileImage) && (
                  <LoadingButton
                    className={`${styles.button} ${styles.deleteButton}`}
                    isLoading={false}
                    variant="contained"
                    color="secondary"
                    onClick={this.onImageDelete}
                  >
                    Delete image
                  </LoadingButton>
                )
              }
            </form>
          </div>
        </section>
        <section className={styles.section}>
          <Typography variant="body2" gutterBottom className={styles.header}>
            <span className={styles.point}><CheckIcon className={styles.icon} /></span>Update contact info
          </Typography>
          <div className={styles.content}>
            <form
              onSubmit={this.onContactsSave}
              className={styles.form}
              noValidate
            >
              <div className={`${styles.fieldWrapper} ${contactsValidation.phoneNumberOne.isInvalid && styles.hasError}`}>
                <Label label="Contact phone number №1" />
                <Input
                  id="phoneNumberOne"
                  placeholder="Enter contact №1"
                  value={this.state.phoneNumberOne}
                  className={styles.field}
                  onChange={event => this.onValueChange('phoneNumberOne', event.target.value)}
                />
                <Typography className={styles.errorLabel}>{contactsValidation.phoneNumberOne.message}</Typography>
              </div>
              <div className={`${styles.fieldWrapper} ${contactsValidation.phoneNumberTwo.isInvalid && styles.hasError}`}>
                <Label label="Contact phone number №2" />
                <Input
                  id="phoneNumberTwo"
                  placeholder="Enter contact №2"
                  value={this.state.phoneNumberTwo}
                  className={styles.field}
                  onChange={event => this.onValueChange('phoneNumberTwo', event.target.value)}
                />
                <Typography className={styles.errorLabel}>{contactsValidation.phoneNumberTwo.message}</Typography>
              </div>
              <div className={`${styles.fieldWrapper} ${contactsValidation.phoneNumberThree.isInvalid && styles.hasError}`}>
                <Label label="Contact phone number №3" />
                <Input
                  id="phoneNumberThree"
                  placeholder="Enter contact №3"
                  value={this.state.phoneNumberThree}
                  className={styles.field}
                  onChange={event => this.onValueChange('phoneNumberThree', event.target.value)}
                />
                <Typography className={styles.errorLabel}>{contactsValidation.phoneNumberThree.message}</Typography>
              </div>
              <LoadingButton
                className={styles.button}
                isLoading={false}
                type="submit"
                variant="contained"
                color="primary">
                Save contacts
              </LoadingButton>
            </form>
          </div>
        </section>
        <section className={styles.section}>
          <Typography variant="body2" gutterBottom className={styles.header}>
            <span className={styles.point}><CheckIcon className={styles.icon} /></span>Change password
          </Typography>
          <div className={styles.content}>
            <form
              onSubmit={this.onDetailsSave}
              className={styles.form}
              noValidate
            >
              <div className={`${styles.fieldWrapper} ${detailsValidation.passwordOne.isInvalid && styles.hasError}`}>
                <Label label="New password" />
                <Input
                  id="passwordOne"
                  label="Password"
                  placeholder="Think up new password"
                  type="password"
                  className={styles.field}
                  value={this.state.passwordOne}
                  onChange={event => this.onValueChange('passwordOne', event.target.value)}
                />
                <Typography className={styles.errorLabel}>{detailsValidation.passwordOne.message}</Typography>
              </div>
              <div className={`${styles.fieldWrapper} ${detailsValidation.passwordTwo.isInvalid && styles.hasError}`}>
                <Label label="Reapeat new password" />
                <Input
                  id="passwordTwo"
                  label="Reapeat password"
                  placeholder="Repeat the password above"
                  type="password"
                  className={styles.field}
                  value={this.state.passwordTwo}
                  onChange={event => this.onValueChange('passwordTwo', event.target.value)}
                />
                <Typography className={styles.errorLabel}>{detailsValidation.passwordTwo.message}</Typography>
              </div>
              <LoadingButton
                className={styles.button}
                isLoading={false}
                type="submit"
                variant="contained"
                color="primary">
                Save changes
              </LoadingButton>
            </form>
          </div>
        </section>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state: AppState) => ({
  user: state.user.authUser
});

const mapDispatchToProps = dispatch => ({
  showSnackbar: type => dispatch(showSnackbar(type)),
  changeUserImage: (formData: FormData) => dispatch(changeUserImage(formData)),
  deleteUserImage: () => dispatch(deleteUserImage()),
  updateUserContacts: (request: UpdateUserContactsRequest) => dispatch(updateUserContacts(request)),
  updateUserDetails: (request: UpdateUserDetailsRequest) => dispatch(updateUserDetails(request)),
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(SettingsPage);