import * as React from 'react';
import { connect } from 'react-redux';
import * as styles from './PublishedProperties.scss';
import Typography from '@material-ui/core/Typography';
import { AppState } from '../../state/appState';
import { State as PublishedPropertiesState } from '../../state/reducers/publishedProperties';
import { fetchPublishedProperties, changePropertyStatus, deleteProperty } from '../../state/actions/publishedProperties';
import InfiniteScrollList from './../../components/InfiniteScrollList';
import PublishedPropertyCard from './../../components/PublishedPropertyCard/PublishedPropertyCard';
import CircularProgress from '@material-ui/core/CircularProgress';
import { ChangePropertyStatusRequest } from './../../interfaces/dto/changePropertyStatusRequest';
import { PropertyStatus } from '../../enums/propertyStatus';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import LoadingButton from '../../components/LoadingButton/LoadingButton';
import * as routes from '../../constants/routes';
import { showSnackbar } from './../../state/actions/overlays';
import { ISnackbar } from './../../interfaces/snackbar';
import { Snackbar } from '../../enums/snackbar';

interface Props {
  publishedProperties: PublishedPropertiesState;
  fetchPublishedProperties: (initialLoad?: boolean) => Promise<void>;
  changePropertyStatus: (request: ChangePropertyStatusRequest) => Promise<void>;
  deleteProperty: (id: string) => Promise<void>;
  showSnackbar: (snackbar: ISnackbar) => Promise<void>;
};

interface State {
  deletingPropertyId: string | null;
  changingStatusPropertyId: string | null;
};

class PublishedPropertiesPage extends React.PureComponent<Props, State> {

  spinner: React.ReactNode = <CircularProgress className={styles.loadingSpinner} />;
  
  constructor(props: Props) {
    super(props);

    this.state = {
      deletingPropertyId: null,
      changingStatusPropertyId: null
    };
  }

  componentDidMount() {
    this.props.fetchPublishedProperties(true);
  }

  onScroll = () => {
    this.props.fetchPublishedProperties();
  }

  onStatusChange(id: string, status: PropertyStatus) {
    if (this.state.changingStatusPropertyId) {
      return;
    }

    this.setState({
      ...this.state,
      changingStatusPropertyId: id
    });

    this.props.changePropertyStatus({
      propertyId: id,
      propertyStatus: status
    })
      .then(() => {
        this.setState({
          ...this.state,
          changingStatusPropertyId: null
        });
      })
      .catch(() => {
        this.setState({
          ...this.state,
          changingStatusPropertyId: null
        });
      });
  }

  onDelete(id: string) {
    if (this.state.deletingPropertyId) {
      return;
    }

    this.setState({
      ...this.state,
      deletingPropertyId: id
    });

    this.props.deleteProperty(id)
      .then(() => {
        this.setState({
          ...this.state,
          deletingPropertyId: null
        });
      })
      .catch(error => {
        this.props.showSnackbar({
          type: Snackbar.Error,
          message: error.message
        });
        this.setState({
          ...this.state,
          deletingPropertyId: null
        });
      });
  }

  render() {
    return (
      <React.Fragment>
        <Typography variant="display1" gutterBottom>Published properties</Typography>
        <Typography variant="subheading" gutterBottom>Here you can see all properties that you added and which are avaliable under the search</Typography>
        <InfiniteScrollList
          onScroll={this.onScroll}
          isEmpty={this.props.publishedProperties.properties.length === 0}
          isLoading={this.props.publishedProperties.isLoading}
          canLoadMore={this.props.publishedProperties.canLoadMore}
          loadingIndicator={this.spinner}
          emptyMock="Empty..."
        >
          <div>
            {
              this.props.publishedProperties.properties.map((p, index) =>
                <PublishedPropertyCard
                  key={p.id}
                  property={p}
                >
                  <React.Fragment>
                    <Link to={`${routes.INCOMING_BOOKINGS}/${p.id}`}>
                      <Button
                        variant="contained"
                        color="primary"
                        className={styles.propertyCtrlButton}
                      >
                        Bookings
                     </Button>
                    </Link>
                    {p.status == PropertyStatus.Active && (
                      <LoadingButton
                        isLoading={p.id === this.state.changingStatusPropertyId}
                        color="primary"
                        className={styles.propertyCtrlButton}
                        onClick={() => this.onStatusChange(p.id, PropertyStatus.Inactive)}
                      >
                        Deactivate
                      </LoadingButton>
                    )}
                    {p.status == PropertyStatus.Inactive && (
                      <LoadingButton
                        isLoading={p.id === this.state.changingStatusPropertyId}
                        color="primary"
                        className={styles.propertyCtrlButton}
                        onClick={() => this.onStatusChange(p.id, PropertyStatus.Active)}
                      >
                        Activate
                      </LoadingButton>
                    )}
                    <LoadingButton
                      isLoading={p.id === this.state.deletingPropertyId}
                      variant="contained"
                      className={styles.propertyCtrlButton}
                      onClick={() => this.onDelete(p.id)}
                    >
                      Delete
                    </LoadingButton>
                  </React.Fragment>
                </PublishedPropertyCard>
              )
            }
          </div>
        </InfiniteScrollList>
      </React.Fragment>
    );
  }
}

const mapStateToProperties = (state: AppState) => ({
  publishedProperties: state.publishedProperties
});

const mapDispatchToProperties = dispatch => ({
  fetchPublishedProperties: initialLoad => dispatch(fetchPublishedProperties(initialLoad)),
  changePropertyStatus: request => dispatch(changePropertyStatus(request)),
  deleteProperty: id => dispatch(deleteProperty(id)),
  showSnackbar: snackbar => dispatch(showSnackbar(snackbar))
});

export default connect(
  mapStateToProperties,
  mapDispatchToProperties
)(PublishedPropertiesPage);