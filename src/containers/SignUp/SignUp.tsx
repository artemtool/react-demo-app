import * as React from 'react';
import { Link } from 'react-router-dom';
import * as styles from './SignUp.scss';
import Typography from '@material-ui/core/Typography';
import * as routes from '../../constants/routes';
import { compose } from '../../../node_modules/recompose';
import { connect } from 'react-redux';
import { signUpWithEmail } from './../../state/actions/user';
import { SIGN_IN } from './../../constants/routes';
import { SignUpRequest } from '../../interfaces/dto/signUpRequest';
import LoadingButton from './../../components/LoadingButton/LoadingButton';
import Input from '../../components/Input/Input';
import Label from './../../components/Label/Label';
import { showSnackbar } from './../../state/actions/overlays';
import { Snackbar } from './../../enums/snackbar';
import { ISnackbar } from './../../interfaces/snackbar';
import FormValidator from './../../common/formValidator';
import * as validator from 'validator';

interface Props {
  history: any;
  isSignUpProceeding: boolean;
  signUpWithEmail: (request: SignUpRequest) => Promise<void>;
  showSnackbar: (snackbar: ISnackbar) => void;
}

interface State {
  username: string;
  email: string;
  passwordOne: string;
  passwordTwo: string;
  validation: any;
}

const initialState: State = {
  username: '',
  email: '',
  passwordOne: '',
  passwordTwo: '',
  validation: null
};

class SignUpPage extends React.PureComponent<Props, State> {
  validator: FormValidator;

  constructor(props) {
    super(props);

    this.validator = new FormValidator([
      {
        field: 'username',
        method: validator.isAlpha,
        args: ['en-US'],
        validWhen: true,
        message: 'Please use only alphabetic characters for name.'
      },
      {
        field: 'email',
        method: validator.isEmail,
        validWhen: true,
        message: 'Please provide an email address.'
      },
      {
        field: 'passwordOne',
        method: validator.isEmpty,
        validWhen: false,
        message: 'Please provide a password.'
      },
      {
        field: 'passwordTwo',
        method: this.passwordMatch,
        validWhen: true,
        message: 'Password should be the same as the one above.'
      }
    ]);

    this.state = {
      ...initialState,
      validation: this.validator.valid()
    };
  }

  passwordMatch = (passwordTwo: string, state: State) => {
    return state.passwordOne === passwordTwo;
  }

  onSubmit = (event) => {
    event.preventDefault();

    const {
      username,
      email,
      passwordOne,
    } = this.state;

    const validation = this.validator.validate(this.state);

    this.setState(state => ({
      ...state,
      validation
    }));

    if (validation.isValid) {
      this.props.signUpWithEmail({
        name: username,
        email: email,
        password: passwordOne
      })
        .then(() => {
          this.props.history.push(SIGN_IN);
          this.props.showSnackbar({
            type: Snackbar.Success,
            message: `Account was created. Sign in, please.`
          });
        })
        .catch(error => {
          this.props.showSnackbar({
            type: Snackbar.Error,
            message: error.message
          });
        });
    }
  }

  onFormValueChange(name: string, value: string) {
    this.setState({
      ...this.state,
      [name]: value
    });
  }

  render() {
    const {
      username,
      email,
      passwordOne,
      passwordTwo,
      validation
    } = this.state;

    return (
      <React.Fragment>
        <Typography variant="display1" align="center" gutterBottom>Sign up</Typography>
        <Typography variant="subheading" align="center">Create an account for better experience</Typography>
        <form
          onSubmit={this.onSubmit}
          className={styles.form}
          noValidate>
          <div className={`${styles.fieldWrapper} ${validation.username.isInvalid && styles.hasError}`}>
            <Label label="Name"/>
            <Input
              id="name"
              label="Name"
              placeholder="Enter your name"
              className={styles.field}
              value={username}
              onChange={event => this.onFormValueChange('username', event.target.value)}
            />
            <Typography className={styles.errorLabel}>{validation.username.message}</Typography>
          </div>
          <div className={`${styles.fieldWrapper} ${validation.email.isInvalid && styles.hasError}`}>
            <Label label="Email"/>
            <Input
              id="email"
              label="Email"
              placeholder="Enter your email"
              className={styles.field}
              value={email}
              onChange={event => this.onFormValueChange('email', event.target.value)}
            />
            <Typography className={styles.errorLabel}>{validation.email.message}</Typography>
          </div>
          <div className={`${styles.fieldWrapper} ${validation.passwordOne.isInvalid && styles.hasError}`}>
            <Label label="Password"/>
            <Input
              id="passwordOne"
              label="Password"
              placeholder="Create password"
              type="password"
              className={styles.field}
              value={passwordOne}
              onChange={event => this.onFormValueChange('passwordOne', event.target.value)}
            />
            <Typography className={styles.errorLabel}>{validation.passwordOne.message}</Typography>
          </div>
          <div className={`${styles.fieldWrapper} ${validation.passwordTwo.isInvalid && styles.hasError}`}>
            <Label label="Reapeat password"/>
            <Input
              id="passwordTwo"
              label="Reapeat password"
              placeholder="Repeat password above"
              type="password"
              className={styles.field}
              value={passwordTwo}
              onChange={event => this.onFormValueChange('passwordTwo', event.target.value)}
            />
            <Typography className={styles.errorLabel}>{validation.passwordTwo.message}</Typography>
          </div>
          <LoadingButton
            className={styles.button}
            isLoading={this.props.isSignUpProceeding}
            type="submit"
            variant="contained"
            color="primary">
            Sign Up
          </LoadingButton>
        </form>
        <Typography variant="body2" align="center" className={styles.signInSuggestion}>
          Already have an account?
          <Link to={routes.SIGN_IN} className={styles.signInLink}>
            Sign in
          </Link>
        </Typography>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  isSignUpProceeding: state.user.isLoading
});

const mapDispatchToProps = dispatch => ({
  signUpWithEmail: (request: SignUpRequest) => dispatch(signUpWithEmail(request)),
  showSnackbar: type => dispatch(showSnackbar(type))
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(SignUpPage);