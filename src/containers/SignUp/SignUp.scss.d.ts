export const form: string;
export const fieldWrapper: string;
export const errorLabel: string;
export const hasError: string;
export const field: string;
export const button: string;
export const signInSuggestion: string;
export const signInLink: string;
