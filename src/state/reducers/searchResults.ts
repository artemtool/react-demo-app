import { Action } from "redux";
import { initReducer } from "../util/reducerInitializer";
import * as searchResults from "../actions/searchResults";
import * as bookings from "../actions/userBookings";
import { PropertyCard } from "../../models/propertyCard";

export interface State {
  properties: Array<PropertyCard>;
  isLoading: boolean;
  canLoadMore: boolean;
  page: number;
}

export const initialState: State = {
  properties: [],
  isLoading: false,
  canLoadMore: true,
  page: 1
}

const actionHandlers: { [actionType: string]: (state: State, action: Action) => State } = {
  [searchResults.REQUEST_PROPERTIES]: (state: State, action: searchResults.RequestPropriesAction): State => {
    const properties = action.initialLoad
      ? []
      : state.properties;
    const canLoadMore = action.initialLoad
      ? true
      : state.canLoadMore;
    const page = action.initialLoad
      ? 1
      : state.page;

    return {
      ...state,
      properties,
      canLoadMore,
      page,
      isLoading: true
    }

  },
  [searchResults.RECEIVE_PROPERTIES]: (state: State, action: searchResults.ReceivePropertiesAction): State => {
    const canLoadMore = action.properties.length > 0;
    return {
      ...state,
      properties: [...state.properties, ...action.properties],
      page: state.page + 1,
      isLoading: false,
      canLoadMore
    }

  },
  [searchResults.FAILURE_PROPERTIES]: (state: State): State => {
    return {
      ...state,
      isLoading: false
    }
  },
  [bookings.BOOK_PROPERTY_SUCCESS]: (state: State, action: bookings.BookPropertySuccessAction): State => {
    const index = state.properties.findIndex(p => p.id == action.booking.propertyId);

    if (index < 0) {
      return state;
    } else {
      state.properties.splice(index, 1);

      return {
        ...state,
        properties: [...state.properties]
      }
    }
  }
}

const searchResultsReducer = initReducer(initialState, actionHandlers);
export default searchResultsReducer;