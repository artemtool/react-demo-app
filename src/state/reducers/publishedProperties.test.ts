import publishedPropertiesReducer, { initialState } from "./publishedProperties";
import { testMock } from "../../testObjectsSet";
import { RequestPublishedPropriesAction, ReceivePublishedPropertiesAction, ChangePropertyStatusSuccessAction, DeletePropertySuccessAction } from "../actions/publishedProperties";
import { PropertyStatus } from "../../enums/propertyStatus";

describe('publishedProperties reducer', () => {

  it('should return the initial state', () => {
    expect(publishedPropertiesReducer(<any>undefined, {
      type: 'UNKNOWN'
    })).toEqual(initialState)
  });

  it('should handle REQUEST_PUBLISHED_PROPERTIES with initialLoad = true', () => {
    const stateBefore = { ...initialState };
    stateBefore.properties = testMock.propertyCard.getFirstSet(true);
    stateBefore.canLoadMore = false;
    stateBefore.page = 10;

    const action: RequestPublishedPropriesAction = {
      type: 'REQUEST_PUBLISHED_PROPERTIES',
      initialLoad: true
    };
    const stateAfter = publishedPropertiesReducer(stateBefore, action);

    expect(stateAfter.properties).toEqual([]);
    expect(stateAfter.canLoadMore).toBe(true);
    expect(stateAfter.page).toBe(1);
    expect(stateAfter.isLoading).toEqual(true);
  });

  it('should handle REQUEST_PUBLISHED_PROPERTIES with initialLoad = false', () => {
    const stateBefore = { ...initialState };
    const expectedProperties = testMock.propertyCard.getFirstSet(true);
    const expectedPage = 10;
    stateBefore.properties = expectedProperties;
    stateBefore.page = expectedPage;

    const action: RequestPublishedPropriesAction = {
      type: 'REQUEST_PUBLISHED_PROPERTIES',
      initialLoad: false
    };
    const stateAfter = publishedPropertiesReducer(stateBefore, action);

    expect(stateAfter.properties).toEqual(expectedProperties);
    expect(stateAfter.page).toBe(expectedPage);
    expect(stateAfter.isLoading).toEqual(true);
  });

  it('should handle RECEIVE_PUBLISHED_PROPERTIES', () => {
    const stateBefore = { ...initialState };
    const propertiesBefore = testMock.propertyCard.getFirstSet(true);
    const newProperties = testMock.propertyCard.getSecondSet(true);
    stateBefore.isLoading = true;
    stateBefore.properties = propertiesBefore;
    
    const action: ReceivePublishedPropertiesAction = {
      type: 'RECEIVE_PUBLISHED_PROPERTIES',
      properties: newProperties
    };
    const stateAfter = publishedPropertiesReducer(stateBefore, action);

    expect(stateAfter.properties).toEqual([...propertiesBefore, ...newProperties]);
    expect(stateAfter.page).toEqual(stateBefore.page + 1);
    expect(stateAfter.isLoading).toEqual(false);
  });

  it('should handle FAILURE_PUBLISHED_PROPERTIES', () => {
    const stateBefore = { ...initialState };
    stateBefore.isLoading = true;

    const stateAfter = publishedPropertiesReducer(stateBefore, {
      type: 'FAILURE_PUBLISHED_PROPERTIES'
    });

    expect(stateAfter.isLoading).toEqual(false);
  });

  it('should handle CHANGE_PROPERTY_STATUS_SUCCESS', () => {
    const stateBefore = { ...initialState };
    stateBefore.properties = testMock.propertyCard.getFirstSet(true);

    const propertyToChangeStatusId = testMock.propertyCard.getFirstSet(true)[0].id;
    const expectedStatus = PropertyStatus.Inactive;
    const action: ChangePropertyStatusSuccessAction = {
      type: 'CHANGE_PROPERTY_STATUS_SUCCESS',
      propertyId: propertyToChangeStatusId,
      propertyStatus: expectedStatus
    };
    const stateAfter = publishedPropertiesReducer(stateBefore, action);

    expect(stateAfter.properties.map(p => p.id))
      .toContain(propertyToChangeStatusId);
    expect(stateAfter.properties.find(p => p.id === propertyToChangeStatusId))
      .toHaveProperty('status', expectedStatus);
  });

  it('should handle CHANGE_PROPERTY_STATUS_SUCCESS with property id, that is not exist in the published properties array', () => {
    const stateBefore = { ...initialState };
    stateBefore.properties = testMock.propertyCard.getFirstSet(true);

    const action: ChangePropertyStatusSuccessAction = {
      type: 'CHANGE_PROPERTY_STATUS_SUCCESS',
      propertyId: 'non-existent id',
      propertyStatus: 0
    };
    const stateAfter = publishedPropertiesReducer(stateBefore, action);

    expect(stateAfter).toBe(stateBefore);
  });

  it('should handle DELETE_PROPERTY_SUCCESS', () => {
    const stateBefore = { ...initialState };
    stateBefore.properties = testMock.propertyCard.getFirstSet(true);

    const propertyToDeleteId = testMock.propertyCard.getFirstSet(true)[0].id;
    const action: DeletePropertySuccessAction = {
      type: 'DELETE_PROPERTY_SUCCESS',
      id: propertyToDeleteId
    };
    const stateAfter = publishedPropertiesReducer(stateBefore, action);

    expect(stateAfter.properties.filter(p => p.id != propertyToDeleteId))
      .toEqual(stateBefore.properties);
  });

  it('should handle DELETE_PROPERTY_SUCCESS with property id, that is not exist in the published properties array', () => {
    const stateBefore = { ...initialState };
    stateBefore.properties = testMock.propertyCard.getFirstSet(true);

    const action: DeletePropertySuccessAction = {
      type: 'DELETE_PROPERTY_SUCCESS',
      id: 'non-existent id'
    };
    const stateAfter = publishedPropertiesReducer(stateBefore, action);

    expect(stateAfter).toEqual(stateBefore);
  });
});