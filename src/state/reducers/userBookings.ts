import { Action } from 'redux';
import { initReducer } from '../util/reducerInitializer';
import { PropertyBooking } from './../../models/propertyBooking';
import * as userBookings from '../actions/userBookings';

export interface State {
  bookings: Array<PropertyBooking>;
  isLoading: boolean;
  canLoadMore: boolean;
  page: number;
};

export const initialState: State = {
  bookings: [],
  isLoading: false,
  canLoadMore: true,
  page: 1
};

const actionHandlers: { [actionType: string]: (state: State, action: Action) => State } = {
  [userBookings.REQUEST_USER_BOOKINGS]: (state: State, action: userBookings.RequestUserBookingsAction): State => {
    const bookings = action.initialLoad
      ? []
      : state.bookings;
    const canLoadMore = action.initialLoad
      ? true
      : state.canLoadMore;
    const page = action.initialLoad
      ? 1
      : state.page;

    return {
      ...state,
      bookings,
      canLoadMore,
      page,
      isLoading: true
    }

  },
  [userBookings.RECEIVE_USER_BOOKINGS]: (state: State, action: userBookings.ReceiveUserBookingsAction): State => {
    const canLoadMore = action.bookings.length > 0;
    return {
      ...state,
      bookings: [...state.bookings, ...action.bookings],
      page: state.page + 1,
      isLoading: false,
      canLoadMore
    }

  },
  [userBookings.FAILURE_USER_BOOKINGS]: (state: State): State => {
    return {
      ...state,
      isLoading: false
    }
  }
};

const userBookingsReducer = initReducer(initialState, actionHandlers);
export default userBookingsReducer;