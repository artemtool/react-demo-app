import { Action } from "redux";
import { initReducer } from "../util/reducerInitializer";
import * as user from "../actions/user";
import { User } from "../../models/user";

export interface State {
  authUser: User | null;
  isLoading: boolean;
}

export const initialState: State = {
  authUser: null,
  isLoading: false
};

const actionHandlers: { [actionType: string]: (state: State, action: Action) => State } = {
  [user.SIGN_IN]: (state: State): State => {
    return {
      ...state,
      isLoading: true
    }
  },
  [user.SIGN_UP]: (state: State): State => {
    return {
      ...state,
      isLoading: true
    }
  },
  [user.SIGN_OUT]: (state: State): State => {
    return {
      ...state,
      isLoading: true
    }
  },
  [user.SIGN_IN_SUCCESS]: (state: State, action: user.SignInSuccessAction): State => {
    return {
      ...state,
      isLoading: false,
      authUser: action.user
    }
  },
  [user.SIGN_UP_SUCCESS]: (state: State): State => {
    return {
      ...state,
      isLoading: false
    }
  },
  [user.SIGN_OUT_SUCCESS]: (state: State): State => {
    return {
      ...state,
      isLoading: false,
      authUser: null
    }
  },
  [user.SIGN_IN_FAILURE]: (state: State): State => {
    return {
      ...state,
      isLoading: false
    }
  },
  [user.SIGN_UP_FAILURE]: (state: State): State => {
    return {
      ...state,
      isLoading: false
    }
  },
  [user.SIGN_OUT_FAILURE]: (state: State): State => {
    return {
      ...state,
      isLoading: false
    }
  },
  [user.UPDATE_USER_SUCCESS]: (state: State, action: user.UpdateUserSuccessAction): State => {
    return {
      ...state,
      authUser: action.user
    }
  }
};

const userReducer = initReducer(initialState, actionHandlers);
export default userReducer;