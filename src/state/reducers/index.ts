import { combineReducers } from 'redux';
import searchResults from "./searchResults";
import searchFilters from './searchFilters';
import user from './user';
import overlays from './overlays';
import userBookings from './userBookings';
import incomingBookings from './incomingBookings';
import publishedProperties from './publishedProperties';
import property from './property';
import messanger from './messanger';

const appReducer = combineReducers({
  searchResults,
  searchFilters,
  user,
  overlays,
  userBookings,
  incomingBookings,
  publishedProperties,
  property,
  messanger
});

export default appReducer;