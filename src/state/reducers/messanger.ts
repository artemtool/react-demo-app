import { Action } from 'redux';
import { initReducer } from '../util/reducerInitializer';
import { MessangerState } from '../../interfaces/messangerState';
import { Message } from '../../models/Message';
import * as messanger from '../actions/messanger';

export interface State {
  states: Array<MessangerState>;
};

export const initialState: State = {
  states: []
};

const actionHandlers: { [actionType: string]: (state: State, action: Action) => State } = {
  [messanger.PUSH_MESSAGE]: (state: State, action: messanger.PushMessageAction): State => {
    const messangerStateIndex = state.states.findIndex(s => s.connectedEntityId === action.message.propertyBookingId);

    if (messangerStateIndex >= 0) {
      const messangerState = state.states.splice(messangerStateIndex, 1)[0];

      return {
        ...state,
        states: [
          ...state.states,
          {
            ...messangerState,
            messages: [...messangerState.messages, action.message]
          }
        ]
      };
    } else {
      return {
        ...state,
        states: [
          ...state.states,
          {
            isLoading: false,
            canLoadMore: false,
            connectedEntityId: action.message.propertyBookingId,
            page: 1,
            messages: [action.message]
          }
        ]
      };
    }
  },
  [messanger.CHANGE_MESSAGE_STATUS]: (state: State, action: messanger.ChangeMessageStatusAction): State => {
    const messangerStateIndex = state.states.findIndex(s => s.connectedEntityId === action.bookingId);

    if (messangerStateIndex >= 0) {
      const messangerState = state.states.splice(messangerStateIndex, 1)[0];
      const messageIndex = messangerState.messages.findIndex(m => m.id === action.messageId);

      if (messageIndex >= 0) {
        const message = messangerState.messages.splice(messageIndex, 1)[0];
        const modifiedMessage = new Message({
          id: message.id,
          senderId: message.senderId,
          propertyBookingId: message.propertyBookingId,
          date: message.date.toISOString(),
          text: message.text,
          status: action.status
        }, message.isCurrentUserSender);

        return {
          ...state,
          states: [...state.states, <MessangerState>{
            ...messangerState,
            messages: [...messangerState.messages, modifiedMessage]
          }],
        };
      } else {
        return state;
      }
    } else {
      return state;
    }
  },
  [messanger.REQUEST_MESSAGES]: (state: State, action: messanger.RequestMessagesAction): State => {
    const messangerStateIndex = state.states.findIndex(s => s.connectedEntityId === action.bookingId);

    if (messangerStateIndex >= 0) {
      const messangerState = state.states.splice(messangerStateIndex, 1)[0];

      const messages = action.initialLoad
        ? []
        : messangerState.messages;
      const canLoadMore = action.initialLoad
        ? true
        : messangerState.canLoadMore;
      const page = action.initialLoad
        ? 1
        : messangerState.page;

      return {
        ...state,
        states: [...state.states, <MessangerState>{
          ...messangerState,
          isLoading: true,
          messages,
          canLoadMore,
          page
        }],
      };
    } else {
      return {
        ...state,
        states: [...state.states, <MessangerState>{
          connectedEntityId: action.bookingId,
          isLoading: true,
          messages: [],
          canLoadMore: true,
          page: 1
        }],
      };
    }
  },
  [messanger.RECEIVE_MESSAGES]: (state: State, action: messanger.ReceiveMessagesAction): State => {
    const messangerStateIndex = state.states.findIndex(s => s.connectedEntityId === action.bookingId);
    const messangerState = state.states.splice(messangerStateIndex, 1)[0];

    return {
      ...state,
      states: [...state.states, <MessangerState>{
        ...messangerState,
        isLoading: false,
        messages: [...action.messages, ...messangerState.messages],
        page: messangerState.page + 1,
        canLoadMore: action.messages.length > 0
      }],
    };
  }
};

const messangerReducer = initReducer(initialState, actionHandlers);
export default messangerReducer; 