import { Action } from 'redux';
import { initReducer } from '../util/reducerInitializer';
import { PropertyBooking } from './../../models/propertyBooking';
import * as incomingBookings from '../actions/incomingBookings';
import { PropertyCard } from './../../models/propertyCard';
import { NewPropertyBookings } from './../../interfaces/newIncomingBookings';

export interface State {
  bookings: Array<PropertyBooking>;
  isLoading: boolean;
  canLoadMore: boolean;
  page: number;
  suggestions: Array<PropertyCard>;
  areSuggestionsLoading: boolean;
  selectedProperty: PropertyCard | null;
  newPropertyBookings: Array<NewPropertyBookings>;
};

export const initialState: State = {
  bookings: [],
  isLoading: false,
  canLoadMore: true,
  page: 1,
  suggestions: [],
  areSuggestionsLoading: false,
  selectedProperty: null,
  newPropertyBookings: []
};

const actionHandlers: { [actionType: string]: (state: State, action: Action) => State } = {
  [incomingBookings.SET_SELECTED_PROPERTY]: (state: State, action: incomingBookings.SetSelectedPropertyAction): State => {
    const bookings = action.property
      ? state.bookings
      : [];
    const suggestions = action.property
      ? state.suggestions
      : [];

    if (action.property) {
      const propertyId = action.property.id;
      const addPropertyToSuggestions = !suggestions.find(s => s.id === propertyId);

      if (addPropertyToSuggestions) {
        suggestions.push(action.property);
      }
    }

    return {
      ...state,
      selectedProperty: action.property,
      bookings,
      suggestions
    }
  },
  [incomingBookings.REQUEST_INCOMING_BOOKINGS]: (state: State, action: incomingBookings.RequestIncomingBookingsAction): State => {
    const bookings = action.initialLoad
      ? []
      : state.bookings;
    const canLoadMore = action.initialLoad
      ? true
      : state.canLoadMore;
    const page = action.initialLoad
      ? 1
      : state.page;

    return {
      ...state,
      bookings,
      canLoadMore,
      page,
      isLoading: true
    }

  },
  [incomingBookings.RECEIVE_INCOMING_BOOKINGS]: (state: State, action: incomingBookings.ReceiveIncomingBookingsAction): State => {
    const canLoadMore = action.bookings.length > 0;
    return {
      ...state,
      bookings: [...state.bookings, ...action.bookings],
      page: state.page + 1,
      isLoading: false,
      canLoadMore
    }

  },
  [incomingBookings.FAILURE_INCOMING_BOOKINGS]: (state: State): State => {
    return {
      ...state,
      isLoading: false
    }
  },
  [incomingBookings.REQUEST_PROPERTY_SUGGESTIONS]: (state: State): State => {
    return {
      ...state,
      areSuggestionsLoading: true
    }
  },
  [incomingBookings.RECEIVE_PROPERTY_SUGGESTIONS]: (state: State, action: incomingBookings.ReceivePropertySuggestionsAction): State => {
    return {
      ...state,
      suggestions: [...action.suggestions],
      areSuggestionsLoading: false
    }
  },
  [incomingBookings.FAILURE_PROPERTY_SUGGESTIONS]: (state: State): State => {
    return {
      ...state,
      areSuggestionsLoading: false
    }
  },
  [incomingBookings.RECEIVE_NEW_PROPERTY_BOOKINGS]: (state: State, action: incomingBookings.ReceiveNewPropertyBookingsAction): State => {
    return {
      ...state,
      newPropertyBookings: action.newPropertyBookings
    }
  },
  [incomingBookings.MARK_NEW_PROPERTY_BOOKINGS_AS_SEEN]: (state: State, action: incomingBookings.MarkNewPropertyBookingsAsSeenAction): State => {
    return {
      ...state,
      newPropertyBookings: state.newPropertyBookings.filter(p => p.propertyId !== action.propertyId)
    }
  },
  [incomingBookings.RECEIVE_NEW_PROPERTY_BOOKING]: (state: State, action: incomingBookings.ReceiveNewPropertyBookingAction): State => {
    const newPropertyBookingsIndex = state.newPropertyBookings.findIndex(b => b.propertyId == action.newBooking.propertyId);
    if (newPropertyBookingsIndex >= 0) {
      const newPropertyBookings = state.newPropertyBookings.splice(newPropertyBookingsIndex, 1)[0];
      return {
        ...state,
        newPropertyBookings: [
          {
            ...newPropertyBookings,
            count: newPropertyBookings.count + 1
          },
          ...state.newPropertyBookings
        ]
      };
    } else {
      return {
        ...state,
        newPropertyBookings: [
          {
            propertyId: action.newBooking.propertyId,
            propertyAddress: action.newBooking.propertyAddress,
            count: 1
          },
          ...state.newPropertyBookings
        ]
      };
    }
  }
};

const incomingBookingsReducer = initReducer(initialState, actionHandlers);
export default incomingBookingsReducer;