import incomingBookingsReducer, { initialState } from "./incomingBookings";
import { ReceiveIncomingBookingsAction, RequestIncomingBookingsAction, ReceivePropertySuggestionsAction, SetSelectedPropertyAction } from "../actions/incomingBookings";
import { testMock } from './../../testObjectsSet';

describe('incomingBookings reducer', () => {

  it('should return the initial state', () => {
    expect(incomingBookingsReducer(<any>undefined, {
      type: 'UNKNOWN'
    })).toEqual(initialState);
  });

  it('should handle SET_SELECTED_PROPERTY', () => {
    const stateBefore = { ...initialState };
    stateBefore.bookings = testMock.booking.getFirstSet(true);
    stateBefore.suggestions = testMock.propertyCard.getFirstSet(true);

    const expectedProperty = testMock.propertyCard.getSecondSet(true)[0];
    const action: SetSelectedPropertyAction = {
      type: 'SET_SELECTED_PROPERTY',
      property: expectedProperty
    };
    const stateAfter = incomingBookingsReducer(stateBefore, action);

    expect(stateAfter.selectedProperty).toBe(expectedProperty);
    expect(stateAfter.suggestions).toContain(expectedProperty);
  });

  it('should handle SET_SELECTED_PROPERTY with NULL payload', () => {
    const stateBefore = { ...initialState };
    stateBefore.bookings = testMock.booking.getFirstSet(true);
    stateBefore.suggestions = testMock.propertyCard.getFirstSet(true);

    const action: SetSelectedPropertyAction = {
      type: 'SET_SELECTED_PROPERTY',
      property: null
    };
    const stateAfter = incomingBookingsReducer(stateBefore, action);

    expect(stateAfter.selectedProperty).toBeNull();
    expect(stateAfter.suggestions).toEqual([]);
    expect(stateAfter.bookings).toEqual([]);
  });

  it('should handle REQUEST_INCOMING_BOOKINGS with initialLoad = true', () => {
    const stateBefore = { ...initialState };
    stateBefore.bookings = testMock.booking.getFirstSet(true);
    stateBefore.canLoadMore = false;
    stateBefore.page = 10;

    const action: RequestIncomingBookingsAction = {
      type: 'REQUEST_INCOMING_BOOKINGS',
      initialLoad: true
    };
    const stateAfter = incomingBookingsReducer(stateBefore, action);

    expect(stateAfter.bookings).toEqual([]);
    expect(stateAfter.canLoadMore).toBe(true);
    expect(stateAfter.page).toBe(1);
    expect(stateAfter.isLoading).toEqual(true);
  });

  it('should handle REQUEST_INCOMING_BOOKINGS with initialLoad = false', () => {
    const stateBefore = { ...initialState };
    const expectedBookings = testMock.booking.getFirstSet(true);
    const expectedPage = 10;
    stateBefore.bookings = expectedBookings;
    stateBefore.page = expectedPage;

    const action: RequestIncomingBookingsAction = {
      type: 'REQUEST_INCOMING_BOOKINGS',
      initialLoad: false
    };
    const stateAfter = incomingBookingsReducer(stateBefore, action);

    expect(stateAfter.bookings).toEqual(expectedBookings);
    expect(stateAfter.page).toBe(expectedPage);
    expect(stateAfter.isLoading).toEqual(true);
  });

  it('should handle RECEIVE_INCOMING_BOOKINGS', () => {
    const stateBefore = { ...initialState };
    const bookingsBefore = testMock.booking.getFirstSet(true);
    const newBookings = testMock.booking.getSecondSet(true);
    stateBefore.isLoading = true;
    stateBefore.bookings = bookingsBefore;

    const action: ReceiveIncomingBookingsAction = {
      type: 'RECEIVE_INCOMING_BOOKINGS',
      bookings: testMock.booking.getSecondSet(true)
    };
    const stateAfter = incomingBookingsReducer(stateBefore, action);

    expect(stateAfter.bookings).toEqual([...bookingsBefore, ...newBookings]);
    expect(stateAfter.page).toEqual(stateBefore.page + 1);
    expect(stateAfter.isLoading).toEqual(false);
  });

  it('should handle FAILURE_INCOMING_BOOKINGS', () => {
    const stateBefore = { ...initialState };
    stateBefore.isLoading = true;

    const stateAfter = incomingBookingsReducer(stateBefore, {
      type: 'FAILURE_INCOMING_BOOKINGS'
    });

    expect(stateAfter.isLoading).toEqual(false);
  });

  it('should handle REQUEST_PROPERTY_SUGGESTIONS', () => {
    const stateAfter = incomingBookingsReducer(<any>undefined, {
      type: 'REQUEST_PROPERTY_SUGGESTIONS'
    });

    expect(stateAfter.areSuggestionsLoading).toEqual(true);
  });

  it('should handle RECEIVE_PROPERTY_SUGGESTIONS', () => {
    const stateBefore = { ...initialState };
    stateBefore.areSuggestionsLoading = true;

    const expectedSuggestions = testMock.propertyCard.getFirstSet(true)
    const action: ReceivePropertySuggestionsAction = {
      type: 'RECEIVE_PROPERTY_SUGGESTIONS',
      suggestions: expectedSuggestions
    };
    const stateAfter = incomingBookingsReducer(stateBefore, action);

    expect(stateAfter.areSuggestionsLoading).toEqual(false);
    expect(stateAfter.suggestions).toEqual(expectedSuggestions);
  });

  it('should handle FAILURE_PROPERTY_SUGGESTIONS', () => {
    const stateBefore = { ...initialState };
    stateBefore.areSuggestionsLoading = true;

    const stateAfter = incomingBookingsReducer(stateBefore, {
      type: 'FAILURE_PROPERTY_SUGGESTIONS'
    });

    expect(stateAfter.areSuggestionsLoading).toEqual(false);
  });
});