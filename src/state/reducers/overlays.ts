import { Action } from 'redux';
import { initReducer } from '../util/reducerInitializer';
import * as overlays from '../actions/overlays';
import { IDialog } from './../../interfaces/dialog';
import { ISnackbar } from './../../interfaces/snackbar';

export interface State {
  dialogHistory: Array <IDialog>;
  currentDialog: IDialog | null;
  currentSnackbar: ISnackbar | null;
};

export const initialState: State = {
  dialogHistory: [],
  currentDialog: null,
  currentSnackbar: null
};

const actionHandlers: { [actionType: string]: (state: State, action: Action) => State } = {
  [overlays.SHOW_DIALOG]: (state: State, action: overlays.ShowDialogAction): State => {
    const history = state.dialogHistory;
    if (state.currentDialog) {
      history.push(state.currentDialog);
    }

    return {
      ...state,
      dialogHistory: [...history],
      currentDialog: action.dialog
    }
  },
  [overlays.HIDE_DIALOG]: (state: State, action: overlays.HideDialogAction): State => {
    if (state.currentDialog && state.currentDialog.type === action.dialogType) {
      return {
        ...state,
        dialogHistory: [],
        currentDialog: null
      }
    } else {
      return state;
    }
  },
  [overlays.SHOW_SNACKBAR]: (state: State, action: overlays.ShowSnackbarAction): State => {
    return {
      ...state,
      currentSnackbar: action.snackbar
    }
  },
  [overlays.HIDE_SNACKBAR]: (state: State, action: overlays.HideSnackbarAction): State => {
    if (state.currentSnackbar && state.currentSnackbar.type === action.snackbarType) {
      return {
        ...state,
        currentSnackbar: null
      }
    } else {
      return state;
    }
  }
};

const overlaysReducer = initReducer(initialState, actionHandlers);
export default overlaysReducer; 