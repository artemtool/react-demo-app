import { Action } from "redux";
import { initReducer } from "../util/reducerInitializer";
import * as property from "../actions/property";
import { Property } from "../../models/property";

export interface State {
  model: Property | null;
  isLoading: boolean;
}

export const initialState: State = {
  model: null,
  isLoading: false
}

const actionHandlers: { [actionType: string]: (state: State, action: Action) => State } = {
  [property.REQUEST_PROPERTY]: (state: State, action: property.RequestPropertyAction): State => {
    return {
      ...state,
      model: null,
      isLoading: true
    }

  },
  [property.RECEIVE_PROPERTY]: (state: State, action: property.ReceivePropertyAction): State => {
    return {
      ...state,
      model: action.property,
      isLoading: false
    }

  },
  [property.FAILURE_PROPERTY]: (state: State): State => {
    return {
      ...state,
      isLoading: false
    }
  }
}

const propertyReducer = initReducer(initialState, actionHandlers);
export default propertyReducer;