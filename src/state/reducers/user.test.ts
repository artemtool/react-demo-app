import userReducer, { initialState } from "./user";
import { testMock } from "../../testObjectsSet";
import { SignInSuccessAction, UpdateUserSuccessAction } from "../actions/user";

describe('user reducer', () => {

  it('should return the initial state', () => {
    expect(userReducer(<any>undefined, {
      type: 'UNKNOWN'
    })).toEqual(initialState)
  });

  it('should handle SIGN_IN', () => {
    const stateAfter = userReducer(<any>undefined, {
      type: 'SIGN_IN'
    });

    expect(stateAfter.isLoading).toEqual(true);
  });

  it('should handle SIGN_UP', () => {
    const stateAfter = userReducer(<any>undefined, {
      type: 'SIGN_UP'
    });

    expect(stateAfter.isLoading).toEqual(true);
  });

  it('should handle SIGN_OUT', () => {
    const stateAfter = userReducer(<any>undefined, {
      type: 'SIGN_OUT'
    });

    expect(stateAfter.isLoading).toEqual(true);
  });

  it('should handle SIGN_IN_SUCCESS', () => {
    const stateBefore = { ...initialState };
    stateBefore.isLoading = true;

    const expectedUser = testMock.getUser(true);
    const action: SignInSuccessAction = {
      type: 'SIGN_IN_SUCCESS',
      user: expectedUser
    };
    const stateAfter = userReducer(stateBefore, action);

    expect(stateAfter.authUser).toEqual(expectedUser);
    expect(stateAfter.isLoading).toEqual(false);
  });

  it('should handle SIGN_UP_SUCCESS', () => {
    const stateBefore = { ...initialState };
    stateBefore.isLoading = true;

    const stateAfter = userReducer(stateBefore, {
      type: 'SIGN_UP_SUCCESS'
    });

    expect(stateAfter.isLoading).toEqual(false);
  });

  it('should handle SIGN_OUT_SUCCESS', () => {
    const stateBefore = { ...initialState };
    stateBefore.authUser = testMock.getUser(true);
    stateBefore.isLoading = true;

    const stateAfter = userReducer(stateBefore, {
      type: 'SIGN_OUT_SUCCESS'
    });

    expect(stateAfter.authUser).toBeNull();
    expect(stateAfter.isLoading).toEqual(false);
  });

  it('should handle SIGN_IN_FAILURE', () => {
    const stateBefore = { ...initialState };
    stateBefore.isLoading = true;

    const stateAfter = userReducer(stateBefore, {
      type: 'SIGN_IN_FAILURE'
    });

    expect(stateAfter.isLoading).toEqual(false);
  });

  it('should handle SIGN_UP_FAILURE', () => {
    const stateBefore = { ...initialState };
    stateBefore.isLoading = true;

    const stateAfter = userReducer(stateBefore, {
      type: 'SIGN_UP_FAILURE'
    });

    expect(stateAfter.isLoading).toEqual(false);
  });

  it('should handle SIGN_OUT_FAILURE', () => {
    const stateBefore = { ...initialState };
    stateBefore.isLoading = true;

    const stateAfter = userReducer(stateBefore, {
      type: 'SIGN_OUT_FAILURE'
    });

    expect(stateAfter.isLoading).toEqual(false);
  });

  it('should handle UPDATE_USER_SUCCESS', () => {
    const stateBefore = { ...initialState };

    const expectedUser = testMock.getUser(true);
    const action: UpdateUserSuccessAction = {
      type: 'UPDATE_USER_SUCCESS',
      user: expectedUser
    };
    const stateAfter = userReducer(stateBefore, action);

    expect(stateAfter.authUser).toEqual(expectedUser);
  });

});