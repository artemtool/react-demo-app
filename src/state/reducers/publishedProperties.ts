import { Action } from 'redux';
import { initReducer } from '../util/reducerInitializer';
import * as publishedProperties from '../actions/publishedProperties';
import { PropertyCard } from '../../models/propertyCard';

export interface State {
  properties: Array<PropertyCard>;
  isLoading: boolean;
  canLoadMore: boolean;
  page: number;
};

export const initialState: State = {
  properties: [],
  isLoading: false,
  canLoadMore: true,
  page: 1
};

const actionHandlers: { [actionType: string]: (state: State, action: Action) => State } = {
  [publishedProperties.REQUEST_PUBLISHED_PROPERTIES]: (state: State, action: publishedProperties.RequestPublishedPropriesAction): State => {
    const properties = action.initialLoad
      ? []
      : state.properties;
    const canLoadMore = action.initialLoad
      ? true
      : state.canLoadMore;
    const page = action.initialLoad
      ? 1
      : state.page;

    return {
      ...state,
      properties,
      canLoadMore,
      page,
      isLoading: true
    }

  },
  [publishedProperties.RECEIVE_PUBLISHED_PROPERTIES]: (state: State, action: publishedProperties.ReceivePublishedPropertiesAction): State => {
    const canLoadMore = action.properties.length > 0;
    return {
      ...state,
      properties: [...state.properties, ...action.properties],
      page: state.page + 1,
      isLoading: false,
      canLoadMore
    }

  },
  [publishedProperties.FAILURE_PUBLISHED_PROPERTIES]: (state: State): State => {
    return {
      ...state,
      isLoading: false
    }
  },
  [publishedProperties.CHANGE_PROPERTY_STATUS_SUCCESS]: (state: State, action: publishedProperties.ChangePropertyStatusSuccessAction): State => {
    const propertyIndex  = state.properties.findIndex(p => p.id === action.propertyId);

    if (propertyIndex < 0) {
      return state;
    } else {
      const property = Object.assign(state.properties[propertyIndex], {});
      property.status = action.propertyStatus;
      state.properties[propertyIndex] = property;

      const properties = [...state.properties];
      return {
        ...state,
        properties
      }
    }
  },
  [publishedProperties.DELETE_PROPERTY_SUCCESS]: (state: State, action: publishedProperties.DeletePropertySuccessAction): State => {
    const index  = state.properties.findIndex(p => p.id === action.id);

    if (index < 0) {
      return state;
    } else {
      state.properties.splice(index, 1);
      return {
        ...state,
        properties: [...state.properties]
      }
    }
  }
};

const publishedPropertiesReducer = initReducer(initialState, actionHandlers);
export default publishedPropertiesReducer;