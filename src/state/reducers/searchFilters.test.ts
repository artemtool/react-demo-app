import searchFiltersReducer, { initialState } from "./searchFilters";
import { testMock } from "../../testObjectsSet";
import { ReceiveSuggestionsAction, UpdateParametersAction } from "../actions/searchFilters";
import { SearchParameters } from "../../interfaces/SearchParameters";

describe('searchFilters reducer', () => {

  it('should return the initial state', () => {
    expect(searchFiltersReducer(<any>undefined, {
      type: 'UNKNOWN'
    })).toEqual(initialState)
  });

  it('should handle REQUEST_SUGGESTIONS', () => {
    const stateAfter = searchFiltersReducer(<any>undefined, {
      type: 'REQUEST_SUGGESTIONS'
    });

    expect(stateAfter.isLoading).toBe(true);
  });

  it('should handle RECEIVE_SUGGESTIONS', () => {
    const stateBefore = { ...initialState };
    stateBefore.isLoading = true;

    const expectedSuggestions = testMock.autocomplete.getSuggestions();
    const action: ReceiveSuggestionsAction = {
      type: 'RECEIVE_SUGGESTIONS',
      suggestions: expectedSuggestions
    }
    const stateAfter = searchFiltersReducer(stateBefore, action);

    expect(stateAfter.suggestions).toBe(expectedSuggestions);
    expect(stateAfter.isLoading).toBe(false);
  });

  it('should handle FAILURE_SUGGESTIONS', () => {
    const stateBefore = { ...initialState };
    stateBefore.isLoading = true;

    const stateAfter = searchFiltersReducer(stateBefore, {
      type: 'FAILURE_SUGGESTIONS'
    });

    expect(stateAfter.isLoading).toBe(false);
  });

  it('should handle UPDATE_PARAMETERS', () => {
    const expectedParameters: SearchParameters = testMock.searchFilters.getModified();
    const action: UpdateParametersAction = {
      type: 'UPDATE_PARAMETERS',
      params: expectedParameters
    };
    const stateAfter = searchFiltersReducer(<any>undefined, action);

    expect(stateAfter.parameters).toBe(expectedParameters);
  });
});