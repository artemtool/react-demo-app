import { Action } from 'redux';
import { initReducer } from '../util/reducerInitializer';
import * as searchFilters from '../actions/searchFilters';
import { SelectGroup } from '../../models/selectGroup';
import { SearchParameters } from './../../interfaces/SearchParameters';

export interface State {
  suggestions: Array<SelectGroup>;
  isLoading: boolean;
  parameters: SearchParameters;
};

const checkInDate = new Date();
checkInDate.setHours(0, 0, 0, 0);
const checkOutDate = new Date(new Date(checkInDate).setDate(checkInDate.getDate() + 1));

export const initialState: State = {
  suggestions: [],
  isLoading: false,
  parameters: {
    cityId: '',
    checkInDate: checkInDate,
    checkOutDate: checkOutDate,
    orderBy: 'PublishDate',
    isDesc: false
  }
};

const actionHandlers: { [actionType: string]: (state: State, action: Action) => State } = {
  [searchFilters.REQUEST_SUGGESTIONS]: (state: State): State => {
    return {
      ...state,
      isLoading: true
    }
  },
  [searchFilters.RECEIVE_SUGGESTIONS]: (state: State, action: searchFilters.ReceiveSuggestionsAction): State => {
    return {
      ...state,
      suggestions: action.suggestions,
      isLoading: false
    }
  },
  [searchFilters.FAILURE_SUGGESTIONS]: (state: State): State => {
    return {
      ...state,
      isLoading: false
    }
  },
  [searchFilters.UPDATE_PARAMETERS]: (state: State, action: searchFilters.UpdateParametersAction): State => {
    return {
      ...state,
      parameters: action.params
    }
  }
};

const searchFiltersReducer = initReducer(initialState, actionHandlers);
export default searchFiltersReducer;