import overlaysReducer, { initialState } from "./overlays";
import { IDialog } from "../../interfaces/dialog";
import { Dialog } from "../../enums/dialog";
import { ShowDialogAction, HideDialogAction, ShowSnackbarAction, HideSnackbarAction } from "../actions/overlays";
import { ISnackbar } from "../../interfaces/snackbar";
import { Snackbar } from "../../enums/snackbar";

describe('overlays reducer', () => {

  it('should return the initial state', () => {
    expect(overlaysReducer(<any>undefined, {
      type: 'UNKNOWN'
    })).toEqual(initialState)
  });

  it('should handle SHOW_DIALOG', () => {
    const stateBefore = { ...initialState };

    const expectedDialog: IDialog = {
      type: Dialog.Booking,
      payload: {
        some: 'data'
      }
    };
    const action: ShowDialogAction = {
      type: 'SHOW_DIALOG',
      dialog: expectedDialog
    };
    const stateAfter = overlaysReducer(stateBefore, action);

    expect(stateAfter.currentDialog).toBe(expectedDialog);
    expect(stateAfter.dialogHistory).toEqual([]);
  });

  it('should handle SHOW_DIALOG when there is already opened dialog', () => {
    const stateBefore = { ...initialState };
    const alreadyOpenedDialog: IDialog = {
      type: Dialog.Booking,
      payload: {
        some: 'data'
      }
    };
    stateBefore.currentDialog = alreadyOpenedDialog;

    const expectedDialog = {
      type: Dialog.UserSettings,
      payload: {
        user: 'someone'
      }
    };
    const action: ShowDialogAction = {
      type: 'SHOW_DIALOG',
      dialog: expectedDialog
    };
    const stateAfter = overlaysReducer(stateBefore, action);

    expect(stateAfter.currentDialog).toBe(expectedDialog);
    expect(stateAfter.dialogHistory).toContain(alreadyOpenedDialog);
  });

  it('should handle HIDE_DIALOG', () => {
    const stateBefore = { ...initialState };
    stateBefore.currentDialog = {
      type: Dialog.Booking,
      payload: {
        some: 'data'
      }
    };
    stateBefore.dialogHistory = [
      {
        type: Dialog.UserSettings,
        payload: 'data',
      }
    ];

    const action: HideDialogAction = {
      type: 'HIDE_DIALOG',
      dialogType: Dialog.Booking
    };
    const stateAfter = overlaysReducer(stateBefore, action);

    expect(stateAfter.currentDialog).toBeNull();
    expect(stateAfter.dialogHistory).toEqual([]);
  });

  it('should handle HIDE_DIALOG with dialog type, that is not actually opened', () => {
    const stateBefore = { ...initialState };
    const currentDialog = {
      type: Dialog.Booking,
      payload: {
        some: 'data'
      }
    };
    stateBefore.currentDialog = currentDialog;
    const previousDialog = {
      type: Dialog.UserSettings,
      payload: 'data',
    };
    stateBefore.dialogHistory = [previousDialog];

    const action: HideDialogAction = {
      type: 'HIDE_DIALOG',
      dialogType: Dialog.UserSettings
    };
    const stateAfter = overlaysReducer(stateBefore, action);

    expect(stateAfter.currentDialog).toBe(currentDialog);
    expect(stateAfter.dialogHistory).toContain(previousDialog);
  });

  it('should handle SHOW_SNACKBAR', () => {
    const stateBefore = { ...initialState };

    const expectedSnackbar: ISnackbar = {
      type: Snackbar.Info,
      message: 'Test message!'
    };
    const action: ShowSnackbarAction = {
      type: 'SHOW_SNACKBAR',
      snackbar: expectedSnackbar
    };
    const stateAfter = overlaysReducer(stateBefore, action);

    expect(stateAfter.currentSnackbar).toBe(expectedSnackbar);
  });

  it('should handle HIDE_SNACKBAR', () => {
    const stateBefore = { ...initialState };
    stateBefore.currentSnackbar = {
      type: Snackbar.Info,
      message: 'Test message!'
    };

    const action: HideSnackbarAction = {
      type: 'HIDE_SNACKBAR',
      snackbarType: Snackbar.Info
    };
    const stateAfter = overlaysReducer(stateBefore, action);

    expect(stateAfter.currentSnackbar).toBeNull;
  });

  it('should handle HIDE_SNACKBAR with snackbar type, that is not actually opened', () => {
    const stateBefore = { ...initialState };
    const currentSnackbar: ISnackbar = {
      type: Snackbar.Warning,
      message: 'Caution! Test message!'
    };
    stateBefore.currentSnackbar = currentSnackbar;

    const action: HideSnackbarAction = {
      type: 'HIDE_SNACKBAR',
      snackbarType: Snackbar.Info
    };
    const stateAfter = overlaysReducer(stateBefore, action);
    
    expect(stateAfter.currentSnackbar).toBe(currentSnackbar);
  });
});