import userBookingsReducer, { initialState } from "./userBookings";
import { testMock } from './../../testObjectsSet';
import { RequestUserBookingsAction, ReceiveUserBookingsAction } from "../actions/userBookings";

describe('userBookings reducer', () => {

  it('should return the initial state', () => {
    expect(userBookingsReducer(<any>undefined, {
      type: 'UNKNOWN'
    })).toEqual(initialState);
  });

  it('should handle REQUEST_USER_BOOKINGS with initialLoad = true', () => {
    const stateBefore = { ...initialState };
    stateBefore.bookings = testMock.booking.getFirstSet(true);
    stateBefore.canLoadMore = false;
    stateBefore.page = 10;

    const action: RequestUserBookingsAction = {
      type: 'REQUEST_USER_BOOKINGS',
      initialLoad: true
    };
    const stateAfter = userBookingsReducer(stateBefore, action);

    expect(stateAfter.bookings).toEqual([]);
    expect(stateAfter.canLoadMore).toBe(true);
    expect(stateAfter.page).toBe(1);
    expect(stateAfter.isLoading).toEqual(true);
  });

  it('should handle REQUEST_USER_BOOKINGS with initialLoad = false', () => {
    const stateBefore = { ...initialState };
    const expectedBookings = testMock.booking.getFirstSet(true);
    const expectedPage = 10;
    stateBefore.bookings = expectedBookings;
    stateBefore.page = expectedPage;

    const action: RequestUserBookingsAction = {
      type: 'REQUEST_USER_BOOKINGS',
      initialLoad: false
    };
    const stateAfter = userBookingsReducer(stateBefore, action);

    expect(stateAfter.bookings).toEqual(expectedBookings);
    expect(stateAfter.page).toBe(expectedPage);
    expect(stateAfter.isLoading).toEqual(true);
  });

  it('should handle RECEIVE_USER_BOOKINGS', () => {
    const stateBefore = { ...initialState };
    const bookings  = testMock.booking.getFirstSet(true);
    const newBookings = testMock.booking.getSecondSet(true);
    stateBefore.isLoading = true;
    stateBefore.bookings = bookings

    const action: ReceiveUserBookingsAction = {
      type: 'RECEIVE_USER_BOOKINGS',
      bookings: newBookings
    };
    const stateAfter = userBookingsReducer(stateBefore, action);

    expect(stateAfter.bookings).toEqual([...bookings, ...newBookings]);
    expect(stateAfter.page).toEqual(stateBefore.page + 1);
    expect(stateAfter.isLoading).toEqual(false);
  });

  it('should handle FAILURE_USER_BOOKINGS', () => {
    const stateBefore = { ...initialState };
    stateBefore.isLoading = true;

    const stateAfter = userBookingsReducer(stateBefore, {
      type: 'FAILURE_USER_BOOKINGS'
    });

    expect(stateAfter.isLoading).toEqual(false);
  });
});