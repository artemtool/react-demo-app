import propertyReducer, { initialState } from "./property";
import { ReceivePropertyAction } from "../actions/property";
import { testMock } from "../../testObjectsSet";

describe('property reducer', () => {

  it('should return the initial state', () => {
    expect(propertyReducer(<any>undefined, {
      type: 'UNKNOWN'
    })).toEqual(initialState)
  });

  it('should handle REQUEST_PROPERTY', () => {
    const stateBefore = { ...initialState };
    stateBefore.model = testMock.property.getFirstSet[0];

    const stateAfter = propertyReducer(stateBefore, {
      type: 'REQUEST_PROPERTY'
    });

    expect(stateAfter.isLoading).toBe(true);
    expect(stateAfter.model).toBeNull();
  });

  it('should handle RECEIVE_PROPERTY', () => {
    const stateBefore = { ...initialState };
    stateBefore.isLoading = true;

    const expectedProperty = testMock.property.getFirstSet(true)[0];
    const action: ReceivePropertyAction = {
      type: 'RECEIVE_PROPERTY',
      property: expectedProperty
    };
    const stateAfter = propertyReducer(stateBefore, action);
    
    expect(stateAfter.model).toBe(expectedProperty);
    expect(stateAfter.isLoading).toBe(false);
  });

  it('should handle FAILURE_PROPERTY', () => {
    const stateBefore = { ...initialState };
    stateBefore.isLoading = true;
    
    const stateAfter = propertyReducer(stateBefore, {
      type: 'FAILURE_PROPERTY'
    });
    
    expect(stateAfter.isLoading).toBe(false);
  });
});