import searchResultsReducer, { initialState } from "./searchResults";
import { testMock } from "../../testObjectsSet";
import { RequestPropriesAction, ReceivePropertiesAction } from "../actions/searchResults";
import { BookPropertySuccessAction } from "../actions/userBookings";

describe('searchFilters reducer', () => {

  it('should return the initial state', () => {
    expect(searchResultsReducer(<any>undefined, {
      type: 'UNKNOWN'
    })).toEqual(initialState)
  });

  it('should handle REQUEST_PROPERTIES with initialLoad = true', () => {
    const stateBefore = { ...initialState };
    stateBefore.properties = testMock.propertyCard.getFirstSet(true);
    stateBefore.canLoadMore = false;
    stateBefore.page = 10;

    const action: RequestPropriesAction = {
      type: 'REQUEST_PROPERTIES',
      initialLoad: true
    };
    const stateAfter = searchResultsReducer(stateBefore, action);

    expect(stateAfter.properties).toEqual([]);
    expect(stateAfter.canLoadMore).toBe(true);
    expect(stateAfter.page).toBe(1);
    expect(stateAfter.isLoading).toEqual(true);
  });

  it('should handle REQUEST_PROPERTIES with initialLoad = false', () => {
    const stateBefore = { ...initialState };
    const expectedProperties = testMock.propertyCard.getFirstSet(true);
    const expectedPage = 10;
    stateBefore.properties = expectedProperties;
    stateBefore.page = expectedPage;

    const action: RequestPropriesAction = {
      type: 'REQUEST_PROPERTIES',
      initialLoad: false
    };
    const stateAfter = searchResultsReducer(stateBefore, action);

    expect(stateAfter.properties).toEqual(expectedProperties);
    expect(stateAfter.page).toBe(expectedPage);
    expect(stateAfter.isLoading).toEqual(true);
  });

  it('should handle RECEIVE_PROPERTIES', () => {
    const stateBefore = { ...initialState };
    const properties = testMock.propertyCard.getFirstSet(true);
    const newProperties = testMock.propertyCard.getSecondSet(true);
    stateBefore.isLoading = true;
    stateBefore.properties = properties;

    const action: ReceivePropertiesAction = {
      type: 'RECEIVE_PROPERTIES',
      properties: newProperties
    };
    const stateAfter = searchResultsReducer(stateBefore, action);

    expect(stateAfter.properties).toEqual([...properties, ...newProperties]);
    expect(stateAfter.page).toEqual(stateBefore.page + 1);
    expect(stateAfter.isLoading).toEqual(false);
  });

  it('should handle FAILURE_PROPERTIES', () => {
    const stateBefore = { ...initialState };
    stateBefore.isLoading = true;

    const stateAfter = searchResultsReducer(stateBefore, {
      type: 'FAILURE_PROPERTIES'
    });

    expect(stateAfter.isLoading).toEqual(false);
  });

  it('should handle BOOK_PROPERTY_SUCCESS', () => {
    const stateBefore = { ...initialState };
    stateBefore.isLoading = true;
    stateBefore.properties = testMock.propertyCard.getFirstSet(true);

    const expectedBookingId = stateBefore.properties[0].id;
    const expectedBooking = testMock.booking.getFirstSet(true)[0];
    expectedBooking.id = expectedBookingId;
    const action: BookPropertySuccessAction = {
      type: 'BOOK_PROPERTY_SUCCESS',
      booking: expectedBooking
    }
    const stateAfter = searchResultsReducer(stateBefore, action);

    expect(stateBefore.properties.filter(p => p.id !== expectedBookingId))
      .toEqual(stateAfter.properties);
  });

  it('should handle BOOK_PROPERTY_SUCCESS, with property id that is actually is not exist in properties array', () => {
    const stateBefore = { ...initialState };
    stateBefore.isLoading = true;
    stateBefore.properties = testMock.propertyCard.getFirstSet(true);

    const expectedBooking = testMock.booking.getFirstSet(true)[0];
    expectedBooking.id = 'non-existent id';
    const action: BookPropertySuccessAction = {
      type: 'BOOK_PROPERTY_SUCCESS',
      booking: expectedBooking
    }
    const stateAfter = searchResultsReducer(stateBefore, action);

    expect(stateBefore).toEqual(stateAfter);
  });

});