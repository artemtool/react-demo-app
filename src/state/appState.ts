import { State as SearchResultsState } from "./reducers/searchResults";
import { State as SearchFiltersState } from "./reducers/searchFilters";
import { State as UserState } from "./reducers/user";
import { State as OverlaysState } from "./reducers/overlays";
import { State as UserBookingsState } from "./reducers/userBookings";
import { State as IncomingBookingsState } from "./reducers/incomingBookings";
import { State as PublishedPropertiesState } from "./reducers/publishedProperties";
import { State as PropertyState } from "./reducers/property";
import { State as MessangerState } from "./reducers/messanger";
import { Action } from 'redux';

export interface AppState {
  searchResults: SearchResultsState;
  searchFilters: SearchFiltersState;
  user: UserState;
  overlays: OverlaysState;
  userBookings: UserBookingsState;
  incomingBookings: IncomingBookingsState;
  publishedProperties: PublishedPropertiesState;
  property: PropertyState;
  messanger: MessangerState;
};

export interface FailureAction extends Action {
  error: any;
} 