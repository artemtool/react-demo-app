import { takeEvery, put, select, call, race } from 'redux-saga/effects'
import { SIGN_IN_SUCCESS, SIGN_OUT_SUCCESS } from '../actions/user';
import signalR from '../../common/signalR';
import { IMessage } from './../../interfaces/message';
import { pushMessage } from '../actions/messanger';
import { Message } from '../../models/Message';
import { AppState } from '../appState';
import { OnConnectedUpdates } from './../../interfaces/onConnectedUpdates';
import { receiveNewPropertyBookings, markNewPropertyBookingsAsSeen } from '../actions/incomingBookings';
import { PropertyBooking } from './../../models/propertyBooking';
import { receiveNewBooking } from './../actions/incomingBookings';
import { IPropertyBooking } from './../../interfaces/propertyBooking';

function* markNewBookingsAsSeen() {
  const propertyId = yield new Promise(resolve => {
    signalR.connection.on("MarkNewPropertyBookingsAsSeen", id => resolve(id));
  });
  yield put(markNewPropertyBookingsAsSeen(propertyId));
}

function* receiveBooking() {
  const booking: PropertyBooking = yield new Promise(resolve => {
    signalR.connection.on("ReceiveBooking", serializedBooking => {
      const booking: IPropertyBooking = JSON.parse(serializedBooking);
      resolve(new PropertyBooking(booking));
    });
  });
  yield put(receiveNewBooking(booking));
}

function* receiveBookingMessage() {
  const message: IMessage = yield new Promise(resolve => {
    signalR.connection.on("ReceiveMessage", message => resolve(message));
  });

  const state: AppState = yield select();
  const messangerState = state.messanger.states.find(s => s.connectedEntityId === message.propertyBookingId);
  if (messangerState) {
    const pushedByCurrentUser = !!messangerState.messages.find(m => m.id === message.id);
    if (!pushedByCurrentUser) {
      yield put(pushMessage(new Message(message, false)));
    }
  }
}

function* receiveOnConnectedUpdates() {
  const updates: OnConnectedUpdates = yield new Promise(resolve => {
    signalR.connection.on("ReceiveOnConnectedUpdates", message => resolve(message));
  });

  yield put(receiveNewPropertyBookings(updates.newPropertyBookings));

  signalR.connection.off("ReceiveOnConnectedUpdates");
}

function* connectSignalR() {
  signalR.connection.start();
  yield call(receiveOnConnectedUpdates);
  while (true) {
    yield race({
      receiveBookingMessage: call(receiveBookingMessage),
      receiveBooking: call(receiveBooking),
      markNewBookingsAsSeen: call(markNewBookingsAsSeen)
    });
  }
}

function* disconnectSignalR() {
  signalR.connection.stop();
}

function* listenForAuthActions() {
  yield takeEvery(SIGN_IN_SUCCESS, connectSignalR);
  yield takeEvery(SIGN_OUT_SUCCESS, disconnectSignalR);
}

export default listenForAuthActions;