import { FailureAction } from "../appState";
import { AppState } from "../appState";
import { PropertyBooking } from './../../models/propertyBooking';
import { Dispatch, Action } from 'redux';
import { http } from './../../common/http';
import { PageableListParameters } from './../../interfaces/pageableListParameters';
import { BookPropertyRequest } from './../../interfaces/dto/bookPropertyRequest';

export const BOOK_PROPERTY = 'BOOK_PROPERTY';
export const BOOK_PROPERTY_SUCCESS = 'BOOK_PROPERTY_SUCCESS';
export const BOOK_PROPERTY_FAILURE = 'BOOK_PROPERTY_FAILURE';
export const REQUEST_USER_BOOKINGS = 'REQUEST_USER_BOOKINGS';
export const RECEIVE_USER_BOOKINGS = 'RECEIVE_USER_BOOKINGS';
export const FAILURE_USER_BOOKINGS = 'FAILURE_USER_BOOKINGS';

export interface RequestUserBookingsAction extends Action {
  initialLoad: boolean;
} 

export interface BookPropertySuccessAction extends Action {
  booking: PropertyBooking;
} 

export interface ReceiveUserBookingsAction extends Action {
  bookings: Array<PropertyBooking>;
} 

export function startBookProperty(): Action {
  return {
    type: BOOK_PROPERTY
  };
}

export function bookPropertySuccess(booking: PropertyBooking): BookPropertySuccessAction {
  return {
    type: BOOK_PROPERTY_SUCCESS,
    booking
  };
}

export function bookPropertyFailure(error: any): FailureAction {
  return {
    type: BOOK_PROPERTY_FAILURE,
    error
  };
}

export function bookProperty(booking: BookPropertyRequest) {
  return (dispatch: Dispatch) => {
    dispatch(startBookProperty());

    const headers = {
      'Content-Type': 'application/json'
    };

    return http.stickAuthHeaderIfExists(headers)
      .then(headers => {
        return fetch(`${process.env.REACT_APP_CORE_API_URL}/booking/add`, {
          method: 'POST',
          body: JSON.stringify(booking),
          headers
        })
      })
      .then(response => response.json())
      .then(booking => dispatch(bookPropertySuccess(new PropertyBooking(booking))))
      .catch(error => dispatch(bookPropertyFailure(error)));
  }
}

export function requestUserBookings(initialLoad: boolean): RequestUserBookingsAction {
  return {
    type: REQUEST_USER_BOOKINGS,
    initialLoad
  };
}

export function receiveUserBookings(bookings: Array<PropertyBooking>): ReceiveUserBookingsAction {
  return {
    type: RECEIVE_USER_BOOKINGS,
    bookings
  };
}

export function failureUserBookings(error: any): FailureAction {
  return {
    type: FAILURE_USER_BOOKINGS,
    error
  };
}

function allowedToFetchBookings(state: AppState): boolean {
  return !state.userBookings.isLoading
    && state.userBookings.canLoadMore;
}

export function fetchUserBookings(initialLoad: boolean = false) {
  return (dispatch: Dispatch, getState: () => AppState) => {
    const state = getState();
    if (allowedToFetchBookings(state) || initialLoad) {
      dispatch(requestUserBookings(initialLoad));

      const parameters: PageableListParameters = {
        page: initialLoad
          ? 1
          : state.userBookings.page,
        perPageAmount: 25,
        isDesc: false,
        orderBy: 'CheckInDate'
      };

      const headers = {
        'Content-Type': 'application/json'
      }

      return http.stickAuthHeader(headers)
        .then(headers => {
          return fetch(`${process.env.REACT_APP_CORE_API_URL}/booking/list/my`, {
            method: "POST",
            body: JSON.stringify(parameters),
            headers
          })
        })
        .then(response => response.json())
        .then(bookings => {
          const mappedModels = bookings.map(b => new PropertyBooking(b));
          dispatch(receiveUserBookings(mappedModels));
        })
        .catch(error => dispatch(failureUserBookings(error)));
    } else {
      return Promise.resolve();
    }
  }
}