import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as fetchMock from 'fetch-mock'
import { testMock } from './../../testObjectsSet';
import { FailureAction } from '../appState';
import { RequestPropertyAction, requestProperty, ReceivePropertyAction, receiveProperty, failureProperty, fetchProperty } from './property';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('incomingBookings actions', () => {
  afterEach(() => {
    fetchMock.reset();
    fetchMock.restore();
  });

  it('should create REQUEST_PROPERTY action', () => {
    const propertyId = 'test-id';
    const expectedAction: RequestPropertyAction = {
      type: 'REQUEST_PROPERTY',
      id: propertyId
    };

    expect(requestProperty(propertyId))
      .toEqual(expectedAction);
  });

  it('should create RECEIVE_PROPERTY action', () => {
    const property = testMock.property.getFirstSet(true)[0];
    const expectedAction: ReceivePropertyAction = {
      type: 'RECEIVE_PROPERTY',
      property
    }

    expect(receiveProperty(property))
      .toEqual(expectedAction);
  });

  it('should create FAILURE_PROPERTY action', () => {
    const error = new Error('Error for testing purporses');
    const expectedAction: FailureAction = {
      type: 'FAILURE_PROPERTY',
      error
    }

    expect(failureProperty(error))
      .toEqual(expectedAction);
  });

  it('should create [REQUEST_PROPERTY, RECEIVE_PROPERTY] async actions', () => {
    const property = testMock.property.getFirstSet(true)[0];
    const requestAction: RequestPropertyAction = {
      type: 'REQUEST_PROPERTY',
      id: property.id
    };
    const receiveAction: ReceivePropertyAction = {
      type: 'RECEIVE_PROPERTY',
      property: property
    };

    const store = mockStore();
    fetchMock
      .getOnce(`${process.env.REACT_APP_CORE_API_URL}/property/${property.id}`, {
        body: testMock.property.getFirstSet(false)[0]
      });

    return store.dispatch(fetchProperty(property.id)).then(() => {
      expect(store.getActions()).toEqual([requestAction, receiveAction]);
    });
  });
});