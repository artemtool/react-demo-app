import { Dispatch, Action } from "redux";
import { FailureAction } from "../appState";
import { Property } from "../../models/property";

export const REQUEST_PROPERTY = 'REQUEST_PROPERTY';
export const RECEIVE_PROPERTY = 'RECEIVE_PROPERTY';
export const FAILURE_PROPERTY = 'FAILURE_PROPERTY';

export interface RequestPropertyAction extends Action {
  id: string;
} 

export interface ReceivePropertyAction extends Action {
  property: Property;
} 

export function requestProperty(id: string): RequestPropertyAction {
  return {
    type: REQUEST_PROPERTY,
    id
  };
}

export function receiveProperty(property: Property): ReceivePropertyAction {
  return {
    type: RECEIVE_PROPERTY,
    property
  };
}

export function failureProperty(error): FailureAction {
  return {
    type: FAILURE_PROPERTY,
    error
  };
}

export function fetchProperty(id: string) {
  return (dispatch: Dispatch) => {
    dispatch(requestProperty(id));

    return fetch(`${process.env.REACT_APP_CORE_API_URL}/property/${id}`, {
      method: "GET",
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(response => response.json())
      .then(result => {
        return dispatch(receiveProperty(new Property(result)));
      })
      .catch(error => dispatch(failureProperty(error)));
  }
}