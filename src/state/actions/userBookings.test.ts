import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as fetchMock from 'fetch-mock'
import { testMock } from './../../testObjectsSet';
import { FailureAction } from '../appState';
import { http } from './../../common/http';
import { RequestUserBookingsAction, requestUserBookings, ReceiveUserBookingsAction, receiveUserBookings, failureUserBookings, fetchUserBookings, startBookProperty, BookPropertySuccessAction, bookPropertySuccess, bookPropertyFailure, bookProperty } from './userBookings';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('userBookings actions', () => {
  afterEach(() => {
    fetchMock.reset();
    fetchMock.restore();
  });

  it('should create BOOK_PROPERTY action', () => {
    const expectedAction = {
      type: 'BOOK_PROPERTY'
    }

    expect(startBookProperty())
      .toEqual(expectedAction);
  });

  it('should create BOOK_PROPERTY_SUCCESS action', () => {
    const expectedBooking = testMock.booking.getFirstSet(true)[0];
    const expectedAction: BookPropertySuccessAction = {
      type: 'BOOK_PROPERTY_SUCCESS',
      booking: expectedBooking
    }

    expect(bookPropertySuccess(expectedBooking))
      .toEqual(expectedAction);
  });

  it('should create BOOK_PROPERTY_FAILURE action', () => {
    const error = new Error('Error for testing purporses');
    const expectedAction: FailureAction = {
      type: 'BOOK_PROPERTY_FAILURE',
      error
    }

    expect(bookPropertyFailure(error))
      .toEqual(expectedAction);
  });

  it('should create [BOOK_PROPERTY, BOOK_PROPERTY_SUCCESS] async actions', () => {
    const bookAction = {
      type: 'BOOK_PROPERTY'
    };
    const expectedBooking = testMock.booking.getFirstSet(true)[0];
    const bookSuccessAction: BookPropertySuccessAction = {
      type: 'BOOK_PROPERTY_SUCCESS',
      booking: expectedBooking
    };

    const store = mockStore();
    fetchMock
      .postOnce(`${process.env.REACT_APP_CORE_API_URL}/booking/add`, {
        body: testMock.booking.getFirstSet(false)[0]
      });

    return store.dispatch(bookProperty(expectedBooking))
      .then(() => {
        expect(store.getActions()).toEqual([bookAction, bookSuccessAction]);
      });
  });

  it('should create [BOOK_PROPERTY, BOOK_PROPERTY_FAILURE] async action and remove access and refresh tokens from localStorage', () => {
    const bookAction = {
      type: 'BOOK_PROPERTY'
    };
    const error = new Error('Error for testing purporses');
    const bookFailureAction: FailureAction = {
      type: 'BOOK_PROPERTY_FAILURE',
      error
    };

    const store = mockStore();
    fetchMock
      .postOnce(`${process.env.REACT_APP_CORE_API_URL}/booking/add`, () => {
        throw error;
      });

    return store.dispatch(bookProperty(testMock.booking.getFirstSet[0]))
      .then(() => {
        expect(store.getActions()).toEqual([bookAction, bookFailureAction]);
      });
  });


  it('should create REQUEST_USER_BOOKINGS action', () => {
    const expectedAction: RequestUserBookingsAction = {
      type: 'REQUEST_USER_BOOKINGS',
      initialLoad: true
    }

    expect(requestUserBookings(true))
      .toEqual(expectedAction);
  });

  it('should create RECEIVE_USER_BOOKINGS action', () => {
    const expectedBookings = testMock.booking.getFirstSet(true);
    const expectedAction: ReceiveUserBookingsAction = {
      type: 'RECEIVE_USER_BOOKINGS',
      bookings: expectedBookings
    }

    expect(receiveUserBookings(expectedBookings))
      .toEqual(expectedAction);
  });

  it('should create FAILURE_USER_BOOKINGS action', () => {
    const error = new Error('Error for testing purporses');
    const expectedAction: FailureAction = {
      type: 'FAILURE_USER_BOOKINGS',
      error
    }

    expect(failureUserBookings(error))
      .toEqual(expectedAction);
  });

  it('should create [REQUEST_USER_BOOKINGS, RECEIVE_USER_BOOKINGS] async actions', () => {
    const isInitialLoad = true;
    const requestAction: RequestUserBookingsAction = {
      type: 'REQUEST_USER_BOOKINGS',
      initialLoad: isInitialLoad
    };
    const receiveAction: ReceiveUserBookingsAction = {
      type: 'RECEIVE_USER_BOOKINGS',
      bookings: testMock.booking.getFirstSet(true)
    };
    const store = mockStore({
      userBookings: {
        canLoadMore: true
      }
    });
    http.stickAuthHeader = jest.fn()
      .mockResolvedValueOnce({});
    fetchMock
      .postOnce(`${process.env.REACT_APP_CORE_API_URL}/booking/list/my`, {
        body: testMock.booking.getFirstSet(false)
      });

    return store.dispatch(fetchUserBookings(isInitialLoad)).then(() => {
      expect(store.getActions()).toEqual([requestAction, receiveAction]);
    });
  });

  it('should do nothing on fetchUserBookings()', () => {
    const store = mockStore({
      userBookings: {
        isLoading: true
      }
    });

    store.dispatch(fetchUserBookings(false)).then(() => {
      expect(store.getActions()).toEqual([]);
    });
  });

  it('should do nothing on fetchUserBookings()', () => {
    const store = mockStore({
      userBookings: {
        canLoadMore: false
      }
    });

    store.dispatch(fetchUserBookings(false)).then(() => {
      expect(store.getActions()).toEqual([]);
    });
  });
});