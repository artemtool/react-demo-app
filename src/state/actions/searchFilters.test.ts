import { requestSuggestions, ReceiveSuggestionsAction, receiveSuggestions, failureSuggestions, fetchSuggestionsIfNeeded, UpdateParametersAction, updateParameters } from './searchFilters';
import { testMock } from '../../testObjectsSet';
import { FailureAction } from '../appState';
import thunk from 'redux-thunk';
import * as fetchMock from 'fetch-mock'
import configureMockStore from 'redux-mock-store';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const autocompleteResponseSuggestions = [
  {
    name: 'Vinnytsia region',
    cities: [
      {
        name: 'Vinnytsia',
        id: '4cb807f7-7fee-4eb0-c460-08d617c08831'
      },
      {
        name: 'Hmilnyk',
        id: '7cb807f7-7fee-4eb0-c460-08d617c08813'
      }
    ]
  },
  {
    name: 'Kyiv region',
    cities: [
      {
        name: 'Kyiv',
        id: '2ac807f7-7fee-4eb0-c460-08d617c08642'
      },
      {
        name: 'Bila Cerkva',
        id: '1aa807f7-7fee-4eb0-c460-08d617c08824'
      }
    ]
  }
];

describe('searchFilters actions', () => {
  it('should create REQUEST_SUGGESTIONS action', () => {
    const expectedAction = {
      type: 'REQUEST_SUGGESTIONS'
    };

    expect(requestSuggestions())
      .toEqual(expectedAction);
  });

  it('should create RECEIVE_SUGGESTIONS action', () => {
    const suggestions = testMock.autocomplete.getSuggestions();
    const expectedAction: ReceiveSuggestionsAction = {
      type: 'RECEIVE_SUGGESTIONS',
      suggestions
    };

    expect(receiveSuggestions(suggestions))
      .toEqual(expectedAction);
  });

  it('should create FAILURE_SUGGESTIONS action', () => {
    const error = new Error('Test error!');
    const expectedAction: FailureAction = {
      type: 'FAILURE_SUGGESTIONS',
      error
    };

    expect(failureSuggestions(error))
      .toEqual(expectedAction);
  });

  it('should create [REQUEST_SUGGESTIONS, RECEIVE_SUGGESTIONS] async actions', () => {
    const requestAction = {
      type: 'REQUEST_SUGGESTIONS'
    };
    const receiveAction: ReceiveSuggestionsAction = {
      type: 'RECEIVE_SUGGESTIONS',
      suggestions: testMock.autocomplete.getSuggestions()
    };
    const store = mockStore({
      searchFilters: {
        suggestions: []
      }
    });
    fetchMock
      .getOnce(`${process.env.REACT_APP_CORE_API_URL}/autocomplete/regions`, {
        body: autocompleteResponseSuggestions
      });

    return store.dispatch(fetchSuggestionsIfNeeded()).then(() => {
      expect(store.getActions()).toEqual([requestAction, receiveAction]);
    });
  });

  it('should do nothing on fetchSuggestionsIfNeeded()', () => {
    const store = mockStore({
      searchFilters: {
        isLoading: true,
        suggestions: []
      }
    });

    store.dispatch(fetchSuggestionsIfNeeded()).then(() => {
      expect(store.getActions()).toEqual([]);
    });
  });

  it('should do nothing on fetchSuggestionsIfNeeded()', () => {
    const store = mockStore({
      searchFilters: {
        suggestions: [{
          label: 'Vin region',
          options: []
        }]
      }
    });

    store.dispatch(fetchSuggestionsIfNeeded()).then(() => {
      expect(store.getActions()).toEqual([]);
    });
  });

  it('should create UPDATE_PARAMETERS action', () => {
    const params = testMock.searchFilters.getModified();
    const expectedAction: UpdateParametersAction = {
      type: 'UPDATE_PARAMETERS',
      params
    };

    expect(updateParameters(params))
      .toEqual(expectedAction);
  });
});

