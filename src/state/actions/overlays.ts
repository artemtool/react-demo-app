import { IDialog } from './../../interfaces/dialog';
import { Dialog } from "../../enums/dialog";
import { ISnackbar } from './../../interfaces/snackbar';
import { Snackbar } from "../../enums/snackbar";
import { Action } from 'redux';

export const SHOW_DIALOG = 'SHOW_DIALOG';
export const HIDE_DIALOG = 'HIDE_DIALOG';
export const SHOW_SNACKBAR = 'SHOW_SNACKBAR';
export const HIDE_SNACKBAR = 'HIDE_SNACKBAR';

export interface ShowDialogAction extends Action {
  dialog: IDialog;
} 

export interface HideDialogAction extends Action {
  dialogType: Dialog;
} 

export interface ShowSnackbarAction extends Action {
  snackbar: ISnackbar;
} 

export interface HideSnackbarAction extends Action {
  snackbarType: Snackbar;
} 

export function showDialog(dialog: IDialog): ShowDialogAction {
  return {
    type: SHOW_DIALOG,
    dialog
  };
}

export function hideDialog(dialogType: Dialog): HideDialogAction {
  return {
    type: HIDE_DIALOG,
    dialogType
  };
}

export function showSnackbar(snackbar: ISnackbar): ShowSnackbarAction {
  return {
    type: SHOW_SNACKBAR,
    snackbar
  };
}

export function hideSnackbar(snackbarType: Snackbar): HideSnackbarAction {
  return {
    type: HIDE_SNACKBAR,
    snackbarType
  };
}