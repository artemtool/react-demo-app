import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as fetchMock from 'fetch-mock'
import { SetSelectedPropertyAction, setSelectedProperty, RequestIncomingBookingsAction, ReceiveIncomingBookingsAction, receiveIncomingBookings, fetchIncomingBookings, requestPropertySuggestions, ReceivePropertySuggestionsAction, receivePropertySuggestions, failurePropertySuggestions, fetchPropertySuggestions, fetchSelectedProperty } from './incomingBookings';
import { testMock } from './../../testObjectsSet';
import { requestIncomingBookings } from './incomingBookings';
import { FailureAction } from '../appState';
import { failureIncomingBookings } from './incomingBookings';
import { http } from './../../common/http';
import { PropertyCard } from '../../models/propertyCard';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('incomingBookings actions', () => {
  afterEach(() => {
    fetchMock.reset();
    fetchMock.restore();
  });

  it('should create SET_SELECTED_PROPERTY action', () => {
    const expectedProperty = testMock.property.getFirstSet(true)[0];
    const expectedAction: SetSelectedPropertyAction = {
      type: 'SET_SELECTED_PROPERTY',
      property: expectedProperty
    }

    expect(setSelectedProperty(expectedProperty))
      .toEqual(expectedAction);
  });

  it('should create REQUEST_INCOMING_BOOKINGS action', () => {
    const expectedAction: RequestIncomingBookingsAction = {
      type: 'REQUEST_INCOMING_BOOKINGS',
      initialLoad: true
    }

    expect(requestIncomingBookings(true))
      .toEqual(expectedAction);
  });

  it('should create RECEIVE_INCOMING_BOOKINGS action', () => {
    const expectedBookings = testMock.booking.getFirstSet(true);
    const expectedAction: ReceiveIncomingBookingsAction = {
      type: 'RECEIVE_INCOMING_BOOKINGS',
      bookings: expectedBookings
    }

    expect(receiveIncomingBookings(expectedBookings))
      .toEqual(expectedAction);
  });

  it('should create FAILURE_INCOMING_BOOKINGS action', () => {
    const error = new Error('Error for testing purporses');
    const expectedAction: FailureAction = {
      type: 'FAILURE_INCOMING_BOOKINGS',
      error
    }

    expect(failureIncomingBookings(error))
      .toEqual(expectedAction);
  });

  it('should create [REQUEST_INCOMING_BOOKINGS, RECEIVE_INCOMING_BOOKINGS] async actions', () => {
    const isInitialLoad = true;
    const requestAction: RequestIncomingBookingsAction = {
      type: 'REQUEST_INCOMING_BOOKINGS',
      initialLoad: isInitialLoad
    };
    const receiveAction: ReceiveIncomingBookingsAction = {
      type: 'RECEIVE_INCOMING_BOOKINGS',
      bookings: testMock.booking.getFirstSet(true)
    };
    const store = mockStore({
      incomingBookings: {
        canLoadMore: true
      }
    });
    http.stickAuthHeader = jest.fn()
      .mockResolvedValueOnce({});
    fetchMock
      .postOnce(`${process.env.REACT_APP_CORE_API_URL}/booking/list/incoming`, {
        body: testMock.booking.getFirstSet(false)
      });

    return store.dispatch(fetchIncomingBookings('any id', isInitialLoad)).then(() => {
      expect(store.getActions()).toEqual([requestAction, receiveAction]);
    });
  });

  it('should do nothing on fetchIncomingBookings()', () => {
    const store = mockStore({
      incomingBookings: {
        isLoading: true
      }
    });

    store.dispatch(fetchIncomingBookings('any id')).then(() => {
      expect(store.getActions()).toEqual([]);
    });
  });

  it('should do nothing on fetchIncomingBookings()', () => {
    const store = mockStore({
      incomingBookings: {
        canLoadMore: false
      }
    });

    store.dispatch(fetchIncomingBookings('any id')).then(() => {
      expect(store.getActions()).toEqual([]);
    });
  });

  it('should create REQUEST_PROPERTY_SUGGESTIONS action', () => {
    const expectedAction = {
      type: 'REQUEST_PROPERTY_SUGGESTIONS'
    };

    expect(requestPropertySuggestions())
      .toEqual(expectedAction);
  });

  it('should create RECEIVE_PROPERTY_SUGGESTIONS action', () => {
    const expectedSuggestions = testMock.propertyCard.getFirstSet(true);
    const expectedAction: ReceivePropertySuggestionsAction = {
      type: 'RECEIVE_PROPERTY_SUGGESTIONS',
      suggestions: expectedSuggestions
    };

    expect(receivePropertySuggestions(expectedSuggestions))
      .toEqual(expectedAction);
  });

  it('should create FAILURE_PROPERTY_SUGGESTIONS action', () => {
    const error = new Error('Error for testing purporses');
    const expectedAction: FailureAction = {
      type: 'FAILURE_PROPERTY_SUGGESTIONS',
      error
    }

    expect(failurePropertySuggestions(error))
      .toEqual(expectedAction);
  });

  it('should create [REQUEST_PROPERTY_SUGGESTIONS, RECEIVE_PROPERTY_SUGGESTIONS] async actions', () => {
    const address = 'test-address';
    const requestAction = {
      type: 'REQUEST_PROPERTY_SUGGESTIONS'
    };
    const receiveAction: ReceivePropertySuggestionsAction = {
      type: 'RECEIVE_PROPERTY_SUGGESTIONS',
      suggestions: testMock.propertyCard.getFirstSet(true)
    };
    const store = mockStore({
      incomingBookings: {}
    });
    http.stickAuthHeader = jest.fn()
      .mockResolvedValueOnce({});
    fetchMock
      .getOnce(`${process.env.REACT_APP_CORE_API_URL}/property/list/published/suggestions/${address}`, {
        body: testMock.propertyCard.getFirstSet(false)
      });

    return store.dispatch(fetchPropertySuggestions(address)).then(() => {
      expect(store.getActions()).toEqual([requestAction, receiveAction]);
    });
  });

  it('should create SET_SELECTED_PROPERTY async action', () => {
    const propertyId = 'test-id';
    const expectedAction: SetSelectedPropertyAction = {
      type: 'SET_SELECTED_PROPERTY',
      property: new PropertyCard(testMock.propertyCard.getFirstSet(true)[0])
    };
    const store = mockStore({
      incomingBookings: {
        suggestions: []
      }
    });
    fetchMock
      .getOnce(`${process.env.REACT_APP_CORE_API_URL}/property/card/${propertyId}`, {
        body: testMock.propertyCard.getFirstSet(false)[0]
      });

    return store.dispatch(fetchSelectedProperty(propertyId)).then(() => {
      expect(store.getActions()).toEqual([expectedAction]);
    });
  });

  it('should create SET_SELECTED_PROPERTY async action without making API call', () => {
    const expectedProperty = testMock.propertyCard.getFirstSet(true)[0];
    const expectedAction: SetSelectedPropertyAction = {
      type: 'SET_SELECTED_PROPERTY',
      property: expectedProperty
    };
    const store = mockStore({
      incomingBookings: {
        suggestions: testMock.propertyCard.getFirstSet(true)
      }
    });

    return store.dispatch(fetchSelectedProperty(expectedProperty.id)).then(() => {
      expect(store.getActions()).toEqual([expectedAction]);
    });
  });

  it('should create SET_SELECTED_PROPERTY async action with NULL payload', () => {
    const expectedAction: SetSelectedPropertyAction = {
      type: 'SET_SELECTED_PROPERTY',
      property: null
    };
    const store = mockStore({
      incomingBookings: { }
    });

    return store.dispatch(fetchSelectedProperty(null)).then(() => {
      expect(store.getActions()).toEqual([expectedAction]);
    });
  });
});