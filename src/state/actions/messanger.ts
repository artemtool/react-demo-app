import { AppState, FailureAction } from "../appState";
import { MessageStatus } from "../../enums/messageStatus";
import { Message } from './../../models/Message';
import { Dispatch } from 'redux';
import uuidv1 from 'uuid/v1';
import signalR from '../../common/signalR';
import { FetchMessagesRequest } from './../../interfaces/dto/fetchMessagesRequest';
import { IMessage } from './../../interfaces/message';
import { http } from "../../common/http";
import { MessangerState } from "../../interfaces/messangerState";
import { Action } from 'redux';

export const PUSH_MESSAGE = 'PUSH_MESSAGE';
export const CHANGE_MESSAGE_STATUS = 'CHANGE_MESSAGE_STATUS';
export const REQUEST_MESSAGES = 'REQUEST_MESSAGES';
export const RECEIVE_MESSAGES = 'RECEIVE_MESSAGES';
export const FAILURE_MESSAGES = 'FAILURE_MESSAGES';

export interface PushMessageAction extends Action {
  message: Message;
} 

export interface ChangeMessageStatusAction extends Action {
  bookingId: string;
  messageId: string;
  status: MessageStatus;
} 

export interface RequestMessagesAction extends Action {
  bookingId: string;
  initialLoad: boolean;
} 

export interface ReceiveMessagesAction extends Action {
  bookingId: string;
  messages: Array<Message>;
} 

export interface FailureMessagesAction extends FailureAction {
  bookingId: string;
}

export function pushMessage(message: Message): PushMessageAction {
  return {
    type: PUSH_MESSAGE,
    message
  };
}

export function changeMessageStatus(
  bookingId: string,
  messageId: string,
  status: MessageStatus
): ChangeMessageStatusAction {
  return {
    type: CHANGE_MESSAGE_STATUS,
    bookingId,
    messageId,
    status
  };
}

export function sendMessage(bookingId: string, text: string) {
  return (dispatch: Dispatch, getState: () => AppState) => {
    const state = getState();
    const userId = <string>(state.user.authUser && state.user.authUser.id);
    const message: Message = new Message({
      id: uuidv1(),
      senderId: userId,
      propertyBookingId: bookingId,
      date: new Date().toString(),
      text: text,
      status: MessageStatus.Sending
    }, true);
    dispatch(pushMessage(message));

    return signalR.connection
      .invoke('SendMessage', message)
      .then(() => dispatch(changeMessageStatus(bookingId, message.id, MessageStatus.Sent)))
      .catch(() => dispatch(changeMessageStatus(bookingId, message.id, MessageStatus.Error)));
  }
}

export function requestMessages(bookingId: string, initialLoad: boolean): RequestMessagesAction {
  return {
    type: REQUEST_MESSAGES,
    bookingId,
    initialLoad
  };
}

export function receiveMessages(bookingId: string, messages: Array<Message>): ReceiveMessagesAction {
  return {
    type: RECEIVE_MESSAGES,
    bookingId,
    messages
  };
}

export function failureMessages(bookingId: string, error: Error): FailureMessagesAction {
  return {
    type: FAILURE_MESSAGES,
    bookingId,
    error
  };
}

function allowedToFetchMessages(bookingId: string, messangerState?: MessangerState): boolean {
  if (messangerState) {
    return !messangerState.isLoading && messangerState.canLoadMore;
  } else {
    return false;
  }
}

export function fetchMessages(bookingId: string, initialLoad: boolean) {
  return (dispatch: Dispatch, getState: () => AppState) => {
    const state = getState();
    const messangerState = state.messanger.states.find(s => s.connectedEntityId === bookingId);
    if (allowedToFetchMessages(bookingId, messangerState) || initialLoad) {
      dispatch(requestMessages(bookingId, initialLoad));

      const request: FetchMessagesRequest = {
        bookingId,
        listParameters: {
          page: messangerState && !initialLoad
            ? messangerState.page
            : 1,
          perPageAmount: 20,
          orderBy: 'Date',
          isDesc: true
        }
      };
      const headers = {
        'Content-Type': 'application/json'
      };

      return http.stickAuthHeader(headers)
        .then(headers => {
          return fetch(`${process.env.REACT_APP_CORE_API_URL}/messanger/message/list`, {
            method: "POST",
            body: JSON.stringify(request),
            headers
          });
        })
        .then(response => response.json())
        .then((result: Array<IMessage>) => {
          const currentUserId = state.user.authUser && state.user.authUser.id;
          const mappedModels = result.map(m => new Message(m, m.senderId === currentUserId));
          return dispatch(receiveMessages(bookingId, mappedModels));
        })
        .catch(error => dispatch(failureMessages(bookingId, error)));
    } else {
      return Promise.resolve();
    }
  }
}