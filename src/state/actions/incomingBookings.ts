import {
  FailureAction,
  AppState
} from "../appState";
import { PropertyBooking } from './../../models/propertyBooking';
import { Dispatch, Action } from 'redux';
import { http } from './../../common/http';
import { PropertyCard } from './../../models/propertyCard';
import { PageableListParameters } from './../../interfaces/pageableListParameters';
import { NewPropertyBookings } from "../../interfaces/newIncomingBookings";

export const SET_SELECTED_PROPERTY = 'SET_SELECTED_PROPERTY';
export const REQUEST_INCOMING_BOOKINGS = 'REQUEST_INCOMING_BOOKINGS';
export const RECEIVE_INCOMING_BOOKINGS = 'RECEIVE_INCOMING_BOOKINGS';
export const FAILURE_INCOMING_BOOKINGS = 'FAILURE_INCOMING_BOOKINGS';
export const REQUEST_PROPERTY_SUGGESTIONS = 'REQUEST_PROPERTY_SUGGESTIONS';
export const RECEIVE_PROPERTY_SUGGESTIONS = 'RECEIVE_PROPERTY_SUGGESTIONS';
export const FAILURE_PROPERTY_SUGGESTIONS = 'FAILURE_PROPERTY_SUGGESTIONS';
export const RECEIVE_NEW_PROPERTY_BOOKINGS = 'RECEIVE_NEW_PROPERTY_BOOKINGS';
export const RECEIVE_NEW_PROPERTY_BOOKING = 'RECEIVE_NEW_PROPERTY_BOOKING';
export const MARK_NEW_PROPERTY_BOOKINGS_AS_SEEN = 'MARK_NEW_PROPERTY_BOOKINGS_AS_SEEN';

export interface RequestIncomingBookingsAction extends Action {
  initialLoad: boolean;
} 

export interface SetSelectedPropertyAction extends Action {
  property: PropertyCard | null;
} 

export interface ReceiveIncomingBookingsAction extends Action {
  bookings: Array<PropertyBooking>;
} 

export interface ReceivePropertySuggestionsAction extends Action {
  suggestions: Array<PropertyCard>;
} 

export interface ReceiveNewPropertyBookingsAction extends Action {
  newPropertyBookings: Array<NewPropertyBookings>;
} 

export interface ReceiveNewPropertyBookingAction extends Action {
  newBooking: PropertyBooking;
} 

export interface MarkNewPropertyBookingsAsSeenAction extends Action {
  propertyId: string;
} 

export function setSelectedProperty(property: PropertyCard | null): SetSelectedPropertyAction {
  return {
    type: SET_SELECTED_PROPERTY,
    property
  };
}

export function requestIncomingBookings(initialLoad: boolean): RequestIncomingBookingsAction {
  return {
    type: REQUEST_INCOMING_BOOKINGS,
    initialLoad
  };
}

export function receiveIncomingBookings(bookings: Array<PropertyBooking>): ReceiveIncomingBookingsAction {
  return {
    type: RECEIVE_INCOMING_BOOKINGS,
    bookings
  };
}

export function failureIncomingBookings(error: any): FailureAction {
  return {
    type: FAILURE_INCOMING_BOOKINGS,
    error
  };
}

function allowedToFetchBookings(state: AppState): boolean {
  return !state.incomingBookings.isLoading
    && state.incomingBookings.canLoadMore;
}

export function fetchIncomingBookings(propertyId: string | null, initialLoad: boolean = false) {
  return (dispatch: Dispatch, getState: () => AppState) => {
    const state = getState();
    if (!propertyId) {
      return dispatch(setSelectedProperty(null));
    } else if (allowedToFetchBookings(state) || initialLoad) {
      dispatch(requestIncomingBookings(initialLoad));

      const listParameters: PageableListParameters = {
        page: initialLoad
          ? 1
          : state.incomingBookings.page,
        perPageAmount: 25,
        isDesc: false,
        orderBy: 'CheckInDate'
      };

      const request = {
        propertyId: propertyId,
        listParameters
      };

      const headers = {
        'Content-Type': 'application/json'
      };

      return http.stickAuthHeader(headers)
        .then(headers => {
          return fetch(`${process.env.REACT_APP_CORE_API_URL}/booking/list/incoming`, {
            method: "POST",
            body: JSON.stringify(request),
            headers
          })
        })
        .then(response => response.json())
        .then(bookings => {
          const mappedModels = bookings.map(b => new PropertyBooking(b));
          dispatch(receiveIncomingBookings(mappedModels));
        })
        .catch(error => dispatch(failureIncomingBookings(error)));
    } else {
      return Promise.resolve();
    }
  }
}

export function requestPropertySuggestions(): Action {
  return {
    type: REQUEST_PROPERTY_SUGGESTIONS
  };
}

export function receivePropertySuggestions(suggestions: Array<PropertyCard>): ReceivePropertySuggestionsAction {
  return {
    type: RECEIVE_PROPERTY_SUGGESTIONS,
    suggestions
  };
}

export function failurePropertySuggestions(error: any): FailureAction {
  return {
    type: FAILURE_PROPERTY_SUGGESTIONS,
    error
  };
}

export function fetchPropertySuggestions(address: string) {
  return (dispatch: Dispatch) => {
    dispatch(requestPropertySuggestions());

    const headers = {
      'Content-Type': 'application/json'
    };

    return http.stickAuthHeader(headers)
      .then(headers => {
        return fetch(`${process.env.REACT_APP_CORE_API_URL}/property/list/published/suggestions/${address}`, {
          method: "GET",
          headers
        })
      })
      .then(response => response.json())
      .then(result => {
        const mappedModels = result.map(p => new PropertyCard(p));
        return dispatch(receivePropertySuggestions(mappedModels));
      })
      .catch(error => {
        console.dir(error);
        dispatch(failurePropertySuggestions(error))
      });
  }
}

export function fetchSelectedProperty(id: string | null) {
  return (dispatch: Dispatch, getState: () => AppState) => {
    if (!id) {
      return Promise.resolve(dispatch(setSelectedProperty(null)));
    } else {
      const state = getState();
      const propertyInState = state.incomingBookings.suggestions.find(p => p.id === id);

      if (propertyInState) {
        return Promise.resolve(dispatch(setSelectedProperty(propertyInState)));
      } else {
        const headers = {
          'Content-Type': 'application/json'
        };

        return fetch(`${process.env.REACT_APP_CORE_API_URL}/property/card/${id}`, {
          method: "GET",
          headers
        })
          .then(response => response.json())
          .then(result => {
            return dispatch(setSelectedProperty(new PropertyCard(result)));
          })
          .catch(error => {
            console.dir(error);
          });
      }
    }
  }
}

export function receiveNewPropertyBookings(newPropertyBookings: Array<NewPropertyBookings>): ReceiveNewPropertyBookingsAction {
  return {
    type: RECEIVE_NEW_PROPERTY_BOOKINGS,
    newPropertyBookings
  };
}

export function receiveNewBooking(newBooking: PropertyBooking): ReceiveNewPropertyBookingAction {
  return {
    type: RECEIVE_NEW_PROPERTY_BOOKING,
    newBooking
  };
}

export function markNewPropertyBookingsAsSeen(propertyId: string): MarkNewPropertyBookingsAsSeenAction {
  return {
    type: MARK_NEW_PROPERTY_BOOKINGS_AS_SEEN,
    propertyId
  };
}