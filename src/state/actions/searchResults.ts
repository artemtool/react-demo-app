import { Dispatch, Action } from "redux";
import { AppState } from "../appState";
import { SearchRequest } from './../../models/dto/searchRequest';
import { PropertyCard } from './../../models/propertyCard';
import { SearchParameters } from "../../interfaces/SearchParameters";

export const REQUEST_PROPERTIES = 'REQUEST_PROPERTIES';
export const RECEIVE_PROPERTIES = 'RECEIVE_PROPERTIES';
export const FAILURE_PROPERTIES = 'FAILURE_PROPERTIES';

export interface RequestPropriesAction extends Action {
  initialLoad: boolean;
} 

export interface ReceivePropertiesAction extends Action {
  properties: Array<PropertyCard>;
} 

export interface FailurePropertiesAction extends Action {
  error: any;
} 

export function requestProperties(initialLoad: boolean): RequestPropriesAction {
  return {
    type: REQUEST_PROPERTIES,
    initialLoad
  };
}

export function receiveProperties(properties: Array<PropertyCard>): ReceivePropertiesAction {
  return {
    type: RECEIVE_PROPERTIES,
    properties
  };
}

export function failureProperties(error): FailurePropertiesAction {
  return {
    type: FAILURE_PROPERTIES,
    error
  };
}

function allowedToFetchProperties(state: AppState): boolean {
  return !state.searchResults.isLoading
    && state.searchResults.canLoadMore;
}

export function fetchProperties(params: SearchParameters, initialLoad: boolean) {
  return (dispatch: Dispatch, getState: () => AppState) => {
    const state = getState();
    if (allowedToFetchProperties(state) || initialLoad) {
      dispatch(requestProperties(initialLoad));

      const request = new SearchRequest(
        params,
        10,
        initialLoad
          ? 1
          : state.searchResults.page
      );

      return fetch(`${process.env.REACT_APP_CORE_API_URL}/property/search`, {
        method: "POST",
        body: JSON.stringify(request),
        headers: {
          'Content-Type': 'application/json'
        }
      })
        .then(response => response.json())
        .then(result => {
          const mappedModels = result.map(p => new PropertyCard(p));
          return dispatch(receiveProperties(mappedModels));
        })
        .catch(error => dispatch(failureProperties(error)));
    } else {
      return Promise.resolve();
    }
  }
}