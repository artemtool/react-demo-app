import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as fetchMock from 'fetch-mock'
import { testMock } from './../../testObjectsSet';
import { http } from './../../common/http';
import { RequestPublishedPropriesAction, requestPublishedProperties, ReceivePublishedPropertiesAction, receivePublishedProperties, failurePublishedProperties, fetchPublishedProperties, ChangePropertyStatusSuccessAction, changePropertyStatus, changePropertyStatusSuccess, DeletePropertySuccessAction, deletePropertySuccess, deleteProperty } from './publishedProperties';
import { FailureAction } from '../appState';
import { ChangePropertyStatusRequest } from './../../interfaces/dto/changePropertyStatusRequest';
import { PropertyStatus } from '../../enums/propertyStatus';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('incomingBookings actions', () => {
  afterEach(() => {
    fetchMock.reset();
    fetchMock.restore();
  });

  it('should create REQUEST_PUBLISHED_PROPERTIES action', () => {
    const isInitialLoad = true;
    const expectedAction: RequestPublishedPropriesAction = {
      type: 'REQUEST_PUBLISHED_PROPERTIES',
      initialLoad: isInitialLoad
    };

    expect(requestPublishedProperties(isInitialLoad))
      .toEqual(expectedAction);
  });

  it('should create RECEIVE_PUBLISHED_PROPERTIES action', () => {
    const properties = testMock.propertyCard.getFirstSet(true);
    const expectedAction: ReceivePublishedPropertiesAction = {
      type: 'RECEIVE_PUBLISHED_PROPERTIES',
      properties
    };

    expect(receivePublishedProperties(properties))
      .toEqual(expectedAction);
  });

  it('should create FAILURE_PUBLISHED_PROPERTIES action', () => {
    const error = new Error('Test error!');
    const expectedAction: FailureAction = {
      type: 'FAILURE_PUBLISHED_PROPERTIES',
      error
    }

    expect(failurePublishedProperties(error))
      .toEqual(expectedAction);
  });

  it('should create [REQUEST_PUBLISHED_PROPERTIES, RECEIVE_PUBLISHED_PROPERTIES] async actions', () => {
    const isInitialLoad = true;
    const requestAction: RequestPublishedPropriesAction = {
      type: 'REQUEST_PUBLISHED_PROPERTIES',
      initialLoad: isInitialLoad
    };
    const receiveAction: ReceivePublishedPropertiesAction = {
      type: 'RECEIVE_PUBLISHED_PROPERTIES',
      properties: testMock.propertyCard.getFirstSet(true)
    };
    const store = mockStore({
      publishedProperties: {
        canLoadMore: true
      }
    });
    http.stickAuthHeader = jest.fn()
      .mockResolvedValueOnce({});
    fetchMock
      .postOnce(`${process.env.REACT_APP_CORE_API_URL}/property/list/published`, {
        body: testMock.propertyCard.getFirstSet(false)
      });

    return store.dispatch(fetchPublishedProperties(isInitialLoad)).then(() => {
      expect(store.getActions()).toEqual([requestAction, receiveAction]);
    });
  });

  it('should do nothing on fetchPublishedProperties()', () => {
    const store = mockStore({
      publishedProperties: {
        isLoading: true
      }
    });

    store.dispatch(fetchPublishedProperties(false)).then(() => {
      expect(store.getActions()).toEqual([]);
    });
  });

  it('should do nothing on fetchPublishedProperties()', () => {
    const store = mockStore({
      publishedProperties: {
        canLoadMore: false
      }
    });

    store.dispatch(fetchPublishedProperties(false)).then(() => {
      expect(store.getActions()).toEqual([]);
    });
  });

  it('should create CHANGE_PROPERTY_STATUS_SUCCESS action', () => {
    const request: ChangePropertyStatusRequest = {
      propertyId: 'any-id',
      propertyStatus: PropertyStatus.Inactive
    };
    const expectedAction: ChangePropertyStatusSuccessAction = {
      type: 'CHANGE_PROPERTY_STATUS_SUCCESS',
      propertyId: request.propertyId,
      propertyStatus: request.propertyStatus
    };

    expect(changePropertyStatusSuccess(request))
      .toEqual(expectedAction);
  });

  it('should create [CHANGE_PROPERTY_STATUS_SUCCESS] async actions', () => {
    const request: ChangePropertyStatusRequest = {
      propertyId: 'any-id',
      propertyStatus: PropertyStatus.Inactive
    };
    const expectedAction: ChangePropertyStatusSuccessAction = {
      type: 'CHANGE_PROPERTY_STATUS_SUCCESS',
      propertyId: request.propertyId,
      propertyStatus: request.propertyStatus
    };
    const store = mockStore();
    http.stickAuthHeader = jest.fn()
      .mockResolvedValueOnce({});
    fetchMock
      .postOnce(`${process.env.REACT_APP_CORE_API_URL}/property/changeStatus`, {
        body: null
      });

    return store.dispatch(changePropertyStatus(request)).then(() => {
      expect(store.getActions()).toEqual([expectedAction]);
    });
  });

  it('should create DELETE_PROPERTY_SUCCESS action', () => {
    const id = 'any-id';
    const expectedAction: DeletePropertySuccessAction = {
      type: 'DELETE_PROPERTY_SUCCESS',
      id
    };

    expect(deletePropertySuccess(id))
      .toEqual(expectedAction);
  });

  it('should create [DELETE_PROPERTY_SUCCESS] async actions', () => {
    const id = 'any-id';
    const expectedAction: DeletePropertySuccessAction = {
      type: 'DELETE_PROPERTY_SUCCESS',
      id
    };
    const store = mockStore();
    http.stickAuthHeader = jest.fn()
      .mockResolvedValueOnce({});
    fetchMock
      .deleteOnce(`${process.env.REACT_APP_CORE_API_URL}/property/${id}`, {
        body: null
      });

    return store.dispatch(deleteProperty(id)).then(() => {
      expect(store.getActions()).toEqual([expectedAction]);
    });
  });
});