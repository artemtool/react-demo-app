import { FailureAction } from "../appState";
import { AppState } from "../appState";
import { Dispatch, Action } from 'redux';
import { http } from './../../common/http';
import { PropertyCard } from '../../models/propertyCard';
import { PageableListParameters } from './../../interfaces/pageableListParameters';
import { ChangePropertyStatusRequest } from './../../interfaces/dto/changePropertyStatusRequest';
import { PropertyStatus } from "../../enums/propertyStatus";

export const REQUEST_PUBLISHED_PROPERTIES = 'REQUEST_PUBLISHED_PROPERTIES';
export const RECEIVE_PUBLISHED_PROPERTIES = 'RECEIVE_PUBLISHED_PROPERTIES';
export const FAILURE_PUBLISHED_PROPERTIES = 'FAILURE_PUBLISHED_PROPERTIES';
export const CHANGE_PROPERTY_STATUS_SUCCESS = 'CHANGE_PROPERTY_STATUS_SUCCESS';
export const DELETE_PROPERTY_SUCCESS = 'DELETE_PROPERTY_SUCCESS';

export interface ReceivePublishedPropertiesAction extends Action {
  properties: Array<PropertyCard>;
} 

export interface ChangePropertyStatusSuccessAction extends Action {
  propertyId: string;
  propertyStatus: PropertyStatus;
} 

export interface DeletePropertySuccessAction extends Action {
  id: string;
} 

export interface RequestPublishedPropriesAction extends Action {
  initialLoad: boolean;
} 

export function requestPublishedProperties(initialLoad: boolean): RequestPublishedPropriesAction {
  return {
    type: REQUEST_PUBLISHED_PROPERTIES,
    initialLoad
  };
}

export function receivePublishedProperties(properties: Array<PropertyCard>): ReceivePublishedPropertiesAction {
  return {
    type: RECEIVE_PUBLISHED_PROPERTIES,
    properties
  };
}

export function failurePublishedProperties(error: any): FailureAction {
  return {
    type: FAILURE_PUBLISHED_PROPERTIES,
    error
  };
}

function allowedToFetchProperties(state: AppState): boolean {
  return !state.publishedProperties.isLoading
    && state.publishedProperties.canLoadMore;
}

export function fetchPublishedProperties(initialLoad: boolean = false) {
  return (dispatch: Dispatch, getState: () => AppState) => {
    const state = getState();
    if (allowedToFetchProperties(state) || initialLoad) {
      dispatch(requestPublishedProperties(initialLoad));

      const parameters: PageableListParameters = {
        page: initialLoad
          ? 1
          : state.publishedProperties.page,
        perPageAmount: 10,
        isDesc: false,
        orderBy: 'PublishDate'
      };

      const headers = {
        'Content-Type': 'application/json'
      }

      return http.stickAuthHeader(headers)
        .then(headers => {
          return fetch(`${process.env.REACT_APP_CORE_API_URL}/property/list/published`, {
            method: "POST",
            body: JSON.stringify(parameters),
            headers
          })
        })
        .then(response => response.json())
        .then(result => {
          const mappedModels = result.map(p => new PropertyCard(p));
          return dispatch(receivePublishedProperties(mappedModels));
        })
        .catch(error => dispatch(failurePublishedProperties(error)));
    } else {
      return Promise.resolve();
    }
  }
}

export function changePropertyStatusSuccess(request: ChangePropertyStatusRequest): ChangePropertyStatusSuccessAction {
  return {
    type: CHANGE_PROPERTY_STATUS_SUCCESS,
    propertyId: request.propertyId,
    propertyStatus: request.propertyStatus
  };
}

export function changePropertyStatus(request: ChangePropertyStatusRequest) {
  return (dispatch: Dispatch) => {

    const headers = {
      'Content-Type': 'application/json'
    };

    return http.stickAuthHeader(headers)
      .then(headers => {
        return fetch(`${process.env.REACT_APP_CORE_API_URL}/property/changeStatus`, {
          method: 'POST',
          body: JSON.stringify(request),
          headers
        });
      })
      .then(() => dispatch(changePropertyStatusSuccess(request)))
      .catch(error => {
        throw error;
      });
  }
}

export function deletePropertySuccess(id: string): DeletePropertySuccessAction {
  return {
    type: DELETE_PROPERTY_SUCCESS,
    id
  };
}

export function deleteProperty(id: string) {
  return (dispatch: Dispatch) => {

    const headers = {
      'Content-Type': 'application/json'
    };

    return http.stickAuthHeader(headers)
      .then(headers => {
        return fetch(`${process.env.REACT_APP_CORE_API_URL}/property/${id}`, {
          method: 'DELETE',
          headers
        });
      })
      .then(response => {
        if (response.status === 403) {
          throw new Error('You cannot delete property, that has incoming bookings');
        } else {
          dispatch(deletePropertySuccess(id));
        }
      })
      .catch(error => {
        throw error;
      });
  }
}