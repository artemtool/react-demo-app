import { ShowDialogAction, showDialog, HideDialogAction, hideDialog, ShowSnackbarAction, HideSnackbarAction, hideSnackbar, showSnackbar } from "./overlays";
import { Dialog } from "../../enums/dialog";
import { IDialog } from './../../interfaces/dialog';
import { ISnackbar } from './../../interfaces/snackbar';
import { Snackbar } from "../../enums/snackbar";

describe('overlays actions', () => {
  it('should create SHOW_DIALOG action', () => {
    const dialog: IDialog = {
      type: Dialog.Booking,
      payload: {
        some: 'data'
      }
    };
    const expectedAction: ShowDialogAction = {
      type: 'SHOW_DIALOG',
      dialog
    };

    expect(showDialog(dialog)).toEqual(expectedAction);
  });

  it('should create HIDE_DIALOG action', () => {
    const dialogType = Dialog.Booking;
    const expectedAction: HideDialogAction = {
      type: 'HIDE_DIALOG',
      dialogType
    };

    expect(hideDialog(dialogType)).toEqual(expectedAction);
  });
  it('should create SHOW_SNACKBAR action', () => {
    const snackbar: ISnackbar = {
      type: Snackbar.Info,
      message: 'Test message!'
    };
    const expectedAction: ShowSnackbarAction = {
      type: 'SHOW_SNACKBAR',
      snackbar
    };

    expect(showSnackbar(snackbar)).toEqual(expectedAction);
  });
  
  it('should create HIDE_SNACKBAR action', () => {
    const snackbarType = Snackbar.Info;
    const expectedAction: HideSnackbarAction = {
      type: 'HIDE_SNACKBAR',
      snackbarType
    };

    expect(hideSnackbar(snackbarType)).toEqual(expectedAction);
  });
});