import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as fetchMock from 'fetch-mock'
import { testMock } from './../../testObjectsSet';
import { http } from './../../common/http';
import { FailureAction } from '../appState';
import { RequestPropriesAction, requestProperties, ReceivePropertiesAction, receiveProperties, failureProperties, fetchProperties } from './searchResults';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('searchResults actions', () => {
  afterEach(() => {
    fetchMock.reset();
    fetchMock.restore();
  });

  it('should create REQUEST_PROPERTIES action', () => {
    const isInitialLoad = true;
    const expectedAction: RequestPropriesAction = {
      type: 'REQUEST_PROPERTIES',
      initialLoad: isInitialLoad
    };

    expect(requestProperties(isInitialLoad))
      .toEqual(expectedAction);
  });

  it('should create RECEIVE_PROPERTIES action', () => {
    const expectedProperties = testMock.propertyCard.getFirstSet(true);
    const expectedAction: ReceivePropertiesAction = {
      type: 'RECEIVE_PROPERTIES',
      properties: expectedProperties
    };

    expect(receiveProperties(expectedProperties))
      .toEqual(expectedAction);
  });

  it('should create FAILURE_PROPERTIES action', () => {
    const error = new Error('Test error!');
    const expectedAction: FailureAction = {
      type: 'FAILURE_PROPERTIES',
      error
    }

    expect(failureProperties(error))
      .toEqual(expectedAction);
  });

  it('should create [REQUEST_PROPERTIES, RECEIVE_PROPERTIES] async actions', () => {
    const isInitialLoad = true;
    const requestAction: RequestPropriesAction = {
      type: 'REQUEST_PROPERTIES',
      initialLoad: isInitialLoad
    };
    const receiveAction: ReceivePropertiesAction = {
      type: 'RECEIVE_PROPERTIES',
      properties: testMock.propertyCard.getFirstSet(true)
    };
    const store = mockStore({
      searchResults: {
        canLoadMore: true
      }
    });
    http.stickAuthHeader = jest.fn()
      .mockResolvedValueOnce({});
    fetchMock
      .postOnce(`${process.env.REACT_APP_CORE_API_URL}/property/search`, {
        body: testMock.propertyCard.getFirstSet(false)
      });

    return store.dispatch(fetchProperties(testMock.searchFilters.getModified(), isInitialLoad)).then(() => {
      expect(store.getActions()).toEqual([requestAction, receiveAction]);
    });
  });

  it('should do nothing on fetchProperties()', () => {
    const store = mockStore({
      searchResults: {
        isLoading: true
      }
    });

    store.dispatch(fetchProperties(testMock.searchFilters.getModified(), false)).then(() => {
      expect(store.getActions()).toEqual([]);
    });
  });

  it('should do nothing on fetchProperties()', () => {
    const store = mockStore({
      searchResults: {
        canLoadMore: false
      }
    });

    store.dispatch(fetchProperties(testMock.searchFilters.getModified(), false)).then(() => {
      expect(store.getActions()).toEqual([]);
    });
  });
});