import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as fetchMock from 'fetch-mock'
import { testMock } from './../../testObjectsSet';
import { FailureAction } from '../appState';
import { startSignIn, SignInSuccessAction, signInSuccess, signInFailure, signInWithEmail, trySignIn, startSignUp, signUpSuccess, signUpFailure, signUpWithEmail, startSignOut, signOutSuccess, signOutFailure, signOut, UpdateUserSuccessAction, updateUserSuccess, changeUserImage, deleteUserImage, updateUserContacts, updateUserDetails } from './user';
import { ACCESS_TOKEN, REFRESH_TOKEN } from './../../constants/keys';
import { http } from '../../common/http';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('user actions', () => {
  afterEach(() => {
    fetchMock.reset();
    fetchMock.restore();
  });

  it('should create SIGN_IN action', () => {
    const expectedAction = {
      type: 'SIGN_IN'
    };

    expect(startSignIn())
      .toEqual(expectedAction);
  });

  it('should create SIGN_IN_SUCCESS action', () => {
    const expectedUser = testMock.getUser(true);
    const expectedAction: SignInSuccessAction = {
      type: 'SIGN_IN_SUCCESS',
      user: expectedUser
    };

    expect(signInSuccess(expectedUser))
      .toEqual(expectedAction);
  });

  it('should create SIGN_IN_FAILURE action', () => {
    const error = new Error('Error for testing purporses');
    const expectedAction: FailureAction = {
      type: 'SIGN_IN_FAILURE',
      error
    }

    expect(signInFailure(error))
      .toEqual(expectedAction);
  });

  it('should create [SIGN_IN, SIGN_IN_SUCCESS] async actions and save access and refresh tokens to localStorage', () => {
    const signInAction = {
      type: 'SIGN_IN'
    };
    const expectedUser = testMock.getUser(true);
    const signInSuccessAction: SignInSuccessAction = {
      type: 'SIGN_IN_SUCCESS',
      user: expectedUser
    };

    const store = mockStore();
    const mockResponse = {
      user: testMock.getUser(false),
      accessToken: 'access token',
      refreshToken: 'refresh token'
    };
    fetchMock
      .postOnce(`${process.env.REACT_APP_AUTH_API_URL}/user/signin`, {
        body: mockResponse
      });
    // https://github.com/clarkbw/jest-localstorage-mock/issues/88
    spyOn(Storage.prototype, 'setItem');

    return store.dispatch(signInWithEmail('email', 'pwd'))
      .then(user => {
        expect(store.getActions()).toEqual([signInAction, signInSuccessAction]);
        expect(user).toEqual(expectedUser);
        expect(localStorage.setItem).toBeCalledWith(ACCESS_TOKEN, mockResponse.accessToken);
        expect(localStorage.setItem).toBeCalledWith(REFRESH_TOKEN, mockResponse.refreshToken);
      });
  });

  it('should create [SIGN_IN, SIGN_IN_FAILURE] async action', () => {
    const signInAction = {
      type: 'SIGN_IN'
    };
    const error = new Error('Error for testing purporses');
    const signInFailureAction: FailureAction = {
      type: 'SIGN_IN_FAILURE',
      error
    };
    const store = mockStore();
    fetchMock
      .postOnce(`${process.env.REACT_APP_AUTH_API_URL}/user/signin`, () => {
        throw error;
      });

    return store.dispatch(signInWithEmail('email', 'pwd'))
      .catch(() => {
        expect(store.getActions()).toEqual([signInAction, signInFailureAction]);
      });
  });

  it('should create [SIGN_IN, SIGN_IN_SUCCESS] async actions', () => {
    const signInAction = {
      type: 'SIGN_IN'
    };
    const signInSuccessAction: SignInSuccessAction = {
      type: 'SIGN_IN_SUCCESS',
      user: testMock.getUser(true)
    };

    const store = mockStore();
    http.stickAuthHeader = jest.fn()
      .mockResolvedValueOnce({});
    fetchMock
      .postOnce(`${process.env.REACT_APP_AUTH_API_URL}/user/iam`, {
        body: testMock.getUser(false)
      });
    spyOn(Storage.prototype, 'getItem')
      .and.returnValue('access token');

    return store.dispatch(trySignIn())
      .then(() => {
        expect(store.getActions()).toEqual([signInAction, signInSuccessAction]);
      });
  });

  it('should create [SIGN_IN, SIGN_IN_FAILURE] async action and remove access and refresh tokens from localStorage', () => {
    const signInAction = {
      type: 'SIGN_IN'
    };
    const error = new Error('Error for testing purporses');
    const signInFailureAction: FailureAction = {
      type: 'SIGN_IN_FAILURE',
      error
    };

    const store = mockStore();
    http.stickAuthHeader = jest.fn()
      .mockResolvedValueOnce({});
    fetchMock
      .postOnce(`${process.env.REACT_APP_AUTH_API_URL}/user/iam`, () => {
        throw error;
      });
    // https://github.com/clarkbw/jest-localstorage-mock/issues/88
    spyOn(Storage.prototype, 'removeItem');
    spyOn(Storage.prototype, 'getItem')
      .and.returnValue('access token');

    return store.dispatch(trySignIn())
      .then(() => {
        expect(store.getActions()).toEqual([signInAction, signInFailureAction]);
      });
  });

  it('should do nothing beacause localStorage does not contain access_token', () => {
    const store = mockStore();

    return store.dispatch(trySignIn())
      .then(() => {
        expect(store.getActions()).toEqual([]);
      });
  });

  it('should create SIGN_UP action', () => {
    const expectedAction = {
      type: 'SIGN_UP'
    };

    expect(startSignUp())
      .toEqual(expectedAction);
  });

  it('should create SIGN_UP_SUCCESS action', () => {
    const expectedAction = {
      type: 'SIGN_UP_SUCCESS'
    };

    expect(signUpSuccess())
      .toEqual(expectedAction);
  });

  it('should create SIGN_UP_FAILURE action', () => {
    const error = new Error('Error for testing purporses');
    const expectedAction: FailureAction = {
      type: 'SIGN_UP_FAILURE',
      error
    }

    expect(signUpFailure(error))
      .toEqual(expectedAction);
  });

  it('should create [SIGN_UP, SIGN_UP_SUCCESS] async actions', () => {
    const signUpAction = {
      type: 'SIGN_UP'
    };
    const signUpSuccessAction = {
      type: 'SIGN_UP_SUCCESS'
    };

    const store = mockStore();
    fetchMock
      .postOnce(`${process.env.REACT_APP_AUTH_API_URL}/user/signup`, {
        body: null
      });

    return store.dispatch(signUpWithEmail({
      email: 'email',
      password: 'pwd',
      name: 'name'
    }))
      .then(() => {
        expect(store.getActions()).toEqual([signUpAction, signUpSuccessAction]);
      });
  });

  it('should create [SIGN_UP, SIGN_UP_FAILURE] async action', () => {
    const signUpAction = {
      type: 'SIGN_UP'
    };
    const error = new Error('Error for testing purporses');
    const signUpFailureAction: FailureAction = {
      type: 'SIGN_UP_FAILURE',
      error
    };
    const store = mockStore();
    fetchMock
      .postOnce(`${process.env.REACT_APP_AUTH_API_URL}/user/signup`, () => {
        throw error;
      });

    return store.dispatch(signUpWithEmail({
      email: 'email',
      password: 'pwd',
      name: 'name'
    }))
      .catch(() => {
        expect(store.getActions()).toEqual([signUpAction, signUpFailureAction]);
      });
  });

  it('should create SIGN_OUT action', () => {
    const expectedAction = {
      type: 'SIGN_OUT'
    };

    expect(startSignOut())
      .toEqual(expectedAction);
  });

  it('should create SIGN_OUT_SUCCESS action', () => {
    const expectedAction = {
      type: 'SIGN_OUT_SUCCESS'
    };

    expect(signOutSuccess())
      .toEqual(expectedAction);
  });

  it('should create SIGN_OUT_FAILURE action', () => {
    const error = new Error('Error for testing purporses');
    const expectedAction: FailureAction = {
      type: 'SIGN_OUT_FAILURE',
      error
    }

    expect(signOutFailure(error))
      .toEqual(expectedAction);
  });

  it('should create [SIGN_OUT, SIGN_OUT_SUCCESS] async actions and save access and refresh tokens to localStorage', () => {
    const signOutAction = {
      type: 'SIGN_OUT'
    };
    const signOutSuccessAction = {
      type: 'SIGN_OUT_SUCCESS'
    };

    const store = mockStore();
    http.stickAuthHeader = jest.fn()
      .mockResolvedValue({});
    fetchMock
      .postOnce(`${process.env.REACT_APP_AUTH_API_URL}/user/signout`, {
        body: null
      });
    // https://github.com/clarkbw/jest-localstorage-mock/issues/88
    spyOn(Storage.prototype, 'removeItem');

    return store.dispatch(signOut())
      .then(() => {
        expect(store.getActions()).toEqual([signOutAction, signOutSuccessAction]);
        expect(localStorage.removeItem).toBeCalledWith(ACCESS_TOKEN);
        expect(localStorage.removeItem).toBeCalledWith(REFRESH_TOKEN);
      });
  });

  it('should create [SIGN_OUT, SIGN_OUT_FAILURE] async action', () => {
    const signOutAction = {
      type: 'SIGN_OUT'
    };
    const error = new Error('Error for testing purporses');
    const signOutFailureAction: FailureAction = {
      type: 'SIGN_OUT_FAILURE',
      error
    };
    const store = mockStore();
    fetchMock
      .postOnce(`${process.env.REACT_APP_AUTH_API_URL}/user/signout`, () => {
        throw error;
      });

    return store.dispatch(signOut())
      .catch(() => {
        expect(store.getActions()).toEqual([signOutAction, signOutFailureAction]);
      });
  });

  it('should create SIGN_OUT_SUCCESS action', () => {
    const expectedAction = {
      type: 'SIGN_OUT_SUCCESS'
    };

    expect(signOutSuccess())
      .toEqual(expectedAction);
  });

  it('should create UPDATE_USER_SUCCESS action', () => {
    const user = testMock.getUser(true);
    const expectedAction: UpdateUserSuccessAction = {
      type: 'UPDATE_USER_SUCCESS',
      user
    };

    expect(updateUserSuccess(user))
      .toEqual(expectedAction);
  });

  it('should create [UPDATE_USER_SUCCESS] async action on changeUserImage()', () => {
    const expectedAction: UpdateUserSuccessAction = {
      type: 'UPDATE_USER_SUCCESS',
      user: testMock.getUser(true)
    };

    const store = mockStore();
    http.stickAuthHeader = jest.fn()
      .mockResolvedValue({});
    fetchMock
      .postOnce(`${process.env.REACT_APP_AUTH_API_URL}/user/changeImage`, {
        body: testMock.getUser(false)
      });

    return store.dispatch(changeUserImage(new FormData()))
      .then(() => {
        expect(store.getActions()).toEqual([expectedAction]);
      });
  });

  it('should create [UPDATE_USER_SUCCESS] async action on deleteUserImage()', () => {
    const expectedAction: UpdateUserSuccessAction = {
      type: 'UPDATE_USER_SUCCESS',
      user: testMock.getUser(true)
    };

    const store = mockStore();
    http.stickAuthHeader = jest.fn()
      .mockResolvedValue({});
    fetchMock
      .deleteOnce(`${process.env.REACT_APP_AUTH_API_URL}/user/deleteImage`, {
        body: testMock.getUser(false)
      });

    return store.dispatch(deleteUserImage())
      .then(() => {
        expect(store.getActions()).toEqual([expectedAction]);
      });
  });

  it('should create [UPDATE_USER_SUCCESS] async action on updateUserContacts()', () => {
    const expectedAction: UpdateUserSuccessAction = {
      type: 'UPDATE_USER_SUCCESS',
      user: testMock.getUser(true)
    };

    const store = mockStore();
    http.stickAuthHeader = jest.fn()
      .mockResolvedValue({});
    fetchMock
      .postOnce(`${process.env.REACT_APP_AUTH_API_URL}/user/updateContacts`, {
        body: testMock.getUser(false)
      });

    return store.dispatch(updateUserContacts({}))
      .then(() => {
        expect(store.getActions()).toEqual([expectedAction]);
      });
  });

  it('should create [UPDATE_USER_SUCCESS] async action on updateUserDetails()', () => {
    const expectedAction: UpdateUserSuccessAction = {
      type: 'UPDATE_USER_SUCCESS',
      user: testMock.getUser(true)
    };

    const store = mockStore();
    http.stickAuthHeader = jest.fn()
      .mockResolvedValue({});
    fetchMock
      .postOnce(`${process.env.REACT_APP_AUTH_API_URL}/user/updateDetails`, {
        body: testMock.getUser(false)
      });

    return store.dispatch(updateUserDetails({}))
      .then(() => {
        expect(store.getActions()).toEqual([expectedAction]);
      });
  });
});