import { Dispatch, Action } from "redux";
import { AppState } from "../appState";
import { FailureAction } from "../appState";
import { SearchParameters } from "../../interfaces/SearchParameters";
import { SelectOption } from './../../models/selectOption';
import { SelectGroup } from "../../models/selectGroup";

export const REQUEST_SUGGESTIONS = 'REQUEST_SUGGESTIONS';
export const RECEIVE_SUGGESTIONS = 'RECEIVE_SUGGESTIONS';
export const FAILURE_SUGGESTIONS = 'FAILURE_SUGGESTIONS';
export const UPDATE_PARAMETERS = 'UPDATE_PARAMETERS';

export interface ReceiveSuggestionsAction extends Action {
  suggestions: Array<SelectGroup>;
} 

export interface UpdateParametersAction extends Action {
  params: SearchParameters;
} 

export function requestSuggestions(): Action {
  return {
    type: REQUEST_SUGGESTIONS
  };
}

export function receiveSuggestions(suggestions: Array<SelectGroup>): ReceiveSuggestionsAction {
  return {
    type: RECEIVE_SUGGESTIONS,
    suggestions
  };
}

export function failureSuggestions(error): FailureAction {
  return {
    type: FAILURE_SUGGESTIONS,
    error
  };
}

function shouldFetchSuggestions(state: AppState): boolean {
  return state.searchFilters.suggestions.length === 0
    && !state.searchFilters.isLoading;
}

export function fetchSuggestionsIfNeeded() {
  return (dispatch: Dispatch, getState: () => AppState) => {

    if (shouldFetchSuggestions(getState())) {
      dispatch(requestSuggestions());

      return fetch(`${process.env.REACT_APP_CORE_API_URL}/autocomplete/regions`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      })
        .then(response => response.json())
        .then(regions => {
          let results: Array<SelectGroup> = [];

          for (let region of regions) {
            let group = new SelectGroup(region.name, []);
            for (let city of region.cities) {
              group.options.push(new SelectOption(city.name, city.id));
            }
            results.push(group);
          }
          dispatch(receiveSuggestions(results));
        })
        .catch(error => failureSuggestions(error));
    } else {
      return Promise.resolve();
    }
  }
}

export function updateParameters(params: SearchParameters): UpdateParametersAction {
  return {
    type: UPDATE_PARAMETERS,
    params
  };
}