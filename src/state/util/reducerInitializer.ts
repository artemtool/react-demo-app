import { Action } from "redux";

export function initReducer<T>(
  initialState: T,
  actionHandlers: { [actionType: string]: (state: T, action: Action) => T }
): (state: T, action: Action) => T {
  return function (state: T = initialState, action: Action) {
    if (actionHandlers[action.type]) {
      return actionHandlers[action.type](state, action);
    } else {
      return state;
    }
  }
}