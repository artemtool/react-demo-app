import { IPropertyBooking } from './propertyBooking';
import { IUser } from './user';
import { IPropertyCard } from './propertyCard';

export interface IProperty extends IPropertyCard {
	owner: IUser | null;
	bookings: Array<IPropertyBooking> | null;
  description: string;
  imageNames: Array<string>;
  hasWiFi: boolean;
  hasParkingPlace: boolean;
  hasTV: boolean;
  hasAirConditioner: boolean;
  hasBedLinen: boolean;
  hasComputer: boolean;
  hasDishes: boolean;
  hasHairDryer: boolean;
  hasIroningStaff: boolean;
  hasKettle: boolean;
  hasMicrowaveOven: boolean;
  hasTowels: boolean;
  hasWashingMachine: boolean;
}