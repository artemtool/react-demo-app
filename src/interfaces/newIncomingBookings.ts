export interface NewPropertyBookings {
  propertyId: string;
  propertyAddress: string;
  count: number;
}