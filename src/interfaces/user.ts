export interface IUser {
  id: string;
  email: string;
  role: string;
  name: string;
  surname: string;
  created: string;
  lastLogin: string;
  verified: boolean;
  imageName: string | null;
  phoneNumber1: string | null;
  phoneNumber2: string | null;
  phoneNumber3: string | null;
}