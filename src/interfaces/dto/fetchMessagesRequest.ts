import { PageableListParameters } from './../pageableListParameters';

export interface FetchMessagesRequest {
  bookingId: string;
  listParameters: PageableListParameters;
}