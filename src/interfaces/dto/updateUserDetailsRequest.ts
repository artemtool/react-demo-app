export interface UpdateUserDetailsRequest {
  name?: string;
  surname?: string;
  password?: string;
}