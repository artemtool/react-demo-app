export interface BookPropertyRequest {
  propertyId: string;
  checkInDate: Date;
  checkOutDate: Date;
  guestName: string;
  guestPhoneNumber: string;
  guestsAmount: number;
  comments: string;
}