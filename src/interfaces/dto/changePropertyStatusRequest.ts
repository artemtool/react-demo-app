import { PropertyStatus } from "../../enums/propertyStatus";

export interface ChangePropertyStatusRequest {
  propertyId: string;
  propertyStatus: PropertyStatus;
}