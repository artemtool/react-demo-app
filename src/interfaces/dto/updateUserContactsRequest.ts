export interface UpdateUserContactsRequest {
  email?: string,
  phoneNumberOne?: string,
  phoneNumberTwo?: string,
  phoneNumberThree?: string
}