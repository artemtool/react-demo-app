import { PropertyType } from "../enums/propertyType";

export interface SearchParameters {
  cityId: string;
  checkInDate: Date;
  checkOutDate: Date;
  orderBy: string;
  isDesc: boolean;
  minPrice?: number;
  maxPrice?: number;
  minRooms?: number;
  maxRooms?: number;
  sleeps?: number;
  hasWiFi?: boolean;
  hasParkingPlace?: boolean;
  hasTV?: boolean;
  hasAirConditioner?: boolean;
  hasBedLinen?: boolean;
  hasComputer?: boolean;
  hasDishes?: boolean;
  hasHairDryer?: boolean;
  hasIroningStaff?: boolean;
  hasKettle?: boolean;
  hasMicrowaveOven?: boolean;
  hasTowels?: boolean;
  hasWashingMachine?: boolean;
  propertyType?: PropertyType;
}