import { Message } from './../models/Message';

export interface MessangerState {
  connectedEntityId: string,
  messages: Array<Message>,
  isLoading: boolean,
  canLoadMore: boolean,
  page: number
}