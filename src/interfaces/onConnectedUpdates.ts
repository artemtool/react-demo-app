import { NewPropertyBookings } from "./newIncomingBookings";

export interface OnConnectedUpdates { 
  newPropertyBookings: Array<NewPropertyBookings>;
}