import { ICity } from './city';
import { PropertyType } from '../enums/propertyType';
import { PropertyStatus as PropertyStatusType } from '../enums/propertyStatus';

export interface IPropertyCard {
  id: string;
	city: ICity;
  type: PropertyType;
  title: string;
  address: string;
  mainImageName: string | null;
  dailyPrice: number;
  weeklyPrice: number;
  rooms: number;
  area: number;
  sleeps: number;
  floor: number;
  publishDate: string;
  status: PropertyStatusType;
  ownerId: string;
}