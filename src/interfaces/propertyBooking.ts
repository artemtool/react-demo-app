export interface IPropertyBooking {
  id: string;
  bookerId: string;
  propertyId: string;
  checkInDate: string;
  checkOutDate: string;
  guestName: string;
  guestPhoneNumber: string;
  guestsAmount: number;
  comments: string;
  dailyPrice: number;
  propertyMainImageName: string;
  propertyAddress: string;
  propertyOwnerName: string;
  propertyOwnerPhone: string;
}