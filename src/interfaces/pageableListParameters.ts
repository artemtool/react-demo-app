export interface PageableListParameters {
  page: number;
  perPageAmount: number;
  orderBy: string;
  isDesc: boolean;
}