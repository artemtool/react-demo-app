import { Dialog } from "../enums/dialog";

export interface IDialog {
  type: Dialog;
  payload: Object;
}