import { MessageStatus } from "../enums/messageStatus";

export interface IMessage {
  id: string;
  senderId: string;
  propertyBookingId: string;
  date: string;
  text: string;
  status: MessageStatus;
}