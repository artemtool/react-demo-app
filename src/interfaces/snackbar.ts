import { Snackbar } from "../enums/snackbar";

export interface ISnackbar {
  type: Snackbar,
  message: string
}