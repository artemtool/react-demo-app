export interface PropertyStatus {
  label: string;
  tooltip: string;
}