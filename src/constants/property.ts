import { PropertyFeature } from './../models/propertyFeature';
import { SelectOption } from './../models/selectOption';
import { PropertyType } from '../enums/propertyType';

export const propertyFeatures: Array<PropertyFeature> = [
  new PropertyFeature('hasWiFi', 'Wi-Fi'),
  new PropertyFeature('hasParkingPlace', 'Parking place'),
  new PropertyFeature('hasTV', 'TV'),
  new PropertyFeature('hasAirConditioner', 'Air conditioner'),
  new PropertyFeature('hasComputer', 'Computer'),
  new PropertyFeature('hasIroningStaff', 'Ironing staff'),
  new PropertyFeature('hasKettle', 'Kettle'),
  new PropertyFeature('hasMicrowaveOven', 'Microwave oven'),
  new PropertyFeature('hasHairDryer', 'Hair dryer'),
  new PropertyFeature('hasWashingMachine', 'Washing machine'),
  new PropertyFeature('hasBedLinen', 'Bed linen'),
  new PropertyFeature('hasTowels', 'Towels'),
  new PropertyFeature('hasDishes', 'Dishes')
];

propertyFeatures.sort((featureOne, featureTwo) => {
  return featureOne.label > featureTwo.label ? 1 : -1;
});

const MAX_SLEEPS: number = 22;
export const sleepsOptionList: Array<SelectOption> = [];
for (let i = 0; i < MAX_SLEEPS; i++) {
  sleepsOptionList.push(new SelectOption(String(i + 1), i + 1));
}

const MAX_FLOORS: number = 54;
export const floorsOptionList: Array<SelectOption> = [];
for (let i = 0; i < MAX_FLOORS; i++) {
  floorsOptionList.push(new SelectOption(String(i + 1), i + 1));
}

const MAX_ROOMS: number = 10;
export const roomsOptionList: Array<SelectOption> = [];
for (let i = 0; i < MAX_ROOMS; i++) {
  roomsOptionList.push(new SelectOption(String(i + 1), i + 1));
}

export const propertyTypesOptionList: Array<SelectOption> = [
  new SelectOption('Apartments', PropertyType.Apartment),
  new SelectOption('Villa', PropertyType.Villa),
  new SelectOption('Hotel room', PropertyType.HotelRoom)
];