import { IPropertyBooking } from './interfaces/propertyBooking';
import { SelectGroup } from './models/selectGroup';
import { SelectOption } from './models/selectOption';
import { IUser } from './interfaces/user';
import { IPropertyCard } from './interfaces/propertyCard';
import { IProperty } from './interfaces/property';
import { PropertyBooking } from './models/propertyBooking';
import { Property } from './models/property';
import { PropertyCard } from './models/propertyCard';
import { User } from './models/user';

const MOCK_DATA = {
  BOOKING: {
    FIRST_SET: <Array<IPropertyBooking>>[
      {
        "id": "40b4553c-eac6-4a0a-abb3-08d617cbe72f",
        "bookerId": "00000000-0000-0000-0000-000000000000",
        "propertyId": "4cb807f7-7fee-4eb0-c460-08d617c08831",
        "checkInDate": "2018-09-10T21:00:00",
        "checkOutDate": "2018-09-11T21:00:00",
        "guestName": "Joe Doe",
        "guestPhoneNumber": "(064) 564-56-45",
        "guestsAmount": 3,
        "comments": "No smoking room.",
        "dailyPrice": 24,
        "propertyMainImageName": "property/4cb807f7-7fee-4eb0-c460-08d617c08831/image/pexels-photo-186077.jpeg",
        "propertyAddress": "Винница, Soborna street 55",
        "propertyOwnerName": "Artemii Pererodov",
        "propertyOwnerPhone": "0930122836  "
      },
      {
        "id": "31a3253c-eec4-4a0a-abb3-08d617cbe72d",
        "bookerId": "00000000-0000-0000-0000-000000000000",
        "propertyId": "4cb807f7-7fee-4eb0-c460-08d617c08831",
        "checkInDate": "2018-08-10T21:00:00",
        "checkOutDate": "2018-08-11T21:00:00",
        "guestName": "Artemii ",
        "guestPhoneNumber": "(064) 564-56-45",
        "guestsAmount": 3,
        "comments": "No smoking room.",
        "dailyPrice": 24,
        "propertyMainImageName": "property/4cb807f7-7fee-4eb0-c460-08d617c08831/image/pexels-photo-186077.jpeg",
        "propertyAddress": "Винница, Soborna street 55",
        "propertyOwnerName": "Artemii Pererodov",
        "propertyOwnerPhone": "0930122836  "
      }
    ],
    SECOND_SET: <Array<IPropertyBooking>>[
      {
        "id": "40b4553c-eac6-4a0a-abb3-08d617cbe72f",
        "bookerId": "00000000-0000-0000-0000-000000000000",
        "propertyId": "4cb807f7-7fee-4eb0-c460-08d617c08831",
        "checkInDate": "2018-09-10T21:00:00",
        "checkOutDate": "2018-09-11T21:00:00",
        "guestName": "Joe Doe",
        "guestPhoneNumber": "(064) 564-56-45",
        "guestsAmount": 3,
        "comments": "No smoking room.",
        "dailyPrice": 24,
        "propertyMainImageName": "property/4cb807f7-7fee-4eb0-c460-08d617c08831/image/pexels-photo-186077.jpeg",
        "propertyAddress": "Винница, Soborna street 55",
        "propertyOwnerName": "Artemii Pererodov",
        "propertyOwnerPhone": "0930122836  "
      }
    ]
  },
  AUTOCOMPLETE: {
    SUGGESTIONS: [
      new SelectGroup('Vinnytsia region', [
        new SelectOption('Vinnytsia', '4cb807f7-7fee-4eb0-c460-08d617c08831'),
        new SelectOption('Hmilnyk', '7cb807f7-7fee-4eb0-c460-08d617c08813')
      ]),
      new SelectGroup('Kyiv region', [
        new SelectOption('Kyiv', '2ac807f7-7fee-4eb0-c460-08d617c08642'),
        new SelectOption('Bila Cerkva', '1aa807f7-7fee-4eb0-c460-08d617c08824')
      ])
    ]
  },
  PROPERTY: {
    FIRST_SET: <Array<IProperty>>[
      {
        "id": "3aea7e0c-da26-4318-6448-08d617eaa7b4",
        "city": {
          "id": "c84cf573-d5de-4c1c-a7d7-fdcc30f201d8",
          "name": "Винница"
        },
        "owner": null,
        "bookings": null,
        "type": 0,
        "title": "Super property somewhere in nice place.",
        "address": "Somewhere street 777",
        "description": "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.",
        "mainImageName": "property/3aea7e0c-da26-4318-6448-08d617eaa7b4/image/summer-road-trip.jpg",
        "imageNames": [
          "property/3aea7e0c-da26-4318-6448-08d617eaa7b4/image/pexels-photo-186077.jpeg",
          "property/3aea7e0c-da26-4318-6448-08d617eaa7b4/image/summer-road-trip.jpg"
        ],
        "dailyPrice": 24,
        "weeklyPrice": 130,
        "rooms": 3,
        "area": 102,
        "sleeps": 6,
        "floor": 7,
        "hasWiFi": true,
        "hasParkingPlace": true,
        "hasTV": false,
        "hasAirConditioner": true,
        "hasBedLinen": false,
        "hasComputer": true,
        "hasDishes": false,
        "hasHairDryer": true,
        "hasIroningStaff": false,
        "hasKettle": true,
        "hasMicrowaveOven": false,
        "hasTowels": true,
        "hasWashingMachine": false,
        "publishDate": "2018-09-11T16:36:59.2193304",
        "status": 0
      },
      {
        "id": "9e441e7f-2918-5a42-a24a-ab43ef80d6f1",
        "city": {
          "id": "c84cf573-d5de-4c1c-a7d7-fdcc30f201d8",
          "name": "Винница"
        },
        "owner": null,
        "bookings": null,
        "type": 0,
        "title": "ullamco ea nisi ut paria",
        "address": "occaecat dolore reprehen",
        "description": "esse irure eiusmod quislaborum adipisicing consequatlaboris sitlaboris deserunt ipsum sintin doloreproidentmagna est dolore utin dolor exexercitationcupidatat exercitation nisi",
        "mainImageName": null,
        "imageNames": [],
        "dailyPrice": 14,
        "weeklyPrice": 121,
        "rooms": 1,
        "area": 69,
        "sleeps": 1,
        "floor": 7,
        "hasWiFi": false,
        "hasParkingPlace": false,
        "hasTV": true,
        "hasAirConditioner": false,
        "hasBedLinen": true,
        "hasComputer": true,
        "hasDishes": true,
        "hasHairDryer": false,
        "hasIroningStaff": false,
        "hasKettle": true,
        "hasMicrowaveOven": false,
        "hasTowels": false,
        "hasWashingMachine": false,
        "publishDate": "0001-01-01T00:00:00",
        "status": 0
      }
    ]
  },
  PROPERTY_CARD: {
    FIRST_SET: <Array<IPropertyCard>>[
      {
        "publishDate": "2018-09-11T11:28:20.5052226",
        "id": "4cb807f7-7fee-4eb0-c460-08d617c08831",
        "city": {
          "id": "c84cf573-d5de-4c1c-a7d7-fdcc30f201d8",
          "name": "Винница"
        },
        "type": 0,
        "title": "Nice flat in the city center",
        "address": "Soborna street 55",
        "mainImageName": "property/4cb807f7-7fee-4eb0-c460-08d617c08831/image/pexels-photo-186077.jpeg",
        "dailyPrice": 24,
        "weeklyPrice": 135,
        "rooms": 2,
        "area": 54,
        "sleeps": 4,
        "floor": 3,
        "status": 0
      },
      {
        "publishDate": "0001-01-01T00:00:00",
        "id": "2443a8ce-0c9d-5ee0-b845-dcfa105ddf69",
        "city": {
          "id": "e746f249-83c4-445f-b25e-5a02b96214b2",
          "name": "Днепропетровск"
        },
        "type": 1,
        "title": "et Excepteur esse",
        "address": "exercitation do aliqua",
        "mainImageName": null,
        "dailyPrice": 32,
        "weeklyPrice": 130,
        "rooms": 1,
        "area": 29,
        "sleeps": 5,
        "floor": 9,
        "status": 0
      },
      {
        "publishDate": "0001-01-01T00:00:00",
        "id": "6c95ce85-35f2-5fd0-93ad-bf1c0e0e6e14",
        "city": {
          "id": "e746f249-83c4-445f-b25e-5a02b96214b2",
          "name": "Днепропетровск"
        },
        "type": 1,
        "title": "esse consequ",
        "address": "deserunt cupidatat Ut",
        "mainImageName": null,
        "dailyPrice": 49,
        "weeklyPrice": 136,
        "rooms": 2,
        "area": 94,
        "sleeps": 2,
        "floor": 24,
        "status": 0
      },
      {
        "publishDate": "0001-01-01T00:00:00",
        "id": "9f050b6e-8742-50ed-bd95-9696f008dd06",
        "city": {
          "id": "e746f249-83c4-445f-b25e-5a02b96214b2",
          "name": "Днепропетровск"
        },
        "type": 1,
        "title": "magna do cupi",
        "address": "magna labore ut voluptate ad",
        "mainImageName": null,
        "dailyPrice": 27,
        "weeklyPrice": 120,
        "rooms": 1,
        "area": 23,
        "sleeps": 3,
        "floor": 18,
        "status": 0
      }
    ],
    SECOND_SET: <Array<IPropertyCard>>[
      {
        "publishDate": "0001-01-01T00:00:00",
        "id": "178b5ee3-9ae4-5153-995a-a03392094b77",
        "city": {
          "id": "e746f249-83c4-445f-b25e-5a02b96214b2",
          "name": "Днепропетровск"
        },
        "type": 1,
        "title": "in ullamco laborum",
        "address": "sit elit do",
        "mainImageName": null,
        "dailyPrice": 35,
        "weeklyPrice": 85,
        "rooms": 2,
        "area": 34,
        "sleeps": 2,
        "floor": 28,
        "status": 0
      },
      {
        "publishDate": "0001-01-01T00:00:00",
        "id": "349aaa4f-3a7e-5d4d-b648-abc499eb89ed",
        "city": {
          "id": "e746f249-83c4-445f-b25e-5a02b96214b2",
          "name": "Днепропетровск"
        },
        "type": 1,
        "title": "cillum officia",
        "address": "ut reprehenderi",
        "mainImageName": null,
        "dailyPrice": 29,
        "weeklyPrice": 136,
        "rooms": 1,
        "area": 33,
        "sleeps": 3,
        "floor": 4,
        "status": 0
      },
      {
        "publishDate": "0001-01-01T00:00:00",
        "id": "08ad02b3-9dd2-59a8-9df7-b27a3a38c780",
        "city": {
          "id": "e746f249-83c4-445f-b25e-5a02b96214b2",
          "name": "Днепропетровск"
        },
        "type": 2,
        "title": "nulla ipsum fugi",
        "address": "nulla aliquip",
        "mainImageName": null,
        "dailyPrice": 49,
        "weeklyPrice": 137,
        "rooms": 2,
        "area": 21,
        "sleeps": 4,
        "floor": 1,
        "status": 0
      }
    ]
  },
  SEARCH_FILTERS: {
    MODIFIED: {
      cityId: '4cb807f7-7fee-4eb0-c460-08d617c08831',
      checkInDate: new Date(),
      checkOutDate: new Date(),
      orderBy: 'DailyPrice',
      isDesc: true,
      minPrice: 10,
      maxPrice: 22,
      minRooms: 1,
      maxRooms: 5,
      sleeps: 2,
      hasWiFi: true,
      hasParkingPlace: true,
      hasTV: true,
      hasAirConditioner: true,
      hasBedLinen: true,
      hasComputer: true,
      hasDishes: true,
      hasHairDryer: true,
      hasIroningStaff: true,
      hasKettle: true,
      hasMicrowaveOven: true,
      hasTowels: true,
      hasWashingMachine: true,
      propertyType: 1
    }
  },
  USER: <IUser>{
    "id": "6db494c5-9c9a-4769-83f9-281c4e846b80",
    "email": "artemtool@gmail.com",
    "role": "Admin",
    "name": "Artemii",
    "surname": "Pererodov",
    "created": "2018-01-01T15:53:00",
    "lastLogin": "2018-01-01T15:53:00",
    "verified": true,
    "imageName": null,
    "phoneNumber1": "0930122836",
    "phoneNumber2": null,
    "phoneNumber3": null
  }
};

const mapData = (data: any, mapToModel: boolean, ctor?: any) => {
  if (mapToModel) {
    if (Array.isArray(data)) {
      return data.map(o => new ctor(o));
    } else {
      return new ctor(data);
    }
  } else {
    return data;
  }
}

export const testMock = {
  booking: {
    getFirstSet: (mapToModel: boolean) => {
      return mapData(MOCK_DATA.BOOKING.FIRST_SET, mapToModel, PropertyBooking);
    },
    getSecondSet: (mapToModel: boolean) => {
      return mapData(MOCK_DATA.BOOKING.SECOND_SET, mapToModel, PropertyBooking);
    }
  },
  autocomplete: {
    getSuggestions: () => {
      return MOCK_DATA.AUTOCOMPLETE.SUGGESTIONS;
    }
  },
  property: {
    getFirstSet: (mapToModel: boolean) => {
      return mapData(MOCK_DATA.PROPERTY.FIRST_SET, mapToModel, Property);
    }
  },
  propertyCard: {
    getFirstSet: (mapToModel: boolean) => {
      return mapData(MOCK_DATA.PROPERTY_CARD.FIRST_SET, mapToModel, PropertyCard);
    },
    getSecondSet: (mapToModel: boolean) => {
      return mapData(MOCK_DATA.PROPERTY_CARD.SECOND_SET, mapToModel, PropertyCard);
    }
  },
  searchFilters: {
    getModified: () => {
      return MOCK_DATA.SEARCH_FILTERS.MODIFIED;
    }
  },
  getUser: (mapToModel: boolean) => {
    return mapData(MOCK_DATA.USER, mapToModel, User);
  }
}