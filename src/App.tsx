import * as React from 'react';
import {
  Route,
  Link,
  Switch
} from 'react-router-dom';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import DrawerMenu from './components/DrawerMenu/DrawerMenu';
import SearchPage from './containers/Search/Search';
import SignUpPage from './containers/SignUp/SignUp';
import SignInPage from './containers/SignIn/SignIn';
import HomePage from './containers/Home/Home';
import * as routes from './constants/routes';
import SearchForm from './components/SearchForm/SearchForm';
import Divider from '@material-ui/core/Divider';
import withCustomTheme from './components/withCustomTheme';
import Dialogs from './components/Dialogs/Dialogs';
import UserBookingsPage from './containers/UserBookings/UserBookings';
import PublishPropertyPage from './containers/PublishProperty/PublishProperty';
import EditPropertyPage from './containers/EditProperty/EditProperty';
import PublishedPropertiesPage from './containers/PublishedProperties/PublishedProperties';
import IncomingBookingsPage from './containers/IncomingBookings/IncomingBookings';
import PropertyPage from './containers/Property/Property';
import SettingsPage from './containers/Settings/Settings';
import Snackbars from './components/Snackbars/Snackbars';
import ProtectedRoute from './components/ProtectedRoute';
import { User } from './enums/user';

const App = (props) => {
  const { classes } = props;

  return (
    <React.Fragment>
      <div className={classes.root}>
        <AppBar position="fixed" className={classes.appBar}>
          <Toolbar>
            <Link to="/">
              <Typography variant="title" color="inherit" noWrap>
                SBS - Simple Booking System
                </Typography>
            </Link>
          </Toolbar>
        </AppBar>
        <Drawer
          variant="permanent"
          classes={{
            docked: classes.drawer,
            paper: classes.drawerPaper
          }}>
          <div className={classes.toolbar} />
          <DrawerMenu>
            <Route exact path={routes.SEARCH} render={props =>
              <React.Fragment>
                <Divider />
                <SearchForm
                  history={props.history}
                  aside={true}
                  expanded={true}
                />
              </React.Fragment>
            } />
          </DrawerMenu>
        </Drawer>
        <main className={classes.content}>
          <div className={classes.toolbar} />
          <Switch>
            <Route exact path={routes.HOME} component={HomePage} />
            <Route exact path={routes.SEARCH} component={SearchPage} />
            <ProtectedRoute
              exact
              path={routes.SIGN_UP}
              component={SignUpPage}
              onlyFor={User.Anonymous}
              redirectPath={routes.HOME}
            />
            <ProtectedRoute
              exact
              path={routes.SIGN_IN}
              component={SignInPage}
              onlyFor={User.Anonymous}
              redirectPath={routes.HOME}
            />
            <ProtectedRoute
              exact
              path={routes.MY_BOOKINGS}
              component={UserBookingsPage}
              onlyFor={User.Authorized}
              redirectPath={routes.SIGN_IN}
            />
            <ProtectedRoute
              exact
              path={`${routes.EDIT_PROPERTY}/:propertyId?`}
              component={EditPropertyPage}
              onlyFor={User.Authorized}
              redirectPath={routes.SIGN_IN}
            />
            <ProtectedRoute
              exact
              path={routes.PUBLISH_PROPERTY}
              component={PublishPropertyPage}
              onlyFor={User.Authorized}
              redirectPath={routes.SIGN_IN}
            />
            <ProtectedRoute
              exact
              path={routes.PUBLISHED_PROPERTIES}
              component={PublishedPropertiesPage}
              onlyFor={User.Authorized}
              redirectPath={routes.SIGN_IN}
            />
            <ProtectedRoute
              exact
              path={`${routes.INCOMING_BOOKINGS}/:propertyId?`}
              component={IncomingBookingsPage}
              onlyFor={User.Authorized}
              redirectPath={routes.SIGN_IN}
            />
            <Route exact path={`${routes.PROPERTY}/:propertyId`} component={PropertyPage} />
            <ProtectedRoute
              exact
              path={routes.SETTINGS}
              component={SettingsPage}
              onlyFor={User.Authorized}
              redirectPath={routes.SIGN_IN}
            />
            <Route render={() => 'Page not found'} />
          </Switch>
        </main>
      </div>
      <Dialogs />
      <Snackbars />
    </React.Fragment>
  );
}

export default withCustomTheme(App);